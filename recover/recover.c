/*
** $Id: recover.c,v 668de5f08019 2008/10/08 21:35:55 mikek $
**
** MLF2 control program.
**
**
*/
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <errno.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include "ioports.h"
#include "lpsleep.h"
#include "log.h"
#include "nvram.h"
#include "newpr.h"
#include "gps.h"
#include "motor.h"
#include "ptable.h"
#include "ballast.h"
#include "sample.h"
#include "msgq.h"
#include "mctl.h"
#include "comm.h"
#include "drogue.h"
#include "version.h"

#define PWORD		"tt8"
#define PWORD_LEN	3

#define NV_ISSET(name, nvp)	(nv_lookup(name, (nvp)) != -1 && (nvp)->l != 0)

#ifdef __GNUC__
static void bailout(void) __attribute__ ((noreturn));
#endif


static double	comm_piston_position = 33.;
extern char	*_mtop, *_mbot, *_mcur;
extern unsigned long 	_H0_org;


static void
bailout(void)
{
    nv_value	nv;

    PET_WATCHDOG();

    fputs("Cleaning up, please wait ... ", stderr);
    fflush(stderr);

    closelog();
    iop_clear_all();

    nv.l = 1;
    nv_insert("power-cycle", &nv, NV_INT, 1);

    fputs("done\n", stderr);
    DelayMilliSecs(500L);

    /*
    ** The infinite loop prevents gcc from thinking the function
    ** does return even though it was declared "noreturn".
    */
    while(1)
	Reset();
}


long nr_spurious_ints;

static void
spur_int_handler(void)
{
    nr_spurious_ints++;
}


/*
 * Try to HOME the piston "N" times.  If the operation fails, manually set
 * the piston position to "x" centimeters.
 */
void
try_home(int N, double x)
{
    while(motor_home(900L) < 0 && N-- > 0)
	log_event("motor HOME process interrupted.  %s\n",
		  (N == 0) ? "Skipping" : "Trying again");

    if(N <= 0)
    {
	log_event("Setting current motor position to %.2f cm\n", x);
	motor_set_position(cm_to_counts(x));
    }

}

INITFUNC(init_recover)
{
    InitTT8(NO_WATCHDOG, TT8_TPU);
    InitCF8(CF8StdCS, CF8StdAddr);
    if(errno != 0)
	printf("\nWARNING: InitCF8 failed, error %d\n", errno);
    if(errno == -1 || errno == PiDosNoHardware)
	exit(1);

    /*
    ** Initialize non-volatile RAM and the I/O ports
    */
    init_nvram();
    init_ioports();

    add_param("comm:piston", PTYPE_DOUBLE, &comm_piston_position);
}

int
main(void)
{
    time_t		secs;
    struct tm		*now;
    long		r, t;
    int			i, c;
    char		pword[PWORD_LEN+1];
    nv_value		nv[2];
    static ExcCFrame	eframe;

    /* Catch spurious interrupts */
    InstallHandler(spur_int_handler, Spurious_Interrupt, &eframe);

    SimSetFSys(16000000L);

    printf("\n\nMLF2 Recovery Program (%s, %s)\n", __DATE__, __TIME__);
    printf("Revision: %s\n\n", REVISION);

    /* Set motor encoder params */
#ifdef DEEP_PISTON
    motor_set_encoder(500L, 546L);
#else
    motor_set_encoder(100L, 411L);
#endif

    /*
    ** Initialize the clock
    */
    secs = RtcToCtm();
    now = localtime(&secs);
    SetTimeTM(now, NULL);

    appendlog("rmlog.txt");
    init_sleep_hooks();

    PET_WATCHDOG();

    /*
    ** Use GPS to set system clock.
    */
    if(gps_init())
    {
	gps_set_clock();
	gps_shutdown();
    }

    mission_read("mission.xml");

    log_event("Start drogue close\n");

    drogue_start_close(30L, 0);

    fputs("Entering RECOVERY MODE in 30 seconds, <CR><CR> to abort\n",
	  stdout);
    if(isleep(30L) != 0)
	bailout();

    log_event("Waiting for drogue\n");
    drogue_wait();

    if(NV_ISSET("piston", &nv[0]))
    {
	log_event("using saved piston position, %ld counts\n", nv[0].l);
	motor_set_position(nv[0].l);
	nv_delete("piston");
	nv_write();
    }
    else
	try_home(5, (double)15.);

    log_event("Adjusting ballast to %.1f cm to reach surface\n",
	      comm_piston_position);
    fputs("Press any key to exit to PicoDOS\n", stdout);

    t = RtcToCtm();

    /*
    ** Try to move the piston out to the COMM-mode position while
    ** checking for keyboard interrupts.
    */
    while((r = motor_move(cm_to_counts(comm_piston_position), 100L,
			  600L, 0)) < 0)
    {
	/*
	** Give up after 30 minutes.
	*/
	if((RtcToCtm() - t) > 1800L)
	    break;

	/*
	** We got a serial interrupt but it might have been line noise
	** so we must prompt for the password.
	*/
	SerInFlush();
	printf("Type %s to exit:", PWORD);
	fflush(stdout);

	/*
	** Allow 2.5s input time for each character
	*/
	i = 0;
	while(i < PWORD_LEN && (c = SerTimedGetByte(2500L)) != -1)
	    pword[i++] = c;
	pword[i] = 0;
	printf("\n");
	if(!strcmp(pword, PWORD))
	    bailout();

	PET_WATCHDOG();
    }

    if(!NV_ISSET("force-abort", &nv[0]))
	mq_add("Watchdog reset");

    /*
     * picpr_init has the side effect of initializing the SPI interface
     */
    newpr_init();
    comm_recovery_mode();

    bailout();

    return 0;
}
