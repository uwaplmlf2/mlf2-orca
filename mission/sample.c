/*
** arch-tag: mlf2 sampling functions
**
** MLF2 sampling functions.
**
*/
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#ifdef __GNUC__
#include <unistd.h>
#endif
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/fcntl.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include "ioports.h"
#include "log.h"
#include "motor.h"
#include "drogue.h"
#include "rh.h"
#include "internalpr.h"
#include "atod.h"
#include "ptable.h"
#include "ballast.h"
#include "lpsleep.h"
#include "gps.h"
#include "util.h"
#include "comm.h"
#include "sensors.h"
#include "sample.h"
#include "abort.h"
#include "cpuclock.h"
#include "fileq.h"
#include "msgq.h"
#include "optode.h"
#include "base64.h"
#include "counters.h"
#include "battery.h"
#include "ctdo.h"
#include "flntu.h"
#include "bb2f.h"
#include "i490.h"
#include "newctd.h"
#include "gtd.h"
#include "suna.h"
#include "newpr.h"

/* LED light activation */
#define LIGHT_ON()      iop_set(IO_C, 0x04)
#define LIGHT_OFF()     iop_clear(IO_C, 0x04)

#define ABS(x)  ((x) < 0 ? -(x) : (x))
#define NINT(x) ((long)((x) + 0.5))
#define MIN(a, b)       ((a) < (b) ? (a) : (b))

#define ESC_CHAR        0x1b

/*
** Elapsed time in days.
*/
#define MISSION_TIME    (double)(RtcToCtm() - mission_start_time)/86400.

#define PI              ((double)3.14159265358979)

#define PISTON_DIA      5.072   /* diameter in centimeters */

/*
** Convert the piston position in cm. to displacement in m^3
*/
#define CM_TO_DISPL(x)  (PI*PISTON_DIA*PISTON_DIA*x/4.)*1.0e-6

/*
** Convert displacement in m^3 to piston position in cm
*/
#define DISPL_TO_CM(x)  (x*4.0e6/(PI*PISTON_DIA*PISTON_DIA))

/*
** Minimum profile mode sampling interval at which we will power-down the
** sensors and sleep between samples.
*/
#define MIN_SAMPLING_INTERVAL   25

/* Seconds required to open/close the drogue */
#define DROGUE_OPEN_TIME        45L
#define DROGUE_CLOSE_TIME       45L


/* Maximum relative humidity (percent) */
#define MAX_RH                  95.0

/* Maximum depth (dbars) */
#define MAX_DEPTH               480.0

/* Maximum log file age in days */
#define MAX_LOG_AGE             10.

/* Ambient Noise preamp warm-up time in seconds */
#define AMB_NOISE_WARMUP        4L

/* ARG channel to use */
#define ARG_CHANNEL             ARG_HIGH_GAIN


/*
** Most of these static variables are parameters which can be
** accessed at runtime via a COMM message.  The parameters
** for this module are setup in init_sample_params() below.
*/
static short    float_id = -1;
static short    env_autoqueue = 1;
static short    sexp_autoqueue = 1;
static long     mtr_stalls, piston_error;
static long     mtr_max_stalls = 50;
static short    check_rh = 1;
static short    eng_max_records = 1000;
static short    surface_gps_fixes = 4;
static short    pressure_emergency = 0, primary_pr = 0;
static short    down_home = 1;
static long     eng_interval = 600;
static long     mission_start_time, mission_duration;
static short    fpr_backlog = 10, sexp_backlog = 0, nc_backlog = 0;
static double   down_start_cc = 50.;
static double   min_micro_battery = 12.0, min_motor_battery = 12.0;
static double   surface_piston;
static double   comm_piston_position = 33.;
static double   max_depth = MAX_DEPTH;
static double   max_log_age = MAX_LOG_AGE;
static short    max_log_count = 10;
static short    drogue_open_time = DROGUE_OPEN_TIME;
static short    drogue_close_time = DROGUE_CLOSE_TIME;
static short    max_bad_pressure = 10;
static short    abort_on_timeout = 1;
static struct sample_t last_sample;
static GPSdata  last_gps;
static Counter_t env_subsample = COUNTER_INIT(0, 8);
static Counter_t data_subsample = COUNTER_INIT(0, 16);
static long sleep_interval = 300;
static enum {PUMP_OFF=0, PUMP_ON} ctdo_pump_state = PUMP_ON;

/*
** Add datafile names to the send queue.
**
** The function below is passed as a callback to sens_open_data_file() to
** record the names of files.
*/

static void
record_filename(const char *name)
{
    (void)fq_add(name);
}

static long
seconds_of_day(void)
{
    time_t      t = RtcToCtm();
    struct tm   *tm;
    long        sod;

    tm = localtime(&t);
    sod = (long)tm->tm_sec + 60L*((long)tm->tm_min + 60L*tm->tm_hour);
    return sod;
}


static void
sensor_sim(int mode)
{
    int         diag = 0, i;
    double      s[4];

#ifdef TESTING
    diag = 1;
#endif

    labsimtest(MISSION_TIME, mode,
               last_sample.T[0],
               last_sample.S[0],
               last_sample.P[last_sample.which_pr],
               CM_TO_DISPL(counts_to_cm(motor_pos())),
               diag, &s[0], &s[1], &s[2]);
#ifdef TESTING
    for(i = 0;i < MAX_PR_SENSORS;i++)
        last_sample.P[i] = s[0];
    last_sample.T[0] = last_sample.T[1] = s[1];
    last_sample.S[0] = last_sample.S[1] = s[2];
#endif /* TESTING */
}




/**
 * Load the "extra message" buffer with a text message which will
 * be sent at the start of the next communications mode.
 *
 * @param       msg     message string.
 *
 */
void
set_extra_message(const char *msg)
{
    log_event("Extra COMM message: %s\n", msg);
    mq_add(msg);
}


static double
rh(void)
{
    short       r;

    if(rh_init())
    {
        while(!rh_dev_ready())
            ;
        r = rh_read_data();
        rh_shutdown();
        return rh_mv_to_percent(r);
    }

    return 0;
}

/*
 * Check the relative humdity.  Return 1 if it is ok, 0 if it exceeds
 * the value MAX_RH.
 */
static int
rh_check(void)
{
    double      x;

    x = rh();
    return (x < MAX_RH);
}

/*
 * Read battery voltage
 */
static double
battery_voltage(battery_pack_t pack)
{
    battery_state_t     batt;

    read_battery(pack, 10, &batt);
    last_sample.v[pack] = batt.v;

#ifdef TESTING
    log_event("Battery%d voltage = %.3f\n", (int)pack, batt.v);
#endif
    return last_sample.v[pack];
}

/*
 * Read internal pressure.
 */
static double
internal_pr(void)
{
    if(ipr_init())
    {
        short   r;

        while(!ipr_dev_ready())
            ;
        r = ipr_read_data();
        ipr_shutdown();
        last_sample.ipr = ipr_mv_to_psi(r);
    }
    else
        last_sample.ipr = 0;


    return last_sample.ipr;
}

/*
 * Add another record to the engineering file. A new
 * file is created if the number of records in the
 * current file exceeds eng_max_records.
 */
static void
log_engineering_data(int mode)
{
    static short eng_records = 0;
    static short eng_file_index = 0;
    static char eng_filename[16];
    FILE        *ofp;

    CPU_SET_SPEED(16000000L);
    if(eng_records > eng_max_records || eng_records == 0)
    {
        /* Add previous file to the send queue */
        if(eng_file_index > 0)
            fq_add(eng_filename);
        while(eng_file_index <= 100)
        {
            eng_file_index++;
            sprintf(eng_filename, "eng%05d.csv", eng_file_index);
            if(!fileexists(eng_filename))
                break;
        }

        eng_records = 0;
        if((ofp = fopen(eng_filename, "w")) != NULL ||
           (ofp = fopen(eng_filename, "w")) != NULL)
        {
            fputs("time,mode,v1,v2,rh,ipr,drogue\n",
                  ofp);
            fclose(ofp);
        }
        else
            log_error("mission",
                      "Cannot open engineering file\n");
    }

    eng_records++;

    if((ofp = fopen(eng_filename, "a")) != NULL ||
       (ofp = fopen(eng_filename, "a")) != NULL)
    {
        fprintf(ofp, "%ld,%d,%.3f,%.3f,%.1f,%.1f,%d\n",
                RtcToCtm(),
                mode,
                battery_voltage(BATTERY_12v),
                battery_voltage(BATTERY_15v),
                rh(),
                internal_pr(),
                drogue_isopen() ? 1 : 0);
        fclose(ofp);
    }
    else
        log_error("mission",
                  "Cannot open engineering file\n");
    CPU_RESET_SPEED();
}


/*
 * Check if float is on the surface.
 * This function moves the piston out to target_cm while checking the
 * GPS for a series of nr_good_gps GPS fixes which should indicate
 * that the float is on the surface.  The piston is allowed to move for
 * 60 seconds in between GPS checks which are allowed 10 seconds.
 *
 * @param  target_cm  desired piston position in cm.
 * @param  nr_good_gps  number of GPS fixes.
 * @param  timeout  maximum time to wait in seconds
 * @return 1 (fix obtained), 0 (no fix, piston at target), -1 (timeout)
 *
 */
static int
check_for_surface(double target_cm, int nr_good_gps, long timeout)
{
    GPSdata     *gdp = &last_gps;
    long        t0, mpos;
    int         r = 0, i = 0, good_fix;
    fstore_t    callback;

    if(env_autoqueue)
        callback = record_filename;
    else
        callback = NULL;

    CPU_SET_SPEED(16000000L);

    gps_init();

    /* Start the drogue closing */
    drogue_start_close(drogue_close_time, 0);

    sens_open_data_file(GPS_FILE, callback);
    t0 = RtcToCtm();
    while((mpos = motor_move(cm_to_counts(target_cm), 1000L, 60L, 0)) < 0)
    {
        PET_WATCHDOG();
        drogue_check();

        if(mpos == MTR_POS_ERROR)
        {
            log_error("motor", "At limit. Forcing motor position to 0\n");
            mq_add("Motor limit switch set");
            motor_set_position(0L);
        }
        else if(mpos == MTR_STALLED)
        {
            log_error("motor", "Stall detected\n");
            mtr_stalls++;
            if(pressure_emergency > 0)
            {
                log_error("motor", "Emergency abort. Motor stall + excessive pressure\n");
                abort_mission(1);
            }
        }
#ifdef TESTING
        else if(mpos ==  MTR_INTERRUPTED)
        {
            log_error("motor", "Keyboard interrupt\n");
            r = 1;
            break;
        }
#endif

        /*
        ** Third argument to sens_wait_for_gps is zero to prevent a TT8 clock
        ** update once a fix is obtained. Updating the clock could cause us to
        ** erroneously report a timeout to the caller.
        */
        good_fix = sens_wait_for_gps(gdp, 10L, 0, 1);
        sens_write_gps_fix(gdp);
        if(good_fix)
        {
            i++;
            if(i == nr_good_gps)
            {
                r = 1;
                if(pressure_emergency)
                    pressure_emergency = 0;
                break;
            }

        }

        if((RtcToCtm() - t0) > timeout)
        {
            log_error("comm", "Motor timeout on surfacing\n");
            r = -1;
            break;
        }

    }

    if(mtr_stalls > 0)
        mq_add("WARNING: motor stalled");

    surface_piston = counts_to_cm(motor_pos());
    sens_close_file(GPS_FILE);
    log_event("Waiting for drogue operation to complete\n");
    drogue_wait();
    gps_shutdown();

    CPU_RESET_SPEED();

    return r;
}

/**
 * Prepare to enter a communications mode.
 * Extends piston until the surface is reached. Will abort the
 * mission if this process times-out.
 *
 * @param  mode  current mode (currently ignored)
 * @return mode
 */
static int
enter_comm(int mode)
{
    int         status;

    log_event("Adjusting ballast to %.1f cm to reach surface\n",
              comm_piston_position);

    status = check_for_surface(comm_piston_position, surface_gps_fixes, 2700L);
    switch(status)
    {
        case -1:        /* timeout */
            if(abort_on_timeout)
                abort_mission(1);
            else
                mode = -1;
            break;
        case 0:         /* no GPS fix */
            break;
        case 1:         /* ok */
            if(gps_init() != 0)
            {
                /* Sync clock to GPS */
                gps_set_clock();
                gps_shutdown();
            }
            break;
    }


    /* Power on the ARGOS PTT */
    iop_set(IO_C, 0x08);

    return mode;
}

/**
 * Re-home the piston to check for accumulated position error.
 *
 * @param errval  pointer to returned piston error value
 * @return 1 on success, 0 on failure
 */
static int
check_piston_error(long *errval)
{
    long        x0, x1;

    /*
    ** Open the drogue to prevent the float from sinking too
    ** fast.
    */
    if(!drogue_isopen())
    {
        log_event("Opening drogue\n");
        PET_WATCHDOG();
        drogue_start_open(drogue_open_time, 0);
        drogue_wait();
        PET_WATCHDOG();
    }

    log_event("Checking piston error\n");

    /*
     * Motor errors when homing indicate a potentially serious problem
     * allow two before returning MODE_ERROR.
     */
    CPU_SET_SPEED(16000000L);
    x0 = motor_pos();
    if((x1 = motor_home(900L)) == MTR_TIMEOUT)
    {
        log_error("motor", "Cannot HOME piston (error code = %ld)\n", x1);

        x1 = x0;
        /*
        ** Try to HOME one more time and then abort.
        */
        if(motor_home(900L) == MTR_TIMEOUT)
        {
            log_error("motor", "Cannot HOME the piston\n");
            mq_add("Cannot HOME piston");
            CPU_RESET_AND_RETURN(0);
        }
    }

    if(x1 == 0)
    {
        log_error("motor", "Motor encoder failed\n");
        mq_add("ERROR: motor encoder failed\n");
        CPU_RESET_AND_RETURN(0);
    }

    CPU_RESET_SPEED();
    *errval = x1 - x0;

    return 1;
}

/**
 * Handle the exit from a communications mode.
 * Re-homes the piston if necessary and calls mlf2_ballast
 * to obtain the next mode.
 *
 * @param  mode  current mode.
 * @param  do_piston_check  if non-zero, re-home the piston.
 * @param  do_gps_check  if non-zero, record a final GPS fix.
 * @return next mode.
 */
static int
exit_comm(int mode, int do_piston_check, int do_gps_check)
{
    double      ballast, ignored;
    int         drogue;
    GPSdata     *gdp = &last_gps;
    fstore_t    callback;

    if(env_autoqueue)
        callback = record_filename;
    else
        callback = NULL;

    /* Record a final GPS fix */
    if(do_gps_check && gps_init())
    {
        sens_open_data_file(GPS_FILE, callback);
        sens_wait_for_gps(gdp, 60L, 1, 1);
        sens_write_gps_fix(gdp);
        sens_close_file(GPS_FILE);
        gps_shutdown();
    }

    if(do_piston_check == 1)
    {
        if(check_piston_error(&piston_error) == 0)
            return MODE_ERROR;
#ifdef TESTING
        log_event("Piston error %ld counts\n", piston_error);
#endif
    }

    sensor_sim(mode);

    /*
    ** Get the next mode.
    */
    drogue = drogue_isopen() ? 1 : 0;
    CPU_SET_SPEED(16000000L);
    ballast = CM_TO_DISPL(counts_to_cm(motor_pos()));
#ifdef SAFE_MODE
    safe_ballast(MISSION_TIME, 0.,
                 ballast,
                 mode,
                 &ballast, &mode, &drogue);
#else
    mlf2_ballast(MISSION_TIME, 0.,
                 last_sample.T[0], last_sample.S[0],
                 last_sample.T[1], last_sample.S[1],
                 ballast,
                 mode,
                 drogue,
                 (double)seconds_of_day(),
                 float_id,
                 &ballast, &mode, &drogue, &ignored);
#endif
    CPU_RESET_SPEED();

    return mode;
}

/**
 * Manage a communications mode.
 *
 * @param  mode  current mode.
 * @return next mode.
 */
static int
call_home(int mode)
{
    int         check_gps = 1;

    mode = enter_comm(mode);

    if(mode >= 0)
        sensor_sim(mode);

    /* Power on the ARGOS PTT */
    iop_set(IO_C, 0x08);

    if(mode == MODE_COMM)
    {
        log_event("Begin communications mode\n");
        comm_mode("status", 0, NULL);
    }
    else if(mode == MODE_ERROR)
    {
        log_event("Begin comm-wait mode\n");
        comm_wait(0);
        /*
        ** By convention we always exit a communications
        ** mode with mode set to MODE_COMM.
        */
        mode = MODE_COMM;
    }
    else if(mode == MODE_DONE)
    {
        log_event("Mission completed\n");
        mq_add("Mission completed.");
        comm_wait(0);
        /* Power off the ARGOS PTT */
        iop_clear(IO_C, 0x08);
        /*
        ** Skip the call to exit_comm because there is no
        ** need to get the next mode, we're done.
        */
        return mode;
    }
    else if(mode == -1)
    {
        /*
         * This code will only be reached if abort_on_timeout is set
         * to zero. All we can do is log the error and carry-on.
         */
        log_error("surfacing", "Timeout while surfacing");
        mode = MODE_COMM;
        check_gps = 0;
    }

    /* Power off the ARGOS PTT */
    iop_clear(IO_C, 0x08);

    return exit_comm(mode, (down_home==0), check_gps);
}


/**
 * Power sensors on or off.
 *
 * @param  off  bitmap of sensors to power off.
 * @param  on   bitmap of sensors to power on.
 * @return bitmap of active sensors.
 */
static unsigned long
update_sensors(unsigned long off, unsigned long on)
{
    /*
     * This is a bit of a hack because the "core" sensors
     * are managed by sens_initialize_sensors while the
     * rest and managed independently.
     */
    sens_shutdown_sensors(off);
    if(on)
        return sens_initialize_sensors(on)
          | (on & SENS_GTD2)
          | (on & SENS_GTD)
          | (on & SENS_OPTODE)
          | (on & SENS_FLNTU)
          | (on & SENS_BB2F)
          | (on & SENS_I490)
          | (on & SENS_SSAL_CAL)
          | (on & SENS_ARG);
    else
        return 0;
}

/**
 * Return the sampling parameters for a sampling mode.
 *
 * @param  mode  sampling mode.
 * @param  sensors  returned bitmap of sensors to be sampled.
 * @param  t_samp  returned sampling interval.
 * @param  t_motor  returned maximum time to run the motor
 * @param  stay_on  if non-zero, sensors should be left on between samples.
 */
static void
check_mode(int mode, unsigned long *sensors, long *t_samp,
           long *t_motor, int *stay_on)
{
    if(mode < 0 || mode >= NR_REAL_MODES)
    {
        /* Invalid mode, keep the old parameters */
        if(mode != MODE_START)
            log_error("check_mode", "Unknown mode: %d\n", mode);
        return;
    }

    *sensors = Sensors[mode];
    if(is_profile(mode))
    {
        *t_motor = Si[mode];
        *t_samp = 0;    /* sample as fast as possible */
        *stay_on = 1;
    }
    else
    {
        *t_samp = Si[mode];
        *t_motor = 0;   /* determine dynamically */
        if(*t_samp < MIN_SAMPLING_INTERVAL)
            *stay_on = 1;
        else
            *stay_on = 0;
    }
}

/**
 * Sample all active sensors.
 *
 * @param  active  bitmap of active sensors.
 * @param  t  returned timestamp.
 * @param  sp  returned sample values.
 * @return 1 if successful, 0 on error.
 */
static int
sample_data(unsigned long active, long *t, struct sample_t *sp)
{
    sp->which_pr = primary_pr;
    *t = sens_read_env_data(active, sp);
    return *t > 0;
}


static void
write_base64(FILE *ofp, void *data, size_t n)
{
    size_t      i, rem, len;
    unsigned char       *p;

    fputc('|', ofp);

    for(i = 0,p = data;i < n;i += 3,p += 3)
    {
        rem = n - i;
        len = (rem < 3) ? rem : 3;
        b64_write(ofp, p, len);
    }
    fputc('|', ofp);
}

/*
 * Print the same formated output to a list of FILES.
 */
static void
printf_multi(FILE **files, const char *fmt, ...)
{
    va_list     args;

    va_start(args, fmt);
    while(*files)
    {
        vfprintf(*files, fmt, args);
        files++;
    }
    va_end(args);
}

static void
write_base64_multi(FILE **files, void *data, size_t n)
{
    while(*files)
    {
        write_base64(*files, data, n);
        files++;
    }
}

static void
write_pr_data(FILE **flist)
{
    int         n, i;

    n = last_sample.npr;

    printf_multi(flist, "(fpr #%lx# (", last_sample.pr_tlast-n);
    for(i = 0;i < MAX_PR_SENSORS;i++)
    {
        write_base64_multi(flist, last_sample.fastP[i], n*sizeof(float));
        printf_multi(flist, " ");
    }
    printf_multi(flist, "))\n");
}

static int
sample_ctdo(CTDOdata *ctdo, int update_pressure)
{
    int rval = 0;
    long    t;
    double  top_pr, bottom_pr;

    if(!ctdo_start_oxy())
        ctdo_start_oxy();

    if(update_pressure)
        sens_read_pressure(&last_sample, &t);
    adjust_pressure(last_sample.P[last_sample.which_pr], last_sample.which_pr,
            &top_pr, &bottom_pr);

    DelayMilliSecs(1000L);

    if(ctdo_start_sample(bottom_pr) || ctdo_start_sample(bottom_pr))
    {
        while(!ctdo_data_ready())
            ;
        if(ctdo_read_data(ctdo))
            rval = 1;
    }
    else
        log_error("ctdo", "Cannot start sample\n");

    if(!ctdo_stop_oxy())
        ctdo_stop_oxy();
    ctdo_cmd("pumpoff\r\n");
#ifdef TESTING
    log_event("CTDO pump off\n");
#endif

    return rval;
}

static unsigned long
pre_sample(int mode, unsigned long sensors)
{
    return sensors;
}

static unsigned long
post_sample(int mode, unsigned long sensors)
{
#ifndef SAFE_MODE
    FILE        *datafp, *qlfp, *prfp, *flist[4];
    long        t;
    CTDOdata    ctdo;

    datafp = qlfp = prfp = NULL;

    sens_open_sexp_files(COUNTER_DEC_TEST(&data_subsample),
                         &datafp, &qlfp, &prfp);

    flist[0] = prfp;
    flist[1] = qlfp;
    flist[2] = NULL;

    /* Write 1-hz pressure data */
    write_pr_data(flist);

    if(sensors & SENS_CTDO)
    {
        flist[0] = datafp;

        if(sample_ctdo(&ctdo, 1))
        {
            printf_multi(flist, "(ctdo #%lx# (", ctdo_start_time());
            write_base64_multi(flist, &ctdo, sizeof(ctdo));
            printf_multi(flist, " #%lx#))\n", (long)ctdo_pump_state);
        }
        else
            printf_multi(flist, "(err #%lx# ctdo)\n", ctdo_start_time());
        /*
         * sample_ctdo() reads the fast-pressure data so we need to call
         * write_pr_data() again to insure that the sampled data is recorded.
         */
        flist[0] = prfp;
        write_pr_data(flist);
    }

    if(datafp)
        fclose(datafp);
    if(prfp)
        fclose(prfp);
    if(qlfp)
        fclose(qlfp);

    sensor_sim(mode);

#endif /* !SAFE_MODE */
    return sensors;
}

static unsigned long
pre_ballast(int mode, unsigned long sensors)
{
    return sensors;
}

static unsigned long
post_loop(int mode, unsigned long sensors)
{
#ifndef SAFE_MODE
    if(sensors & SENS_CTDO)
    {
        if(!ctdo_cmd("pumpoff\r\n"))
            ctdo_cmd("pumpoff\r\n");
    }
#endif
    return sensors;
}

/**
 * Write the sampled data to disk.
 *
 * @param  timestamp  data timestamp in seconds since 1/1/1970.
 * @param  sp  sampled data values.
 * @return 1 if successful, 0 on error.
 */
static int
write_data(long timestamp, struct sample_t *sp)
{
    fstore_t    callback;

    if(env_autoqueue)
        callback = record_filename;
    else
        callback = NULL;

    CPU_SET_SPEED(16000000L);

    if(sens_maybe_close_file(ENV_FILE))
    {
        if(!sens_open_data_file(ENV_FILE, callback))
        {
            log_error("mission", "Cannot open data file\n");
            mq_add("Cannot open data file");
            CPU_RESET_AND_RETURN(0);
        }
    }

    sens_write_env_data(timestamp, counts_to_cm(motor_pos()),
                        sp, COUNTER_DEC_TEST(&env_subsample));
    CPU_RESET_SPEED();

    return 1;
}

/**
 * Check for various error conditions.
 *
 * @param  sp  most recent data sample.
 * @return 1 if ok, 0 on error.
 */
static int
error_check(struct sample_t *sp)
{
    double      v;

    if(sp->P[0] > max_depth)
    {
        mq_add("Maximum depth exceeded: %f", sp->P[0]);
        pressure_emergency = 1;
        return 0;
    }

    if((v = battery_voltage(BATTERY_12v)) <= min_micro_battery)
    {
        log_error("mission", "Low micro battery voltage: %f\n", v);
        mq_add("Low micro battery voltage: %f", v);
        return 0;
    }

    if((v = battery_voltage(BATTERY_15v)) <= min_motor_battery)
    {
        log_error("mission", "Low motor battery voltage: %f\n", v);
        mq_add("Low motor battery voltage: %f", v);
        return 0;
    }

    if(check_rh && !rh_check())
    {
        log_error("mission", "Maximum humdity exceeded\n");
        mq_add("Excessive humidity detected");
        return 0;
    }

    return 1;
}

/**
 * Adjust the float ballast.
 * Calculates the new piston displacement and moves the piston if neccessary.
 * Also starts any required drogue adjustment.
 *
 * @param  mode  sampling mode.
 * @param  sp  latest data sample.
 * @param  motor_timeout  allowed time for ballast adjustment.
 * @return new sampling mode.
 */
static int
ballast_adjust(int mode, struct sample_t *sp, long motor_timeout)
{
    int         drogue;
    long        target = 0, r;
    double      ballast, ignored, actual_ballast, db;


    CPU_SET_SPEED(16000000L);
    drogue = drogue_isopen();
    ballast = CM_TO_DISPL(counts_to_cm(motor_pos()));
#ifdef SAFE_MODE
    safe_ballast(MISSION_TIME, last_sample.P[last_sample.which_pr],
                 ballast,
                 mode,
                 &ballast, &mode, &drogue);
#else
    mlf2_ballast(MISSION_TIME, last_sample.P[last_sample.which_pr],
                 last_sample.T[0], last_sample.S[0],
                 last_sample.T[1], last_sample.S[1],
                 ballast,
                 mode,
                 drogue,
                 (double)seconds_of_day(),
                 float_id,
                 &ballast, &mode, &drogue, &ignored);
#endif
    target = cm_to_counts(DISPL_TO_CM(ballast));
    CPU_RESET_SPEED();

    if(mode == MODE_ERROR)
    {
        mq_add("MODE_ERROR returned from mlf2_ballast");
        return mode;
    }

    if(drogue)
    {
        if(drogue_start_open(drogue_open_time, 0))
            log_engineering_data(mode);
    }
    else
    {
        if(drogue_start_close(drogue_close_time, 0))
            log_engineering_data(mode);
    }


    r = 0;
    if(motor_timeout > 2L)
    {
        CPU_SET_SPEED(16000000L);
        r = motor_move(target, 1000L, motor_timeout, 0);
        CPU_RESET_SPEED();

        switch(r)
        {
            case MTR_STALLED:
                if(++mtr_stalls >= mtr_max_stalls)
                {
                    mq_add("ERROR: motor stalled");
                    mode = MODE_ERROR;
                }
                break;
            case MTR_POS_ERROR:
                mq_add("Motor limit switch set");
                motor_set_position(0L);
                mode = MODE_ERROR;
                break;
            default:
                break;
        }
    }
    else
        log_event("WARNING: Not enough time to move piston\n");

    actual_ballast = CM_TO_DISPL(counts_to_cm(motor_pos()));
    db = ABS(actual_ballast - ballast);
    if(db > 1e-8)
        log_event("Target ballast = %.2f cm^3 ; Actual ballast = %.2f cm^3\n",
                  ballast*1.0e6, actual_ballast*1.0e6);

    drogue_check();
    return mode;
}

static void
wait_for_next(long t, unsigned long sensorlist)
{
    time_tt     t_end;

    t_end.secs = t;
    t_end.ticks = 0L;

    while(ttmcmp(t_end, ttm_now()) > 0L)
    {
        if(!sensorlist)
            lp_sleep_till(t_end, 0);
    }
}

static int
check_for_escape(void)
{
    int         c;

    if(SerByteAvail() && (c = SerGetByte()) == ESC_CHAR)
    {
        printf("ESC detected. Enter a second ESC to confirm: ");
        fflush(stdout);
        c = SerTimedGetByte(3000L);
        switch(c)
        {
            case ESC_CHAR:
                return 1;
                break;
            case -1:
                printf("\nUser input timed out\n");
                break;
            default:
                printf("\nCancelled\n");
                break;
        }
    }

    return 0;
}


/*
 * Synchronize mode start time to a sampling interval boundary.
 *
 * @param  dt  sampling interval in seconds.
 * @return current time.
 */
static time_t
wait_for_start(long dt)
{
    time_tt     start;
    time_t      T;
    long        m;

    T = RtcToCtm();
    if(dt == 0)
        return T;

    m = T % dt;
    if(m == 0)
        return T;

    start.secs = T + dt - m;
    start.ticks = 0L;
    log_event("Waiting for start interval\n");
    while(ttmcmp(start, ttm_now()) > 0L)
        lp_sleep_till(start, 0);
    return RtcToCtm();
}


/* Check for end of sampling */
#define TERMINAL(m) ((m) == MODE_DONE ||        \
                     (m) == MODE_ERROR ||       \
                     (m) == MODE_COMM ||        \
                     (m) == MODE_SLEEP ||       \
                     (m) == MODE_GPS)
/* Sensors which are always on */
#ifdef SAFE_MODE
#define ALWAYS_ON       0
#else
#define ALWAYS_ON       (SENS_GTD|SENS_GTD2|SENS_CTDO)
#endif

static int
sampling_mode(int mode)
{
    unsigned long       sensors, active, onlist, offlist;
    long                dt, t, timestamp, tnext, count, t0;
    long                motor_timeout, mt;
    long                last_eng;
    int                 stay_on;
    fstore_t    callback;

    active = 0;
    count = 0;
    sensors = 0;
    dt = 0;
    stay_on = 0;
    last_eng = 0;
    motor_timeout = 0;
    mt = 0;
    t0 = RtcToCtm();
    last_sample.mode = MODE_DONE;

    if(env_autoqueue)
        callback = record_filename;
    else
        callback = NULL;

    if(!sens_open_data_file(ENV_FILE, callback))
    {
        log_error("mission", "Cannot open data file\n");
        mq_add("Cannot open data file");
        return MODE_ERROR;
    }

    active = update_sensors(0, ALWAYS_ON);

    while(!TERMINAL(mode))
    {
        t = RtcToCtm();
        if((t - mission_start_time) >= mission_duration)
        {
            mode = MODE_DONE;
            break;
        }

        if(mode != last_sample.mode)
        {
            if(last_sample.mode == MODE_PROFILE_DOWN && down_home == 1)
            {
                if(check_piston_error(&piston_error) == 0)
                {
                    mode = MODE_ERROR;
                    break;
                }
#ifdef TESTING
                log_event("Piston error %ld counts\n", piston_error);
#endif
            }

            log_event("Entering mode %d\n", mode);
            check_mode(mode, &sensors, &dt,
                       &motor_timeout, &stay_on);
            log_engineering_data(mode);

            t0 = wait_for_start(dt);
            count = 0;
            last_sample.mode = mode;
        }
        else
            check_mode(mode, &sensors, &dt,
                       &motor_timeout, &stay_on);

        count++;
        tnext = t0 + count*dt;

        /*
         * sensors = list of sensors to be sampled in this mode
         * active = list of sensors currently powered on
         * offlist = list of sensors to be powered off
         * onlist = list of sensors to be powered on
         */
        offlist = active & ~sensors;
        onlist = sensors & ~active;
        if(offlist || onlist)
        {
#ifdef TESTING
            log_event("on=0x%08lx  off=0x%08lx\n", onlist, offlist);
#endif
            active |= update_sensors(offlist, onlist);
            active &= ~offlist;
            onlist &= ~ALWAYS_ON;
        }

#ifdef TESTING
        log_event("active=0x%08lx\n", active);
#endif
        active = pre_sample(mode, active);

#ifdef TESTING
        log_event("active=0x%08lx\n", active);
#endif

        if(active & SENS_CTDO)
        {
            ctdo_cmd("pumpfast\r\n");
            ctdo_cmd("pumpfast\r\n");
#ifdef TESTING
            log_event("CTDO pump on\n");
#endif
        }

        if(!sample_data(active, &timestamp, &last_sample))
        {
            mode = MODE_ERROR;
            mq_add("Pressure sensor error");
            pressure_emergency = 1;
            break;
        }

        if(!stay_on)
        {
            /*
             * Bit of a hack here to prevent the SUNA from being removed
             * from the active list. This device is managed by the
             * pre_sample and post_sample functions.
             */
            offlist = onlist & ~SENS_SUNA;
            update_sensors(offlist, 0);
            active &= ~offlist;
        }

        if(!error_check(&last_sample))
        {
            mode = MODE_ERROR;
            break;
        }

        active = post_sample(mode, active);

        printf("BEGIN Escape Window.\nType ESC to end mission ...\n");

        if(!write_data(timestamp, &last_sample))
        {
            mode = MODE_ERROR;
            break;
        }

        if((t - last_eng) >= eng_interval)
        {
            last_eng = t;
            log_engineering_data(mode);
        }

        active = pre_ballast(mode, active);

        printf("END Escape Window.\n");

        if(check_for_escape())
        {
            mq_add("Mission ended from console");
            log_event("Mission ended from console\n");
            mode = MODE_DONE;
            break;
        }

        /* set motor timeout */
        mt = (motor_timeout == 0) ? tnext-RtcToCtm()-4L : motor_timeout;

        mode = ballast_adjust(mode, &last_sample, mt);
        wait_for_next(tnext, active);
    }

    active = post_loop(mode, active);

    /* Power off the CTDO */
    update_sensors(active & SENS_CTDO, 0);
    active &= ~SENS_CTDO;

    /* Power everything off */
    update_sensors(active, 0);

    /* Close the data files */
    sens_close_file(ENV_FILE);

    log_engineering_data(mode);

    /*
     * On a PROFILE_DOWN->SLEEP transition, re-home the piston if
     * the down_home parameter is set.
     */
    if(mode == MODE_SLEEP && last_sample.mode == MODE_PROFILE_DOWN
       && down_home == 1)
    {
        if(check_piston_error(&piston_error) == 0)
            mode = MODE_ERROR;
#ifdef TESTING
        log_event("Piston error %ld counts\n", piston_error);
#endif
    }

    return mode;
}

INITFUNC(init_sample_params)
{
    add_param("duration",               PTYPE_LONG, &mission_duration);
    add_param("check_rh",               PTYPE_SHORT, &check_rh);
    add_param("log_age",                PTYPE_DOUBLE, &max_log_age);
    add_param("log_count",              PTYPE_SHORT, &max_log_count);
    add_param("env:subsample",          PTYPE_LONG, &env_subsample.reset);
    add_param("sexp:subsample",          PTYPE_LONG, &data_subsample.reset);
    add_param("motor:max_stalls",       PTYPE_LONG, &mtr_max_stalls);
    add_param("down:start_cc",          PTYPE_DOUBLE, &down_start_cc);
    add_param("min_micro_battery",      PTYPE_DOUBLE, &min_micro_battery);
    add_param("min_motor_battery",      PTYPE_DOUBLE, &min_motor_battery);
    add_param("surface:gps_fixes",      PTYPE_SHORT, &surface_gps_fixes);
    add_param("comm:piston",            PTYPE_DOUBLE, &comm_piston_position);
    add_param("motor:stalls",           PTYPE_LONG, &mtr_stalls);
    add_param("primary_pr",             PTYPE_SHORT, &primary_pr);
    add_param("down_home",              PTYPE_SHORT, &down_home);
    add_param("env_autoqueue",          PTYPE_SHORT, &env_autoqueue);
    add_param("sexp_autoqueue",         PTYPE_SHORT, &sexp_autoqueue);
    add_param("fpr_backlog",            PTYPE_SHORT, &fpr_backlog);
    add_param("sexp_backlog",           PTYPE_SHORT, &sexp_backlog);
    add_param("nc_backlog",             PTYPE_SHORT, &nc_backlog);
    add_param("pr:max_zeros",           PTYPE_SHORT, &max_bad_pressure);

    add_param("drogue:topen",           PTYPE_SHORT, &drogue_open_time);
    add_param("drogue:tclose",          PTYPE_SHORT, &drogue_close_time);

    add_param("sleep:interval",         PTYPE_LONG, &sleep_interval);
    add_param("abort_on_timeout",       PTYPE_SHORT, &abort_on_timeout);

    add_param("piston_error",           PTYPE_LONG|PTYPE_READ_ONLY,
              &piston_error);
    add_param("start_time",             PTYPE_LONG|PTYPE_READ_ONLY,
              &mission_start_time);
    add_param("surface_piston",         PTYPE_DOUBLE|PTYPE_READ_ONLY,
              &surface_piston);
    add_param("max_depth",              PTYPE_DOUBLE|PTYPE_READ_ONLY,
              &max_depth);
    add_param("floatid",                PTYPE_SHORT, &float_id);
}

/*
 * Dummpy functions to allow us to use the SPURS mission code.
 */
static int _fake_obj;
void *ssal_start_profile(long timeout)
{
    return &_fake_obj;
}

int ssal_stop_profile(void *obj)
{
    return 1;
}

void enable_pump_cycle(void)
{
}

void disable_pump_cycle(void)
{
}

#ifdef __GNUC__
#define rename(a, b)    _rename(a, b)
int _rename(const char* old, const char *new);
#endif

void
rotate_logs(void)
{
    int         i, j;
    char        file1[16], file2[16];

    CPU_SET_SPEED(16000000L);

    log_event("Rotating log files\n");

    i = max_log_count - 1;
    /* Two digit max */
    if(i > 99)
        i = 99;
    j = i - 1;
    sprintf(file1, "syslog%02d.txt", i);
    if(fileexists(file1))
        unlink(file1);
    while(j > 0)
    {
        sprintf(file1, "syslog%02d.txt", i);
        sprintf(file2, "syslog%02d.txt", j);
        if(fileexists(file2))
            rename(file2, file1);
        j--;
        i--;
    }

    if(fileexists("syslog0.txt"))
        rename("syslog0.txt", "syslog01.txt");

    CPU_RESET_SPEED();
}

int
gps_fix(int mode)
{
    log_event("Surface for GPS fix\n");

    (void)enter_comm(mode);
    mode = exit_comm(mode, 0, 0);

    return mode;
}

int
sleep_mode(int mode)
{
    long            timestamp;
    int             file_opened;
    fstore_t        callback;
    unsigned long   active;

    if(env_autoqueue)
        callback = record_filename;
    else
        callback = NULL;

    file_opened = sens_open_data_file(ENV_FILE, callback);

    while(mode == MODE_SLEEP)
    {
        log_event("Sleeping for %ld seconds ...\n", sleep_interval);
        newpr_shutdown();
        if(isleep(sleep_interval) == 1)
        {
            newpr_init();
            log_event("Mission interrupted from console\n");
            mode = MODE_DONE;
        }
        else
        {
            newpr_init();
            DelayMilliSecs(2000L);
            /*
             * Power-on the CTDs, take a sample, power-off.
             */
            active = update_sensors(0, SENS_CTD|SENS_CTDO);
            if(active & SENS_CTDO)
                ctdo_cmd("pumpfast\r\n");
            sample_data(SENS_CTD|SENS_CTDO, &timestamp, &last_sample);
            if(active & SENS_CTDO)
                if(!ctdo_cmd("pumpoff\r\n"))
                    ctdo_cmd("pumpoff\r\n");
            update_sensors(SENS_CTD|SENS_CTDO, 0);

            sensor_sim(MODE_SLEEP);

            if(file_opened)
                write_data(timestamp, &last_sample);

            /* Ballasting code sets the next mode */
            mode = ballast_adjust(mode, &last_sample, 300L);

            /* Check for mission end-time */
            if((RtcToCtm() - mission_start_time) >= mission_duration)
                mode = MODE_DONE;
        }
    }

    /* Close the data file */
    if(file_opened)
        sens_close_file(ENV_FILE);

    return mode;
}

void
mlf2_main_loop(int mode, long duration)
{
    unsigned long       tlog;

    pressure_emergency = 0;

    unlink(DATAFILE_LIST);

    if(fileexists(MESSAGE_FILE))
        unlink(MESSAGE_FILE);

    log_event("Store initial GPS reading\n");

    sens_wait_for_gps(&last_gps, 5L, 0, 1);

    mission_duration = duration;
    tlog = mission_start_time = RtcToCtm();
    disable_pump_cycle();
    mode = ballast_adjust(mode, &last_sample, 5L);

    while(mode != MODE_DONE)
    {
#ifdef SAFE_MODE
        int     i;

        /* Disable all sensors */
        for(i = 0;i < NR_REAL_MODES;i++)
            Sensors[i] = 0;

        /* Set sample interval to 55 seconds */
        Si[MODE_SETTLE] = 55;
        Si[MODE_DRIFT_ISO] = 55;
        Si[MODE_DRIFT_ML] = 55;
        Si[MODE_DRIFT_SEEK] = 55;

#endif
        mode = sampling_mode(mode);

#ifndef SAFE_MODE
        /* Start a new log file if necessary */
        if((RtcToCtm() - tlog) >= (max_log_age*86400.))
        {
            closelog();
            rotate_logs();
            openlog("syslog0.txt");
            tlog = RtcToCtm();
        }
#else
        mq_add("Safe Mode");
#endif
        switch(mode)
        {
            case MODE_DONE:
                break;
            case MODE_GPS:
                mode = gps_fix(mode);
                break;
            case MODE_SLEEP:
                mode = sleep_mode(mode);
                break;
            default:
                mode = call_home(mode);
                break;
        }

    }

    (void)call_home(MODE_DONE);
    log_event("Mission complete. Aborting.\n");

    abort_mission(1);
}
