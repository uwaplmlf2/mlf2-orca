/*
** arch-tag: 550f1d8c-4802-47fb-830f-a3e5ad2c9c3e
**
** MLF2 comm-mode functions.
**
*/
#include "config.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <tt8lib.h>
#include <tt8.h>
#include <picodcf8.h>
#include "util.h"
#include "nvram.h"
#include "ioports.h"
#include "lpsleep.h"
#include "ptable.h"
#include "log.h"
#include "gps.h"
#include "atod.h"
#include "rh.h"
#include "internalpr.h"
#include "iridium.h"
#include "xmodem.h"
#include "netcdf.h"
#include "picalarm.h"
#include "message.h"
#include "sample.h"
#include "sensors.h"
#include "hash.h"
#include "fileq.h"
#include "msgq.h"
#include "compress.h"
#include "ballast.h"
#include "abort.h"
#include "comm.h"
#include "xcat.h"
#include "battery.h"
#include "serial.h"
#include "cpuclock.h"

#ifdef __GNUC__
#define rename(a, b)    _rename(a, b)
int _rename(const char* old, const char *new);
static void serial_text(int sdev, const char *text, ...) __attribute__ ((format(printf, 2, 3)));
#endif

#define RUDICS_PREFIX   "00"
/* CLS RUDICS number */
#define RUDICS_PN       "881600005347"

#define DIR_ENTRY       24
#define DIRBUF_SIZE     (DIR_ENTRY*2000L)

/* Constants for file transmission */
#define MAX_FILELEN     16
#define RX_TEMPLATE "put %s\n"
#define MAX_RX_COMMAND MAX_FILELEN+16
#define RX_RESP_TEMPLATE "receive %s|NO CARRIER"
#define MAX_RX_RESPONSE MAX_FILELEN+24

/* LED flasher activation */
#define FLASHER_ON()    iop_set(IO_C, 0x04)
#define FLASHER_OFF()   iop_clear(IO_C, 0x04)

#define ARGOS_ON()      iop_set(IO_C, 0x08)
#define ARGOS_OFF()     iop_clear(IO_C, 0x08)

#define MIN(a, b)       ((a) < (b) ? (a) : (b))

typedef enum {
    AC_NONE=0,
    AC_WAIT=1,
    AC_CONTINUE=2,
    AC_SAFE=4,
    AC_RESTART=8,
    AC_RECOVER=16,
    AC_ABORT=32,
    AC_TIMEOUT=64
} action_t;

static int irsq = 0;

/*
** Tunable parameters. All timeouts are in seconds.
*/
static short comm_checkreg = 1;
static short comm_waiting = 0;
static long comm_reg_timeout = 120;
static long comm_wait_timeout = 7200;
static long comm_timeout = 1000;
static long comm_gps_timeout = 120;
static long comm_recovery_interval = 3600;
static long comm_wait_interval = 300;
static short comm_status_throttle = 0;
static short file_queue_len = 0;
static short comm_gps_fixes = 5;
static long comm_max_disk_errors = 10;
static long safe_mode_duration = 0;
static long safe_mode_interval = 0;
static short comm_postreg_delay = 10;

INITFUNC(init_comm_params)
{
    add_param("comm:wait_timeout",      PTYPE_LONG, &comm_wait_timeout);
    add_param("comm:recovery_interval", PTYPE_LONG, &comm_recovery_interval);
    add_param("comm:gps_timeout",       PTYPE_LONG, &comm_gps_timeout);
    add_param("comm:status_throttle",   PTYPE_SHORT, &comm_status_throttle);
    add_param("comm:checkreg",          PTYPE_SHORT, &comm_checkreg);
    add_param("comm:reg_timeout",       PTYPE_LONG, &comm_reg_timeout);
    add_param("comm:timeout",           PTYPE_LONG, &comm_timeout);
    add_param("comm:wait_interval",     PTYPE_LONG, &comm_wait_interval);
    add_param("comm:waiting",           PTYPE_SHORT, &comm_waiting);
    add_param("comm:gps_fixes",         PTYPE_SHORT, &comm_gps_fixes);
    add_param("comm:max_disk_errors",   PTYPE_LONG, &comm_max_disk_errors);
    add_param("comm:postreg_delay",     PTYPE_SHORT, &comm_postreg_delay);
    add_param("fileq",                  PTYPE_SHORT|PTYPE_READ_ONLY,
              &file_queue_len);
}


static int
rudics_login(const char *prefix, const char *pn, snumber_t *irsn)
{
    snumber_t   sn;
    char        buf[64];

    if(irsn != NULL)
        strncpy(sn.data, irsn->data, sizeof(snumber_t)-1);
    else
        iridium_get_sn(&sn);

    /* Sanity check */
    if(strlen(prefix)+strlen(pn) > sizeof(buf))
        return 0;
    sprintf(buf, "%s%s", prefix, pn);

    if(iridium_chat("AT+CBST=71,0,1\r", "OK", 6L) != 1)
        return 0;

    log_event("Dialing RUDICS %s\n", buf);

    if(iridium_try_dial(buf, 60L))
    {
        log_event("Connected\n");
        if(iridium_chat(NULL, "irsn: |NO CARRIER", 30L) == 1)
        {
            sprintf(buf, "%s\n", sn_to_str(sn));
            if(iridium_chat(buf, "?|NO CARRIER", 30L) == 1)
                return 1;
            else
                log_error("comm", "No shell prompt\n");
        }
        else
            log_error("comm", "No IRSN prompt from server\n");
    }

    log_error("comm", "Dialing error (pn = %s)\n", pn);
    return 0;
}


/*
 * write_status_file - write status message to a file
 * @type: message type, "status" or "recover".
 * @gdp: latest GPS position.
 *
 * Create an XML status message file to be sent to the remote system.
 * Returns 1 if successful, 0 if file creation fails.
 */
static int
write_status_file(char *type, GPSdata *gdp)
{
    int         send_counts;
    float       lat, lon;
    time_t      t;
    long        size, dfree;
    FILE        *ofp, *mfp;
    struct tm   *tm;
    battery_state_t     batt[2];

    if((ofp = fopen("status.xml", "w")) == NULL)
    {
        log_error("comm", "Cannot open status file\n");
        return 0;
    }

    send_counts = strcmp(type, "status") ? 0 : 1;

    pdcfinfo("A:", &size, &dfree);

    t = time(0);
    tm = localtime(&t);

    /*
    ** Current date/time
    */
    fprintf(ofp, "<%s>", type);
    fprintf(ofp, "<date>%d-%02d-%02d %02d:%02d:%02d</date>",
                     tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday,
                     tm->tm_hour, tm->tm_min, tm->tm_sec);

    /*
    ** GPS location and satellite count.
    */
    lat = gdp->lat.deg;
    lat += (((float)gdp->lat.min + ((float)gdp->lat.frac*1e-4))*0.0166667);
    if(gdp->lat.dir == 'S')
        lat *= -1.0;

    lon = gdp->lon.deg;
    lon += (((float)gdp->lon.min + ((float)gdp->lon.frac*1e-4))*0.0166667);
    if(gdp->lon.dir == 'W')
        lon *= -1.0;

    fprintf(ofp, "<gps nsats='%d' fmt='deg'>%.6f/%.6f</gps>",
            gdp->satellites, lat, lon);
    if(gdp->status == 0)
        mq_add("Questionable GPS fix");

    /* Iridium signal quality */
    fprintf(ofp, "<irsq>%d</irsq>", irsq);

    /* Free disk space */
    fprintf(ofp, "<df>%ld</df>", dfree);

    /*
    ** Relative humidity.
    */
    if(rh_init())
    {
        short   r;

        while(!rh_dev_ready())
            ;
        r = rh_read_data();
        rh_shutdown();

        fprintf(ofp, "<rh>%.1f</rh>", rh_mv_to_percent(r));
    }
    else
        mq_add("RH sensor failure");

    /*
    ** Internal pressure.
    */
    if(ipr_init())
    {
        short   r;

        while(!ipr_dev_ready())
            ;
        r = ipr_read_data();
        ipr_shutdown();

        fprintf(ofp, "<ipr>%.1f</ipr>", ipr_mv_to_psi(r));
    }
    else
        mq_add("IPR sensor failure");

    /*
    ** Battery voltages.
    */
    read_battery(BATTERY_12v, 10, &batt[0]);
    read_battery(BATTERY_15v, 10, &batt[1]);
    fprintf(ofp, "<v>%d/%d</v>", (int)(batt[0].v*1000),
                     (int)(batt[1].v*1000));

    /*
    ** Accumulated piston positioning error.
    */
    fprintf(ofp, "<ep>%ld</ep>",
                     get_param_as_int("piston_error"));

    /*
    ** Include "alert" messages.
    */
    if((mfp = fopen(MESSAGE_FILE, "r")) != NULL)
    {
        int     c;

        while((c = fgetc(mfp)) != EOF)
            fputc(c, ofp);
        fclose(mfp);
    }

    fprintf(ofp, "</%s>\n", type);
    fclose(ofp);

    return 1;
}

int
write_short_status(char *type, GPSdata *gdp, char *buf)
{
    char        *p;
    float       lat, lon;
    time_t      t;
    struct tm   *tm;
    battery_state_t     batt[2];

    p = buf;

    t = time(0);
    tm = localtime(&t);

    /*
    ** Current date/time
    */
    p += sprintf(p, "<%s>", type);
    p += sprintf(p, "<date>%d-%02d-%02d %02d:%02d:%02d</date>",
                 tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday,
                 tm->tm_hour, tm->tm_min, tm->tm_sec);
    /*
    ** GPS location and satellite count.
    */
    lat = gdp->lat.deg;
    lat += (((float)gdp->lat.min + ((float)gdp->lat.frac*1e-4))*0.0166667);
    if(gdp->lat.dir == 'S')
        lat *= -1.0;

    lon = gdp->lon.deg;
    lon += (((float)gdp->lon.min + ((float)gdp->lon.frac*1e-4))*0.0166667);
    if(gdp->lon.dir == 'W')
        lon *= -1.0;

    p += sprintf(p, "<gps nsats='%d' fmt='deg'>%.6f/%.6f</gps>",
            gdp->satellites, lat, lon);

    if(gdp->status == 0)
        p += sprintf(p, "<alert>Questionable GPS fix</alert>");

    /*
    ** Battery voltages.
    */
    read_battery(BATTERY_12v, 10, &batt[0]);
    read_battery(BATTERY_15v, 10, &batt[1]);
    p += sprintf(p, "<v>%d/%d</v>", (int)(batt[0].v*1000),
                 (int)(batt[1].v*1000));

    p += sprintf(p, "<alert>Disk write error</alert>");

    p += sprintf(p, "</%s>\n", type);
    *p = '\0';

    return (int)(p - buf);
}

/*
 * login - power-on iridium and login to remote system
 */
static int
login(snumber_t *irsn)
{
    long        t0;
    int         reg;

    PET_WATCHDOG();

    if(!iridium_init(2400L))
    {
        log_error("comm", "Cannot initialize modem\n");
        return 0;
    }

    /*
     * Wait for the modem to register with the network.
     */
    if(comm_checkreg)
    {
        t0 = RtcToCtm();
        do
        {
            reg = iridium_get_reg();
            PET_WATCHDOG();
        } while(reg != 1 && (RtcToCtm() - t0) < comm_reg_timeout);

        if(reg != 1)
        {
            log_error("comm", "Cannot register with network (%d)\n", reg);
            iridium_shutdown();
            return 0;
        }
    }

    /*
    ** A delay between registering and attempting a call seems to
    ** improve the odds that the call will go through.
    */
    DelayMilliSecs(comm_postreg_delay*1000L);

    /* Save the iridium signal quality value */
    irsq = iridium_get_sq();
    log_event("iridium SQ = %d\n", irsq);

    PET_WATCHDOG();

    if(!rudics_login(RUDICS_PREFIX, RUDICS_PN, irsn) &&
       !rudics_login(RUDICS_PREFIX, RUDICS_PN, irsn))
    {
        return 0;
    }

    PET_WATCHDOG();

    return 1;
}

static void
logout(void)
{
    iridium_shutdown();
}

/*
 * send_file - send a file to the remote system.
 * @fname: file name
 *
 * Returns 1 on success, 0 on failure.
 */
static int
send_file(const char *fname)
{
    long        bytes;
    char        xmcmd[MAX_RX_COMMAND];
    char        response[MAX_RX_RESPONSE];

    if(!fileexists(fname))
        return 1;

    log_event("Uploading file %s\n", fname);
    sprintf(xmcmd, RX_TEMPLATE, fname);
    sprintf(response, RX_RESP_TEMPLATE, fname);
    if(iridium_chat(xmcmd, response, 30L) == 1)
    {
        if((bytes = iridium_put_file(fname)) == 0)
            return 0;
        log_event("File sent (%ld bytes)\n", bytes);
       return (iridium_chat(NULL, "?|NO CARRIER", 30L) == 1);
    }
    else
        log_error("file-send", "Cannot start file transfer\n");

    return 0;
}

/*
 * send_dir_listing - send directory listing to remote system.
 */
static void
save_dir_listing(void)
{
    char        *dirbuf, *p;
    FILE        *ofp;
    time_t      now = RtcToCtm();
    struct tm   *t;
    long        n_bytes, i;
    int         c;
    char        cmd[32];

    if((dirbuf = malloc(DIRBUF_SIZE)) == NULL)
    {
        log_error("comm", "Cannot allocate memory for directory buffer\n");
        return;
    }

    CPU_SET_SPEED(16000000L);
    /*
    ** Use the /m flag with the PicoDOS DIR command to dump the directory
    ** listing into memory.  Each directory entry is a fixed size string
    ** containing the filename and filesize separated by spaces.
    */
    sprintf(cmd, "dir *.* /m %lx", (long)dirbuf);
    execstr(cmd);
    for(p = dirbuf;*p;p += DIR_ENTRY)
        ;
    n_bytes = (long)(p - dirbuf);

    log_event("Storing dir listing (%ld bytes)\n", n_bytes);
    if((ofp = fopen("00index", "w")) != NULL)
    {
        t = localtime(&now);
        fprintf(ofp, "%4d-%02d-%02d %02d:%02d:%02d\n",
                t->tm_year+1900, t->tm_mon+1, t->tm_mday,
                t->tm_hour, t->tm_min, t->tm_sec);

        /* Write the dir listing, converting NULs to linefeeds */
        p = dirbuf;
        for(i = 0;i < n_bytes;i++)
        {
            c = *p;
            fputc(c == '\0' ? '\n' : c, ofp);
            p++;
        }
        fclose(ofp);
    }
    CPU_RESET_SPEED();

    free(dirbuf);
}

/*
 * send_status - send status message to the remote system
 * @type: message type, "status" or "recover".
 * @gdp: gps position data.
 *
 * Build a status message and send it to the remote system.  First try to
 * build a full size message in a file, if that fails, an abbreviated message
 * is built in RAM. Returns non-zero if succesful and zero on failure.
 */
static int
send_status(char *type, GPSdata* gdp)
{
    static char status_buf[512];
    static int  status_sent = 0;
    static int  disk_error_counter = 0;
    int         n, r, c, i;
    FILE        *ifp;

    /*
     * The 'status_sent' flag provides a way to throttle the sending of
     * status messages while the float is waiting on the surface.
     */
    if(comm_waiting && status_sent)
    {
        status_sent--;
        return 1;
    }

    r = 0;
    if(write_status_file(type, gdp))
    {
        if(disk_error_counter > 0)
            disk_error_counter--;

        iridium_write("status\napplication/xml\n", 23L);
        if((ifp = fopen("status.xml", "r")) != NULL)
        {
            while((c = fgetc(ifp)) != EOF)
            {
                status_buf[0] = c;
                iridium_write(status_buf, 1L);
                PET_WATCHDOG();
                DelayMilliSecs(10L);
            }
            status_sent = comm_status_throttle;
            r = 1;
            fclose(ifp);
        }

    }
    else
    {
        /*
         * If we could not write the status file, build a short status
         * message in memory and send that.
         */
        disk_error_counter++;
        mq_add("Disk write failed");
        if(disk_error_counter >= comm_max_disk_errors)
            mq_add("Aborting mission");

        n = write_short_status(type, gdp, status_buf);
        iridium_write("status\napplication/xml\n", 23L);
        for(i = 0;i < n;i++)
        {
            iridium_write(&status_buf[i], 1L);
            PET_WATCHDOG();
            DelayMilliSecs(10L);
        }
        status_sent = comm_status_throttle;
        r = 1;
    }

    /*
    ** If we are having disk write errors, the float will not be able
    ** to upload commands from shore. All we can do is abort the mission
    ** and hope the reset fixes the problem...
    */
    if(disk_error_counter >= comm_max_disk_errors)
        abort_mission(0);

    log_event("Status sent, waiting for prompt\n");
    return (iridium_chat(NULL, "?|NO CARRIER", 20L) == 1);
}

/*
 * send_telem - send the telemetry file to the remote system.
 *
 * The term "telemetry file" is historical and refers to the subsampled
 * environmental data file. Returns 1 if succesful, 0 on failure.
 */
static int
send_telem(void)
{
    if(!fileexists("telem.ncz"))
        return 1;

    if(send_file("telem.ncz"))
    {
        unlink("telem.ncz");
        return 1;
    }

    log_error("comm", "File send failed (telem.ncz)\n");

    return 0;
}


/*
 * send_data - send the next data file to the remote system.
 *
 * Remove the next file from the queue (oldest file) and try to send it to
 * the remote system. Returns 1 if successful or 0 on failure.
 */
static int
send_data(void)
{
    return fq_send(send_file, 0);
}


static void
serial_text(int sdev, const char *text, ...)
{
    va_list     args;
    static char outbuf[80];

    va_start(args, text);
    vsnprintf(outbuf, sizeof(outbuf), text, args);
    serial_write_zstr(sdev, outbuf);
    va_end(args);
}

static void
serial_write_param(int sdev, char *name, struct param *p)
{
    int         type;

    type = p->type & ~PTYPE_SIZEMASK;

    switch(type & PTYPE_TYPEMASK)
    {
        case PTYPE_DOUBLE:
            serial_text(sdev, "%s=%.7f\n", name, *((double*)p->loc));
            break;
        case PTYPE_LONG:
            serial_text(sdev, "%s=%ld\n", name, *((long*)p->loc));
            break;
        case PTYPE_SHORT:
            serial_text(sdev, "%s=%d\n", name, *((short*)p->loc));
            break;
        case PTYPE_STRING:
            serial_text(sdev, "%s='%s'\n", name, (char*)p->loc);
            break;
        default:
            break;
    }

}

/*
 * process_command - process the command string from shore
 * @sdev: serial device
 * @line: command string
 */
static action_t
process_command(int sdev, char *line, int allow_abort)
{
    int         n, limit;
    long        larg1, larg2;
    char        *args_p, *val_p;
    action_t    ac = AC_NONE;
    const char  *src;
    char         *dst;
    long        start_time = get_param_as_int("start_time");
    char        fname[16];
    struct param        p;

    /* Split the line into a command and arguments */
    args_p = line;
    while(isalnum(*args_p) || *args_p == '-' || *args_p == '_')
        args_p++;
    if(*args_p != '\0')
    {
        *args_p = '\0';
        args_p++;
    }

    if(!strcmp(line, "flasher-on") ||
       (!strcmp(line, "flasher") && !strcmp(args_p, "on")))
    {
        FLASHER_ON();
        serial_write_zstr(sdev, "Activating LED flasher\n");
    }
    else if(!strcmp(line, "flasher-off") ||
            (!strcmp(line, "flasher") && !strcmp(args_p, "off")))
    {
        FLASHER_OFF();
        serial_write_zstr(sdev, "Deactivating LED flasher\n");
    }
    else if(!strcmp(line, "abort"))
    {
        if(allow_abort > 0)
        {
            ac |= AC_ABORT;
            serial_write_zstr(sdev, "Mission will abort\n");
        }
        else
            serial_write_zstr(sdev, "Abort command not allowed in this context\n");
    }
    else if(!strcmp(line, "safe"))
    {
        if(args_p && sscanf(args_p, "%ld %ld", &larg1, &larg2) == 2)
        {
            if(larg1 > 0 && larg2 > 0)
            {
                ac |= AC_SAFE;
                safe_mode_duration = larg1;
                safe_mode_interval = larg2;
                serial_text(sdev, "Float will enter SAFE mode (duration=%ld, interval=%ld)\n",
                            safe_mode_duration, safe_mode_interval);
            }
            else
                serial_text(sdev, "Invalid SAFE mode parameters (%ld, %ld)\n", larg1, larg2);
        }
        else
            serial_write_zstr(sdev, "Malformed \"safe\" command\n");

    }
    else if(!strcmp(line, "restart"))
    {
        ac |= AC_RESTART;
        serial_write_zstr(sdev, "Mission will restart\n");
    }
    else if(!strcmp(line, "recover"))
    {
        ac |= AC_RECOVER;
        serial_write_zstr(sdev, "Float will enter RECOVERY mode\n");
    }
    else if(!strcmp(line, "wait"))
    {
        ac |= AC_WAIT;
        ac &= ~AC_CONTINUE;
        serial_write_zstr(sdev, "Waiting on the surface\n");
    }
    else if(!strcmp(line, "continue"))
    {
        ac |= AC_CONTINUE;
        ac &= ~AC_WAIT;
        serial_write_zstr(sdev, "Mission continuing\n");
    }
    else if(!strcmp(line, "dir"))
    {
        /*
         * Forbid directory listings after 100 days to reduce the
         * chance of a memory buffer overflow when we run the
         * PicoDOS dir command.
         */
        if((RtcToCtm() - start_time)/86400L >= 100)
        {
            serial_write_zstr(sdev, "Command not allowed\n");
        }
        else
        {
            save_dir_listing();
            n = fq_add_next("00index");
            if(n > 0)
            {
                serial_text(sdev,
                            "Adding dir listing to file queue (qlen = %d)\n", n);
                file_queue_len = n;
            }
            else
                serial_write_zstr(sdev, "Dir listing failed\n");
        }

    }
    else if(!strncmp(line, "send-file", 9L))
    {
        PET_WATCHDOG();

        /* Remove leading and trailing spaces from filename */
        src = args_p;
        dst = fname;
        n = 0;
        limit = sizeof(fname) - 1;
        while(!isspace(*src) && n < limit)
        {
            *dst++ = *src++;
            n++;
        }
        *dst = '\0';
        if(!strcmp(line, "send-file-next"))
        {
            if((n = fq_add_next(fname)) > 0)
            {
                file_queue_len = n;
                serial_text(sdev, "%s queued at front (qlen = %d)\n", fname, n);
            }
            else
                serial_text(sdev, "%s not found\n", fname);
        }
        else
        {
            if((n = fq_add(fname)) > 0)
            {
                file_queue_len = n;
                serial_text(sdev, "%s queued (qlen = %d)\n", fname, n);
            }
            else
                serial_text(sdev, "%s not found\n", fname);
        }
    }
    else if(!strcmp(line, "del"))
    {
        PET_WATCHDOG();

        /* Remove leading and trailing spaces from filename */
        src = args_p;
        dst = fname;
        n = 0;
        limit = sizeof(fname) - 1;
        while(!isspace(*src) && n < limit)
        {
            *dst++ = *src++;
            n++;
        }
        *dst = '\0';

        /* Prevent removing .run files, everything else is ok. */
        dst = strrchr(fname, '.');
        if(dst && strcmp(dst, ".run") && fileexists(fname))
        {
            unlink(fname);
            serial_text(sdev, "%s deleted\n", fname);
        }
        else
            serial_text(sdev, "Cannot delete %s\n", fname);

    }
    else if(!strcmp(line, "get-errors"))
    {
        /*
         * Copy error messages to a compressed file and queue
         * for sending.
         */
        larg1 = 0;
        sscanf(args_p, "%ld", &larg1);
        log_event("Extracting log error messages\n");
        log_copy_errors("errors.txt", larg1);
        file_queue_len = fq_add_next("errors.txt");
        serial_write_zstr(sdev, "Errors file queued for upload\n");
    }
    else if(!strcmp(line, "set"))
    {
        val_p = strchr(args_p, ' ');
        if(val_p)
        {
            *val_p = '\0';
            val_p++;
        }

        if(set_param_str(args_p, val_p) && get_param(args_p, &p))
            serial_write_param(sdev, args_p, &p);
        else
            serial_text(sdev, "Unknown parameter '%s'\n", args_p);

    }
    else if(!strcmp(line, "get"))
    {
        if(get_param(args_p, &p))
            serial_write_param(sdev, args_p, &p);
        else
            serial_text(sdev, "Unknown parameter '%s'\n", args_p);
    }
    else if(!strcmp(line, "release"))
    {
        larg1 = 0;
        sscanf(args_p, "%ld", &larg1);
        log_event("Releasing syntactic foam\n");
        iop_set(IO_B, 0x08);
        if(larg1 > 0 && larg1 <= 10)
            DelayMilliSecs(larg1*1000L);
        else
            DelayMilliSecs(2000L);
        iop_clear(IO_B, 0x08);
        serial_write_zstr(sdev, "Syntactic foam released\n");
    }
    else if(!strcmp(line, "dump"))
    {
        FILE *pfp;

        /* Dump the parameter table to a file */
        if((pfp = fopen("params.xml", "a")) != NULL)
        {
            PET_WATCHDOG();
            dump_params(pfp);
            fclose(pfp);
        }
        serial_write_zstr(sdev, "Parameters dumped to params.xml\n");
    }
    else
        serial_text(sdev, "Unknown command '%s'\n", line);


    return ac;
}

static __inline__ int sgetc_timed(int desc, ulong start, long timeout)
{
    unsigned char       c;

    while(serial_read(desc, &c, 1L) != 1L)
    {
        if((MilliSecs() - start) >= timeout)
            return -1;
    }
    return ((int) c)&0xff;
}

/*
 * s_getline - read a line from a serial device
 * @sdev: serial device number
 * @timeout: timeout in seconds
 * @linebuf: buffer for returned data
 * @n: maximum number of characters to read
 * @return 1 on success, 0 on timeout.
 *
 * Read the next linefeed terminated line from a serial device. The input
 * characters (minus the trailing linefeed) are written to the buffer and
 * terminated with a '\0'.
 */
static int
s_getline(int sdev, long timeout, char *linebuf, size_t n)
{
    long        start = MilliSecs(), ms_timeout;
    char        *p = linebuf;
    int         c, rval, text_started;

    rval = 1;
    text_started = 0;
    ms_timeout = timeout*1000L;

    while(n--)
    {
        c = sgetc_timed(sdev, start, ms_timeout);
        if(c < 0)
        {
            rval = 0;
            break;
        }
        if(c == '\n')
            break;

        if(!text_started && !isspace(c))
            text_started = 1;

        if(text_started && c != '\r')
            *p++ = c;
        if((MilliSecs() - start) > ms_timeout)
        {
            rval = 0;
            break;
        }
    }

    *p = '\0';
    return rval;
}

/*
 * command_mode - interactive command mode with shore
 * @sdev: serial device number
 */
static action_t
command_mode(int sdev, long timeout, int allow_abort)
{
    int         mode;
    action_t    ac = AC_NONE;
    static char linebuf[80];

    mode = serial_inmode(sdev, SERIAL_NONBLOCKING);

    serial_text(sdev, "msgs?\n");

    while(s_getline(sdev, timeout, linebuf, sizeof(linebuf)-1))
    {
        if(linebuf[0] == '\0')
        {
            /* Blank line signals end of input */
            serial_inmode(sdev, mode);
            return ac;
        }

        log_event("Command: \"%s\"\n", linebuf);

        ac |= process_command(sdev, linebuf, allow_abort);
    }

    ac |= AC_TIMEOUT;
    serial_inmode(sdev, mode);
    return ac;
}


/*
 * exec_commands - fetch and interpret commands from the remote system
 * @allow_abort: if non-zero, allow 'abort' commands.
 *
 * Returns 1 of success, 0 on failure.
 */
static int
exec_commands(int allow_abort)
{
    action_t    ac;

    ac = command_mode(iridium_serialdev(),
                      30L,
                      allow_abort);
    if(ac & AC_WAIT)
    {
        comm_waiting = 1;
    }
    else if(ac & AC_CONTINUE)
    {
        comm_waiting = 0;
    }
    else if(ac & AC_SAFE)
    {
        enter_safe_mode(safe_mode_duration, safe_mode_interval);
    }
    else if(ac & AC_RESTART)
    {
        restart_sys();
    }
    else if(ac & AC_RECOVER)
    {
        enter_recovery_mode();
    }
    else if(ac & AC_ABORT)
    {
        abort_mission(1);
    }

    return (ac & AC_TIMEOUT) ? 0 : 1;
}

/**
 * comm_mode - communication procedure
 * @type: status message type ("status" or "recover")
 * @gps_on: if != 0, GPS is already powered-on.
 * @irsn: if non-NULL, supplies the Iridium Serial Number to use to login.
 *
 * This function runs the communication procedure with the remote system on
 * shore through the Iridium modem. The procedure is as follows:
 *
 *    1. Read latest GPS position.
 *    2. Power-on iridium modem and login to remote system.
 *    3. Create and upload status message.
 *    4. Download and execute command file.
 *    5. Upload reply file from step #4
 *    6. Upload subsampled environmental data file.
 *    7. Upload next data file from file queue.
 *
 * If a communication failure occurs during steps #3-#7, the modem is
 * powered-off and control is passed to step #2 and then back to the step
 * where the failure occured. The procedure exits when all steps have been
 * performed or when the process times out. Timeout is set with the
 * "comm:timeout" parameter.
 *
 * If the float is "waiting" (comm:waiting is set), the above seven steps are
 * repeated until comm:wait_timeout expires. Another "wait mode" change is
 * that all intervening steps are re-executed if a communication failure
 * occurs (i.e. there is no jump back to the step where the failure
 * occurs). This is to prevent the float getting "stuck" trying to send a
 * large data file and never getting around to sending status updates or
 * checking for commands.
 */
void
comm_mode(char *type, int gps_on, snumber_t *irsn)
{
    long                t0, timeout, login_time, dt;
    int                 allow_abort, first_fix = 1;
    long                login_wait, login_max_wait = 24000;
    double              backoff_factor = 1.4142;
    GPSdata             gd;
    char                pbuf[32];
    enum {ST_LOGIN=0,
          ST_STATUS,
          ST_CMD,
          ST_TELEM,
          ST_DATA,
          ST_GPS,
          ST_DONE} comm_state, next_state;

    ARGOS_ON();
    if(!xcat_setup(pbuf, 31))
        xcat_setup(pbuf, 31);

    comm_state = ST_GPS;
    next_state = ST_STATUS;
    login_time = 0;

    t0 = RtcToCtm();
    if(comm_waiting)
    {
        timeout = comm_wait_timeout;
        mq_add("Waiting on surface");
    }
    else
        timeout = comm_timeout;

    if(!strcmp(type, "recover"))
        allow_abort = 0;
    else
        allow_abort = 1;

    if(fileexists("telem.nc"))
    {
        if(compress_file("telem.nc", "telem.ncz"))
            unlink("telem.nc");
    }

    if(fileexists("dataql.sx"))
    {
        compress_file("dataql.sx", "dataql.sxz");
        unlink("dataql.sx");
    }

    file_queue_len = fq_len();

    /* Wait 8000ms before retrying to login */
    login_wait = 8000L;

    while(comm_state != ST_DONE && (RtcToCtm() - t0) < timeout)
    {
        PET_WATCHDOG();

        switch(comm_state)
        {
            case ST_GPS:
                logout();
                memset(&gd, 0, sizeof(gd));
                if(!gps_on)
                    gps_on = gps_init();

                if(!gps_on)
                {
                    log_error("comm", "GPS initialization failed\n");
                    mq_add("GPS failed");
                }
                else
                {
                    sens_open_data_file(GPS_FILE, 0);
                    sens_wait_for_gps(&gd, comm_gps_timeout, 1, comm_gps_fixes);
                    sens_write_gps_fix(&gd);
                    sens_close_file(GPS_FILE);
                    gps_shutdown();
                    gps_on = 0;
                }

                /*
                ** Reset the timer after the first GPS fix so the GPS wait time
                ** doesn't affect the COMM-mode timeout.
                */
                if(first_fix)
                {
                    t0 = RtcToCtm();
                    first_fix = 0;
                }

                if(comm_waiting)
                    timeout = comm_wait_timeout;
                else
                    timeout = comm_timeout;
                comm_state = ST_LOGIN;
                break;
            case ST_LOGIN:
                /* power-on iridium and login */
                logout();
                if(comm_waiting)
                {
                    /*
                     * Throttle the login rate during wait mode to reduce
                     * power consumption.
                     */
                    dt = login_time + comm_wait_interval - RtcToCtm();
                    if(dt > 0)
                    {
                        log_event("Sleeping until next login time (%ld seconds)\n", dt);
                        while(dt > 0)
                        {
                            /* Sleep in 30s chunks so we can service the watchdog */
                            PET_WATCHDOG();
                            if(isleep(MIN(dt, 30)) == 1)
                            {
                                log_event("Got abort signal\n");
                                abort_prog();
                            }
                            dt -= 30;
                        }
                    }
                }

                if(login(irsn) == 0)
                {
                    /*
                     * If the login process fails and we are waiting, set the
                     * next state to ST_GPS so we can update the GPS position,
                     * otherwise apply an exponential backoff to the login-delay
                     * and try again.
                     */
                    if(comm_waiting)
                        comm_state = ST_GPS;
                    else
                    {
                        comm_state = ST_LOGIN;
                        DelayMilliSecs(login_wait);
                        /* Exponential back-off */
                        login_wait = (double)login_wait*backoff_factor;
                        if(login_wait > login_max_wait)
                            login_wait = login_max_wait;
                    }
                }
                else
                {
                    /*
                     * If we are waiting, force a pass through all of the
                     * intermediate states rather than a jump to the
                     * saved 'next_state'. This insures that we don't get
                     * "stuck" trying to send a large data file and thus
                     * never get a chance to send an updated status message
                     * or to check for a command file.
                     */
                    if(comm_waiting)
                        next_state = ST_STATUS;
                    login_time = RtcToCtm();
                    /* Reset login wait time to 8 seconds */
                    login_wait = 8000;
                    comm_state = next_state;
                }
                break;
            case ST_STATUS:
                log_event("Sending status information\n");
                /* send status message */
                next_state = comm_state;

                if(send_status(type, &gd) == 0)
                    comm_state = ST_LOGIN;
                else
                    comm_state = ST_CMD;
                break;
            case ST_CMD:
                log_event("Checking for commands\n");
                /* fetch commands */
                next_state = comm_state;
                if(exec_commands(allow_abort) == 0)
                    comm_state = ST_GPS;
                else
                    comm_state = ST_TELEM;
                /*
                 * comm_waiting may have been set by the command file so
                 * we need to check it and adjust the timeout if
                 * necessary.
                 */
                if(comm_waiting)
                    timeout = comm_wait_timeout;
                else
                    timeout = comm_timeout;
                break;
            case ST_TELEM:
                /* send telemetry file */
                next_state = comm_state;
                if(send_telem() == 0)
                    comm_state = ST_GPS;
                else
                    comm_state = ST_DATA;
                break;
            case ST_DATA:
                /* send data file */
                next_state = comm_state;
                if(send_data() == 0)
                    comm_state = ST_GPS;  /* Connection dropped */
                else
                {
                    /*
                     * File transfer was successful, connection is ok. Try to send
                     * the next file unless we are in wait-mode and it has been more
                     * than comm_wait_interval seconds since our last GPS check in
                     * which case we log-out to get a new fix.
                     */
                    if(comm_waiting &&
                       (RtcToCtm() - login_time) > comm_wait_interval)
                        comm_state = ST_GPS;
                    else
                    {
                        if(fq_len() > 0)
                            comm_state = ST_DATA;
                        else
                            comm_state = comm_waiting ? ST_GPS : ST_DONE;
                    }

                }
                break;
            case ST_DONE:
                break;
        }

    }

    if(comm_state != ST_LOGIN && comm_state != ST_GPS)
    {
        log_event("Exiting shell\n");
        iridium_write("bye\n", 4L);
    }

    logout();


    if(fileexists("telem.ncz"))
    {
        int     i;
        char newname[12];

        /*
         * We could not send the quick-look file, rename it and
         * queue it for later delivery.
         */
        for(i = 0;i < 100;i++)
        {
            sprintf(newname, "telem%02d.ncz", i);
            if(!fileexists(newname))
            {
                rename("telem.ncz", newname);
                fq_add(newname);
                break;
            }
        }

        if(fileexists("telem.ncz"))
        {
            log_error("comm", "Too many quick-look files saved\n");
            unlink("telem.ncz");
        }

    }

    if(fileexists(MESSAGE_FILE))
        unlink(MESSAGE_FILE);

    if(gps_on)
        gps_shutdown();

    /*
     * Make sure flasher is not left active unless we are
     * in recovery mode.
     */
    if(strcmp(type, "recover"))
    {
        ARGOS_OFF();
    }

}

/**
 * comm_wait - comm-mode with a forced wait on the surface.
 * @msg: extra message to send
 * @gps_on: if non-zero, GPS is slready powered-on.
 *
 */
void
comm_wait(int gps_on)
{
    comm_waiting = 1;
    comm_mode("status", gps_on, NULL);
}

void
comm_recovery_mode(void)
{
    long        last_time = 0L, dt;

    comm_waiting = 0;

    while(1)
    {
        PET_WATCHDOG();
        /*
         * Wait in low-power mode for the next communication interval. While
         * waiting, wake every 60 seconds and print a prompt to the console to
         * allow the user to interrupt the wait and perform an orderly restart
         * of the system.
         */
        dt = comm_recovery_interval - (RtcToCtm() - last_time);

        while(dt > 0)
        {
            PET_WATCHDOG();
            fputs("Waiting to send RECOVERY message\n", stdout);
            if(isleep(MIN(dt, 60L)) == 1)
            {
                fputs("Control program will now restart\n", stdout);
                abort_prog();
            }
            dt = comm_recovery_interval - (RtcToCtm() - last_time);
        }

        last_time = RtcToCtm();
        comm_mode("recover", 0, NULL);

    }

}
