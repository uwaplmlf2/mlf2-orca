/*
** $Id: mission.c,v fefe2f7ac288 2008/08/03 23:55:18 mikek $
**
** MLF2 mission control program.
**
**
*/
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <errno.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include "ioports.h"
#include "lpsleep.h"
#include "log.h"
#include "nvram.h"
#include "gps.h"
#include "motor.h"
#include "ptable.h"
#include "util.h"
#include "mtype.h"
#include "ballast.h"
#include "sample.h"
#include "comm.h"
#include "mctl.h"
#include "sensors.h"
#include "picadc.h"
#include "newpr.h"
#include "abort.h"
#include "version.h"
#include "libversion.h"
#include "ballastvers.h"

#define MK_ID() "$Id: " ## REVISION ## " " ## __DATE__ ## " " ## __TIME__ ## " $"

static const char *__id__ = MK_ID();

#define FLASHER_ON()    iop_set(IO_C, 0x04)
#define FLASHER_OFF()   iop_clear(IO_C, 0x04)

/* Number of seconds to delay before entering armed-mode */
#define COUNTDOWN       60

/* Default number of seconds to wait for activation depth */
#define DEPTH_WAIT_TIMEOUT      (12L*3600L)

#define NV_ISSET(name, nvp)     (nv_lookup(name, (nvp)) != -1 && (nvp)->l != 0)

#ifdef __GNUC__
static void bailout(void) __attribute__ ((noreturn));
#endif

static long     depth_wait = DEPTH_WAIT_TIMEOUT, absolute_wait;
static long     depth_check_interval = 10L;
static long     cpu_clock = 4;
static short    profile_first = 1, abort_on_timeout, countdown = COUNTDOWN;
static short    pr_reset = 1;
static double   start_depth = 1.;

extern char     *_mtop, *_mbot, *_mcur;
extern unsigned long    _H0_org;


static void
mlf2_qspi_init(void)
{
    register unsigned   cs_mask, cntl_mask;

    cs_mask   = M_PCS0 | M_PCS1 | M_PCS2 | M_PCS3;
    cntl_mask = M_SCK | M_MOSI | M_MISO;

    /*
    ** Configure the QSPI control and chip select lines as
    ** general purpose outputs. Force all lines except CS3
    ** low. CS3 is the chip-select for the MAX186 and is
    ** active low.
    */
    *QPAR = 0;
    *QPDR = M_PCS3;
    *QDDR = (cs_mask | cntl_mask);
}

static void
bailout(void)
{
    nv_value    nv;

    PET_WATCHDOG();

    fputs("Cleaning up, please wait ... ", stderr);
    fflush(stderr);

    closelog();
    iop_clear_all();

    /* Set power-cycle flag to indicate "clean" reboot */
    nv.l = 1;
    nv_insert("power-cycle", &nv, NV_INT, 1);

    fputs("done\n", stderr);
    DelayMilliSecs(500L);

    /*
    ** The infinite loop prevents gcc from thinking the function
    ** does return even though it was declared "noreturn".
    */
    while(1)
        Reset();
}

long nr_spurious_ints;

static void
spur_int_handler(void)
{
    nr_spurious_ints++;
}

static double
check_depth(int sensor)
{
    double              pr;
    int                 n;
    pr_channel_t        chan;
    long        p[MAX_PR_SENSORS*MAX_PR_SAMPLES];

    FLASHER_ON();

    DelayMilliSecs(2000L);

    /* Reinitialize the SPI-bus interface */
    newpr_spi_init();
    n = newpr_get_count();

    if(n == 0)
    {
        DelayMilliSecs(2000L);
        n = newpr_get_count();
    }

    chan = (1 << sensor);
    n = newpr_read_data(chan, MAX_PR_SAMPLES, p);
    pr = newpr_mv_to_psi(chan,
                         newpr_counts_to_mv(p[n-1]))/1.47;

    FLASHER_OFF();

    return pr;
}

static int
wait_for_depth(double rel_dbars, long interval)
{
    double      depth, dbars;
    long        t0, timeout;
    int         nosleep = 0, pr_sensor;
    struct tm   *tm;

    t0 = RtcToCtm();
    if(absolute_wait)
    {
        tm = localtime(&depth_wait);
        log_event("ARMED mode will timeout on %4d-%02d-%02d %02d:%02d:%02d\n",
                  tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday,
                  tm->tm_hour, tm->tm_min, tm->tm_sec);

        timeout = depth_wait - t0;
    }
    else
    {
        log_event("ARMED mode will timeout in %ld seconds\n", depth_wait);
        timeout = depth_wait;
    }



    pr_sensor = get_param_as_int("primary_pr");
    log_event("Using pressure sensor %d\n", pr_sensor);
    check_depth(pr_sensor);

    /* Convert relative depth to absolute */
    dbars = rel_dbars + check_depth(pr_sensor);
    log_event("Target depth = %.3f dbars\n", dbars);

    while((depth = check_depth(pr_sensor)) < dbars)
    {
        printf("Checking depth: %.3f dbars\n", depth);

        if((RtcToCtm() - t0) > timeout)
        {
            log_error("init", "Timeout waiting for depth\n");
            return 0;
        }

        PET_WATCHDOG();

        if(nosleep)
        {
            /* busy wait */
            time_t stop;

            stop = RtcToCtm() + interval;
            while(RtcToCtm() < stop)
                ;
        }
        else
        {
            if(isleep(interval) == 1)
            {
                fputs("Mission cancelled, RESTARTing float ...\n", stdout);
                bailout();
            }
        }

    }
    log_event("Target depth reached\n");

    return 1;
}

/*
 * Try to HOME the piston "N" times.  If the operation fails, manually set
 * the piston position to "x" centimeters.
 */
void
try_home(int N, double x)
{
    while(motor_home(900L) < 0 && N-- > 0)
        log_event("motor HOME process interrupted.  %s\n",
                  (N == 0) ? "Skipping" : "Trying again");

    if(N <= 0)
    {
        log_event("Setting current motor position to %.2f cm\n", x);
        motor_set_position(cm_to_counts(x));
    }

}

INITFUNC(init_mission)
{
    InitTT8(NO_WATCHDOG, TT8_TPU);
    InitCF8(CF8StdCS, CF8StdAddr);
    if(errno != 0)
        printf("\nWARNING: InitCF8 failed, error %d\n", errno);
    if(errno == -1 || errno == PiDosNoHardware)
        exit(1);

    /*
    ** Initialize non-volatile RAM and the I/O ports
    */
    init_nvram();
    init_ioports();

    add_param("start_depth", PTYPE_DOUBLE, &start_depth);
    add_param("armed:depth", PTYPE_DOUBLE, &start_depth);
    add_param("depth_wait", PTYPE_LONG, &depth_wait);
    add_param("armed:wait", PTYPE_LONG, &depth_wait);
    add_param("armed:wait_absolute", PTYPE_LONG, &absolute_wait);
    add_param("armed:abort", PTYPE_SHORT, &abort_on_timeout);
    add_param("depth_check",    PTYPE_LONG,     &depth_check_interval);
    add_param("cpu_clock",      PTYPE_LONG,     &cpu_clock);
    add_param("countdown",      PTYPE_SHORT,    &countdown);
    add_param("pr_reset",       PTYPE_SHORT,    &pr_reset);

}

/* ANSI terminal escape sequences */
#define RED_TEXT        "\033[1;31m"
#define GREEN_TEXT      "\033[1;32m"
#define NC_TEXT         "\033[0m"

static void
extra_sensor_check(void)
{
}

int
main(void)
{
    long                mission_length;
    FILE                *pfp;
    int                 i;
    nv_value            nv[2];
    static ExcCFrame    eframe;


    /* Catch spurious interrupts */
    InstallHandler(spur_int_handler, Spurious_Interrupt, &eframe);


    mlf2_qspi_init();


    printf("\n*** MLF2 Control Program ***\n");

    /* Set motor encoder params */
#ifdef DEEP_PISTON
    motor_set_encoder(500L, 546L);
#else
    motor_set_encoder(100L, 411L);
#endif
    PET_WATCHDOG();

    rotate_logs();
    fputs("Opening new system log file ... ", stdout);
    fflush(stdout);
    openlog("syslog0.txt");
    fputs("done\n", stdout);

    /*
    ** Ensure mlf2_qspi_init is called when exiting LP-sleep mode.
    */
    init_sleep_hooks();
    add_after_hook("QSPI-init", mlf2_qspi_init);

    PET_WATCHDOG();

    log_event("Software revision: %s\n", REVISION);
    log_event("Library revision: %s\n", MLF2LIB_REVISION);
    log_event("Ballasting code revision: %s\n", BALLAST_REVISION);
    log_event("Mission type: %s\n", MISSIONTYPE);
    MFUNC;

    printf("Checking Sensors:\n");
    sens_check_all();
    extra_sensor_check();

    PET_WATCHDOG();

    /*
    ** Use GPS to set system clock.
    */
    if(gps_init())
    {
        gps_set_clock();
        gps_shutdown();
    }

    PET_WATCHDOG();

    /* Dump the parameter table to a file */
    if((pfp = fopen("params.xml", "w")) != NULL)
    {
        dump_params(pfp);
        fclose(pfp);
    }

    PET_WATCHDOG();


    /*
    ** Open mission control file, load and display the parameters.
    */
    if((mission_length = mission_read("mission.xml")) <= 0)
    {
        log_error("init", "Mission duration is not set\n");
        goto done;
    }

    log_event("Mission duration %ld seconds\n", mission_length);

    /*
    ** Read "extra" parameters.
    */
    params_read("metadata.xml");


    /*
    ** Try moving the piston to its HOME position.  Limit the number
    ** of attempts in case something has gone very wrong with the
    ** motor.
    */
    try_home(10, (double)15.);

    PET_WATCHDOG();

    if(cpu_clock > 16 || cpu_clock <= 1)
        cpu_clock = 16;
    log_event("Setting CPU clock to %ld mhz\n", cpu_clock);
    SimSetFSys(cpu_clock*1000000L);
    SerSetBaud(9600L, 0L);

    newpr_init();

    while(countdown > 0)
    {
        printf("\rCountdown to start (press any key to stop):  %2d  ",
               countdown);
        countdown--;
        fflush(stdout);
        if(SerByteAvail())
        {
            while(SerByteAvail())
                SerGetByte();
            goto done;
        }
        DelayMilliSecs(960L);
    }
    fputs("\n", stdout);

    for(i = 0;i < pr_reset;i++)
    {
        log_event("Reseting pressure board A/D\n");
        newpr_adreset();
        DelayMilliSecs(1500L);
    }

    fputs("Setting NVRAM flags ... ", stdout);
    fflush(stdout);

    /*
    ** Set power-cycle flag to -1 and saved piston position to 0.
    */
    nv[0].l = -1L;
    nv_insert("power-cycle", &nv[0], NV_INT, 1);

    nv[0].l = 0;
    nv_insert("piston", &nv[0], NV_INT, 1);

    nv[0].l = 0;
    nv_insert("force-abort", &nv[0], NV_INT, 1);
    nv_write();

    fputs("done\n", stdout);

    newpr_clear();

    /*
    ** At this point, the float is committed to the mission in the
    ** sense that if the system is reset it will re-boot into
    ** this program.
    **
    ** A mission normally starts with a down profile (profile_first is
    ** true) but it can optionally start in "error mode" to force the
    ** float to wait on the surface.
    */
    if(profile_first)
    {
        log_event("Waiting for start depth: %.2f m\n", start_depth);
        if(!wait_for_depth(start_depth, depth_check_interval) &&
           abort_on_timeout)
            abort_mission(0);
        else
        {
            /*
            ** At this stage, the float has left ARMED mode and is ready
            ** to start sampling. The power-cycle flag is set to 0 which
            ** will force the float into RECOVERY mode if it is reset.
            */
            log_event("Clearing power-cyle flag\n");
            nv[0].l = 0L;
            nv_insert("power-cycle", &nv[0], NV_INT, 0);
            nv_write();

            mlf2_main_loop(MODE_START, mission_length);
        }

    }
    else
        mlf2_main_loop(MODE_ERROR, mission_length);

done:
    bailout();

    return 0;
}
