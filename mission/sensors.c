/*
** arch-tag: mlf2 sensor interface
** Time-stamp: <2016-04-12 20:20:35 mike>
**
** Sensor interface for MLF2 sampling.  This module contains functions for
** reading the sensors and writing the various data files.
**
*/
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#ifdef __GNUC__
#include <unistd.h>
#endif
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/fcntl.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include "siod.h"
#include "ioports.h"
#include "log.h"
#include "hash.h"
#include "netcdf.h"
#include "newctd.h"
#include "motor.h"
#include "drogue.h"
#include "inertial.h"
#include "adcp.h"
#include "par.h"
#include "i490.h"
#include "alt.h"
#include "rh.h"
#include "atod.h"
#include "noise.h"
#include "flr.h"
#include "eco.h"
#include "anr.h"
#include "ctdo.h"
#include "therm.h"
#include "nvram.h"
#include "ptable.h"
#include "lpsleep.h"
#include "util.h"
#include "comm.h"
#include "sample.h"
#include "ballast.h"
#include "picadc.h"
#include "newpr.h"
#include "sensors.h"
#include "fileq.h"

#define TOP_CTD_INDEX           1
#define BOTTOM_CTD_INDEX        0

#define CTD_DELTA_PR    1.397   /* distance between CTDs in dbars */

#define MAX_PR_ADC              0xffffffL

struct file_table {
    FILE        *fp;
    const char  *basename;
    const char  *ext;
    long        n;
    int         vdim;
    long        t;
    long        max_age;
    netCDFdesc  *nd;
};


#define ABS(x)  ((x) < 0 ? -(x) : (x))
#define NINT(x) ((long)((x) + 0.5))
#define MIN(a, b)       ((a) < (b) ? (a) : (b))

/*
** Function table for the sensors we will be sampling during the
** mission.
*/
struct sensor_table {
    int         sdev;   /* For future use */
    unsigned long       mask;
    const char  *name;
    const char  *desc;
    int         (*f_init)();
    void        (*f_shutdown)();
    int         (*f_ready)();
    void        (*f_stats)();
    int         (*f_config)();
    int         (*f_check)();
};

#ifdef HAVE_ANR
static int anr_startup(void);
static void anr_clean_shutdown(void);
#endif

static struct sensor_table s_table[] = {
#ifdef HAVE_THERM
{ -1, SENS_THERM,               "therm",                "SBE-38 Thermometer",
  therm_init,   therm_shutdown, therm_dev_ready,        therm_get_stats,
  therm_cmd },
#endif
#ifdef HAVE_CTDO
{ -1, SENS_CTDO,        "ctdo",         "CTD+Oxy",
  ctdo_init,    ctdo_shutdown,  ctdo_dev_ready, NULL,  ctdo_cmd },
#endif
#ifdef HAVE_ANR
{ -1, SENS_ANR,         "anr",          "Acoustic Noise Recorder",
      anr_startup,      anr_clean_shutdown,     anr_dev_ready,  NULL,  anr_cmd, anr_init },
#endif
#if (HAVE_CTD & 1)
{ 0, SENS_CTD1,         "ctd0",         "CTD0",
  newctd_init,  newctd_shutdown,        newctd_dev_ready,       newctd_get_stats },
#endif
#if (HAVE_CTD & 2)
{ 1, SENS_CTD2,         "ctd1",         "CTD1",
  newctd_init,  newctd_shutdown,        newctd_dev_ready,       newctd_get_stats },
#endif
  };


#define SEXP_MAX_FILE_AGE       21600L

/*
** Table to manage the different types of data files which will be
** created during the mission.
*/
static struct file_table f_table[] = {
{ NULL,         "env",          "nc",   0L,     0,      0L,     21600L,         NULL },
{ NULL,         "gps",          "nc",   0L,     0,      0L,     30*86400L,      NULL },
  };


#ifdef HAVE_ANR
static int
anr_startup(void)
{
    long        interval = get_param_as_int("anr:interval");
    int         no_gtd = get_param_as_int("anr:gtdnr");

    if(!anr_init())
        return 0;

    if(!(anr_start(interval, no_gtd) || anr_start(interval, no_gtd)))
    {
        log_error("anr", "ANR $start failed\n");
        anr_shutdown();
        return 0;
    }

    return 1;
}

static void
anr_clean_shutdown(void)
{
    if(!anr_stop())
        anr_stop();

    anr_shutdown();
}

#endif
/**
 * sens_check_all - check that all sensors can be initialized
 *
 * This function checks that all sensors can be initialized and prints
 * a success or failure message to the console.
 */

/* ANSI terminal escape sequences */
#define RED_TEXT        "\033[1;31m"
#define GREEN_TEXT      "\033[1;32m"
#define NC_TEXT         "\033[0m"

void
sens_check_all(void)
{
    int i, n, r;
    int (*fn)();

    n = sizeof(s_table)/sizeof(struct sensor_table);

    /*
    ** Try to initialize all of the listed sensors.
    */
    for(i = 0;i < n;i++)
    {
        struct sensor_table     *st = &s_table[i];

        printf("Checking %s ... ", st->desc);
        fflush(stdout);
        fn = st->f_check != NULL ? st->f_check : st->f_init;

        if(st->sdev >= 0)
            r = fn(st->sdev);
        else
            r = fn();

        if(r)
        {
            printf("%s OK %s\n", GREEN_TEXT, NC_TEXT);
            if(st->sdev >= 0)
                (*st->f_shutdown)(st->sdev);
            else
                (*st->f_shutdown)();
        }
        else
            printf("%s FAILED %s\n", RED_TEXT, NC_TEXT);
    }

}

/**
 * sens_get_err_counters - fill a Hash Table with sensor error stats.
 * @ctable: table for returned data.
 *
 * Stores the error statistics for each serial sensors in hash table
 * entries indexed by the sensor name.  Each entry consists of two
 * long integers.  The first integer gives the number of read attempts
 * and the second gives the number of errors.
 */
void
sens_get_err_counters(HashTable *ctable)
{
    unsigned long       count[2];
    int                 i, n;

    n = sizeof(s_table)/sizeof(struct sensor_table);

    for(i = 0;i < n;i++)
    {
        struct sensor_table     *st = &s_table[i];

        if(st->f_stats)
        {
            if(st->sdev >= 0)
                (*st->f_stats)(st->sdev, &count[1], &count[0]);
            else
                (*st->f_stats)(&count[1], &count[0]);
            ht_insert_elem(ctable, st->name, (void*)count, 8);
        }

    }
}

/**
 * sens_initialize_sensors - initialize a set of sensors.
 * @which: sensor set bitmask
 *
 * Initialize all sensors specified in the bitmask @which. The return value
 * contains the bitmask which represents all of the sensors which were
 * successfully initialized.
 */
unsigned long
sens_initialize_sensors(unsigned long which)
{
    register unsigned long      mask = 0;
    register int        i, n;

    n = sizeof(s_table)/sizeof(struct sensor_table);

    /*
    ** Try to initialize all of the listed sensors.
    */
    for(i = 0;i < n;i++)
    {
        register struct sensor_table    *st = &s_table[i];

        if(st->sdev >= 0)
        {
            if((which & st->mask) && (*st->f_init)(st->sdev))
                mask |= st->mask;
        }
        else
        {
            if((which & st->mask) && (*st->f_init)())
                mask |= st->mask;
        }

    }

    /*
    ** Wait for the sensors to become ready.
    */
    for(i = 0;i < n;i++)
    {
        register struct sensor_table    *st = &s_table[i];

        if(mask & st->mask)
        {
            if(st->sdev >= 0)
            {
                while(!(*st->f_ready)(st->sdev))
                    ;
            }
            else
            {
                while(!(*st->f_ready)())
                    ;
            }

        }

    }

    return mask;
}

/**
 * sens_send_cmd - send an arbitrary command to a sensor
 * @which: sensor ID code
 * @cmd: command string.
 *
 * Send an arbitrary command string to a sensor.  The sensor must have
 * a configuration function defined in the sensor table.  Returns 1 if
 * successful, 0 if an error occurs.
 */
int
sens_send_cmd(unsigned long which, const char *cmd)
{
    int i, n;

    n = sizeof(s_table)/sizeof(struct sensor_table);

    for(i = 0;i < n;i++)
    {
        register struct sensor_table    *st = &s_table[i];

        if(st->sdev >= 0)
        {
            if((which & st->mask) && st->f_config)
                return (*st->f_config)(st->sdev, cmd);
        }
        else
        {
            if((which & st->mask) && st->f_config)
                return (*st->f_config)(cmd);
        }

    }

    return 0;
}

/*
 * Shutdown all sensors specified in the bitmask 'which'.
 */
void
sens_shutdown_sensors(unsigned long which)
{
    register int        i, n;

    n = sizeof(s_table)/sizeof(struct sensor_table);

    for(i = 0;i < n;i++)
    {
        register struct sensor_table    *st = &s_table[i];

        if(which & st->mask)
        {
            if(st->sdev >= 0)
                (*st->f_shutdown)(st->sdev);
            else
                (*st->f_shutdown)();
            which &= ~st->mask;
        }
    }

}

/**
 * sens_read_pressure - read the pressure sensors.
 *
 */
int
sens_read_pressure(struct sample_t *s, long *timestamp)
{
    double              psum[MAX_PR_SENSORS];
    int                 n, i, j, k, n_zeros;
    unsigned            max_bad_pressure;
    static unsigned     n_bad_pressure = 0;
    static long         p[MAX_PR_SENSORS*MAX_PR_SAMPLES];

    /* Reinitialize the SPI-bus interface */
    newpr_spi_init();

    /* Read all channels */
    s->npr = newpr_read_data(PR_ALL, MAX_PR_SAMPLES, p);
    if(s->npr == 0)
        log_error("pr", "No pressure values stored\n");

    /* Record the time of the last sample */
    s->pr_tlast = RtcToCtm();
    if(timestamp)
        *timestamp = s->pr_tlast;

    memset((void*)psum, 0, sizeof(psum));
    n_zeros = 0;

    for(i = 0,k = 0;i < s->npr*MAX_PR_SENSORS;i+=MAX_PR_SENSORS,k++)
    {
        if(p[i+s->which_pr] >= MAX_PR_ADC)
        {
            log_error("sample", "Maximum pressure A/D value\n");
            return 0;
        }

        if(p[i+s->which_pr] == 0)
            n_zeros++;

        for(j = 0;j < MAX_PR_SENSORS;j++)
        {
            s->fastP[j][k] = newpr_mv_to_psi((pr_channel_t)(1 << j),
                                             newpr_counts_to_mv(p[i+j]))/1.47;
            psum[j] += s->fastP[j][k];
        }
    }

    if(s->npr == 0 || n_zeros >= k)
    {
        log_error("pr", "Restarting pressure board\n");
        newpr_shutdown();
        DelayMilliSecs(1000L);
        newpr_init();
        n_bad_pressure++;
    }
    else
        n_bad_pressure = 0;

    for(j = 0;j < MAX_PR_SENSORS;j++)
    {
        s->P[j] = s->fastP[j][s->npr-1];
        s->Pavg[j] = psum[j]/(float)s->npr;
    }

    max_bad_pressure = get_param_as_int("pr:max_zeros");

    return (n_bad_pressure > max_bad_pressure) ? 0 : 1;
}

/*
 * Calculate the pressure values at the CTDs which depends on which pressure
 * sensor was read.
 */
void
adjust_pressure(double pr, int which, double *top_pr, double *bottom_pr)
{
    switch(which)
    {
        case PR_INDEX_HULL:     /* Hull sensor */
            *top_pr = pr + TOP_CTD_DELTA_PR;
            *bottom_pr = pr + BOT_CTD_DELTA_PR;
            break;
        case PR_INDEX_TOP: /* Top end-cap sensor */
            *top_pr = pr;
            *bottom_pr = pr + CTD_DELTA_PR;
            break;
        case PR_INDEX_BOTTOM: /* Bottom end-cap sensor */
            *bottom_pr = pr;
            *top_pr = pr - CTD_DELTA_PR;
            break;
        default:
            *top_pr = pr;
            *bottom_pr = pr;
    }
}

/**
 * sens_read_env_data - read the environmental data sensors.
 * @sensors: bitmask of active sensors
 * @s: pointer to returned data.
 *
 * Reads a single sample from all specified sensors and stores the
 * data in @s.  Return value is a timestamp for the data or -1 if
 * the pressure sensor cannot be read.
 */
long
sens_read_env_data(unsigned long sensors, struct sample_t *s)
{
    long                t;
    NewCTDdata          ctd;
    CTDOdata            ctdo;
    double              top_pr, bottom_pr;

    PET_WATCHDOG();

    /* Initialize all sample variables to zero */
    CLEAR_SAMPLE(s);

    if(!sens_read_pressure(s, &t))
        return -1;

    adjust_pressure(s->P[s->which_pr], s->which_pr, &top_pr, &bottom_pr);

    if(sensors & SENS_CTD1)
    {
        s->ctds++;
        s->ctd_tlast = RtcToCtm();
        if(!newctd_start_sample(0, bottom_pr))
            sensors &= ~SENS_CTD1;
    }

    if(sensors & SENS_CTD2)
    {
        s->ctds++;
        s->ctd_tlast = RtcToCtm();
        if(!newctd_start_sample(1, top_pr))
            sensors &= ~SENS_CTD2;
    }

    if(sensors & SENS_CTDO)
    {
        /* Start the CTDO sample, read it at the end */
        s->ctds++;
        if(sensors & SENS_O2)
        {
            if(!ctdo_start_oxy())
                ctdo_start_oxy();
        }

        if(!(ctdo_start_sample(bottom_pr) ||
             ctdo_start_sample(bottom_pr)))
        {
            sensors &= ~SENS_CTDO;
            if(sensors & SENS_O2)
                ctdo_stop_oxy();
        }
    }

    if(sensors & SENS_THERM)
    {
        if(!therm_start_sample())
            sensors &= ~SENS_THERM;
    }

    if(sensors & SENS_IPAR)
        s->par = par_read_data();
    else
        s->par = -1;

    if(sensors & SENS_I490)
        s->i490 = i490_read_data();
    else
        s->i490 = -1;


    if(sensors & SENS_CTD1)
    {
        while(!newctd_data_ready(0))
            ;
        newctd_read_data(0, &ctd);
        s->T[0] = ctd.t;
        s->S[0] = ctd.s;
    }

    if(sensors & SENS_CTD2)
    {
        while(!newctd_data_ready(1))
            ;
        newctd_read_data(1, &ctd);
        s->T[1] = ctd.t;
        s->S[1] = ctd.s;
    }

    if(sensors & SENS_CTDO)
    {
        while(!ctdo_data_ready())
            ;
        ctdo_read_data(&ctdo);
        s->T[0] = ctdo.t;
        s->S[0] = ctdo.s;
        s->oxy = ctdo.oxy;
        s->ctd_tlast = ctdo_start_time();
        if(sensors & SENS_O2)
        {
            if(!ctdo_stop_oxy())
                ctdo_stop_oxy();
        }
    }

    if(sensors & SENS_THERM)
    {
        while(!therm_data_ready())
            ;
        s->therm = therm_read_data();
    }

    s->sensors = sensors;

    return t;
}


/**
 * sens_open_data_file - open a data file for writing.
 * @type: file type index
 * @store: callback function, called with the old filename whenever a
 *         new file is opened.
 *
 * Open an MLF2 data file. @type specifies the file type and must be one
 * of the following constants defined in sensors.h.
 *
 * %ENV_FILE - environmental data file
 *
 * %GPS_FILE - GPS location file.
 *
 * %FASTPR_FILE - 1hz pressure data file.
 *
 * All data associated with the file is maintained within this module.  The
 * return value is 1 if the file was successfully opened or 0 if the operation
 * failed.
 */
int
sens_open_data_file(int type, fstore_t store)
{
    struct file_table   *ftp;
    long                age;
    int                 exists;
    int                 nc_backlog = get_param_as_int("nc_backlog");
#ifdef NC_FILE_LIST
    FILE                *dlog;
#endif
    char                fname[16];

    sens_close_file(type);      /* just in case ... */

    ftp = &f_table[type];

    age = RtcToCtm() - ftp->t;

    sprintf(fname, "%s%04ld.%s", ftp->basename, ftp->n, ftp->ext);
    exists = fileexists(fname);

    if(exists && age < ftp->max_age)
    {
        /*
        ** File exists and it is not too old.  Open the file in update mode
        ** and position the file pointer at the end.
        */
        ftp->fp = fopen(fname, "rb+");
        fseek(ftp->fp, 0L, SEEK_END);
    }
    else
    {
        if(exists && store)
            store(fname);

        if(ftp->n == 0)
        {
            /*
            ** Find the first unused file index.
            */
            do
            {
                ftp->n++;
                sprintf(fname, "%s%04ld.%s", ftp->basename, ftp->n, ftp->ext);
            } while(fileexists(fname));
        }
        else
        {
            ftp->n++;
            if(nc_backlog > 0 && (ftp->n - nc_backlog) > 1)
            {
                /*
                 * We want to keep fpr_backlog files in addition to
                 * the file we are going to open next.
                 */
                sprintf(fname, "%s%04ld.%s", ftp->basename,
                        ftp->n-nc_backlog-1, ftp->ext);
                if(fileexists(fname))
                {
                    log_event("Purging %s\n", fname);
                    unlink(fname);
                }
            }

            sprintf(fname, "%s%04ld.%s", ftp->basename, ftp->n, ftp->ext);
        }


        if((ftp->fp = fopen(fname, "wb")) == NULL)
        {
            log_error("open-next-file", "Cannot open %s (%d)\n", fname, errno);
            return 0;
        }

        log_event("New data file: %s\n", fname);

        ftp->t = RtcToCtm();

        /*
        ** The new file will get a new netCDF record descriptor from
        ** write_*_header so we must free the current one.
        */
        if(ftp->nd != NULL)
            free(ftp->nd);

        ftp->nd = NULL;

        switch(type)
        {
            case ENV_FILE:
                ftp->nd = write_env_header(ftp->fp, "Environmental data", TOTAL_CTDS);
                break;
            case GPS_FILE:
                ftp->nd = write_gps_header(ftp->fp, "GPS Data");
                break;
        }

        if(!strcmp(ftp->ext, "nc"))
        {
            if(ftp->nd == NULL)
            {
                log_error("open-next-file", "Cannot allocate netCDF record\n");
                fclose(ftp->fp);
                ftp->fp = 0;
                return 0;
            }
#ifdef NC_FILE_LIST
            /*
            ** Log the new data file information.
            */
            if((dlog = fopen(DATAFILE_LIST, "ab")) != NULL)
            {
                fprintf(dlog, "%-12s\t%ld\t%ld\t%ld\n", fname, ftp->t,
                        ftp->nd->hsize, ftp->nd->size);
                fclose(dlog);
            }
#endif
        }

    }

    return 1;
}

/**
 * Open the s-expression data and maybe quick-look files.
 *
 * @param  ql  if non-zero, open the quick-look file.
 * @param  datafp  returned data file pointer
 * @param  qlfp  returned quick-look file pointer.
 * @param  prfp  returned pressure file pointer (if non-NULL).
 * @return 1 if a new datafile is created.
 */
int
sens_open_sexp_files(int ql, FILE **datafp, FILE **qlfp, FILE **prfp)
{
    int                 r = 0;
    int                 fpr_backlog = get_param_as_int("fpr_backlog");
    int                 sexp_backlog = get_param_as_int("sexp_backlog");
    int                 sexp_autoqueue = get_param_as_int("sexp_autoqueue");
    static unsigned             _fcount = 0;
    static unsigned long        _ctime = 0;
    static char                 _datafile[16], _prfile[16];

    if(_fcount == 0)
    {
        /*
        ** At startup, initialize the counter to the first unused
        ** file index to avoid overwriting old files.
        */
        log_event("Checking for existing data files\n");

        do
        {
            if(_fcount >= 9999)
            {
                /* overwrite the oldest file. */
                _fcount = 1;
                break;
            }

            _fcount++;
            sprintf(_datafile, "data%04d.sx", _fcount);
            sprintf(_prfile, "fpr%04d.sx", _fcount);
        } while(fileexists(_datafile));
    }

    if((RtcToCtm() - _ctime) >= SEXP_MAX_FILE_AGE)
    {
        /*
         *  Note: _fcount is the index number for the next file.
         */

        if(sexp_autoqueue && fileexists(_datafile))
            (void)fq_add(_datafile);

        if(fpr_backlog > 0 && (_fcount - fpr_backlog) > 1)
        {
            /*
             * We want to keep fpr_backlog files in addition to
             * the file we are going to open next.
             */
            sprintf(_prfile, "fpr%04d.sx", _fcount-fpr_backlog-1);
            if(fileexists(_prfile))
            {
                log_event("Purging %s\n", _prfile);
                unlink(_prfile);
            }
        }

        if(sexp_backlog > 0 && (_fcount - sexp_backlog) > 1)
        {
            /*
             * We want to keep sexp_backlog files in addition to
             * the file we are going to open next.
             */
            sprintf(_datafile, "data%04d.sx", _fcount-sexp_backlog-1);
            if(fileexists(_datafile))
            {
                log_event("Purging %s\n", _datafile);
                unlink(_datafile);
            }
        }

        /*
         * Maximum file age exceeded. Update the file names and
         * increment the file counter.
         */
        sprintf(_datafile, "data%04d.sx", _fcount);
        sprintf(_prfile, "fpr%04d.sx", _fcount);
        _fcount++;
        _ctime = RtcToCtm();
        /*
         * We're opening the files in append mode so remove the
         * file if it already exists.
         */
        unlink(_datafile);
        unlink(_prfile);
        log_event("New S-exp data file, %s\n", _datafile);
        r = 1;
    }

    *datafp = fopen(_datafile, "a");
    if(prfp)
        *prfp = fopen(_prfile, "a");

    if(ql)
    {
        if(!fileexists("dataql.sx"))
        {
            r = 1;
        }

        *qlfp = fopen("dataql.sx", "a");
    }
    else
        *qlfp = NULL;

    return r;
}


/**
 * Close file if too old.
 */
int
sens_maybe_close_file(int type)
{
    struct file_table   *ftp;
    long                age;

    ftp = &f_table[type];
    age = RtcToCtm() - ftp->t;

    if(ftp->fp == 0)
        return 1;  /* already closed */

    if(age >= ftp->max_age)
    {
        sens_close_file(type);
        return 1;
    }

    return 0;
}

void
sens_set_file_time(int type, long t)
{
    f_table[type].t = t;
}

/**
 * sens_close_file - close an open data file.
 * @type: file type index.
 *
 * Close an open data file.  @type is the file type type (see
 * sens_open_data_file()).
 */
void
sens_close_file(int type)
{
    if(f_table[type].fp)
    {
        if(!strcmp(f_table[type].ext, "nc"))
            sync_file(f_table[type].nd, f_table[type].fp);
        fclose(f_table[type].fp);
        f_table[type].fp = 0;
    }

}


#define QL_FILENAME     "telem.nc"
static FILE             *ql_fp = NULL;
static netCDFdesc       *ql_nd = NULL;

static void
_close_quicklook(void)
{
    if(ql_fp)
    {
        sync_file(ql_nd, ql_fp);
        fclose(ql_fp);
        ql_fp = 0;
    }
}

static int
_open_quicklook(void)
{
    int         r = 1;

    if(ql_fp)
        _close_quicklook();
    if(ql_nd != NULL && fileexists(QL_FILENAME))
    {
        if((ql_fp = fopen(QL_FILENAME, "rb+")) != NULL ||
            (ql_fp = fopen(QL_FILENAME, "rb+")) != NULL)
            fseek(ql_fp, 0L, SEEK_END);
        else
            r = 0;
    }
    else
    {
        if((ql_fp = fopen(QL_FILENAME, "wb")) != NULL)
        {
            /*
             * The ql_nd data structure is allocated dynamically and
             * exists throughout the lifetime of the file. We must
             * free the previously created structure before allocating
             * a new one.
             */
            if(ql_nd)
                free(ql_nd);
            ql_nd = write_env_header(ql_fp, "Subsampled env data", TOTAL_CTDS);

            if(!ql_nd)
            {
                log_error("write_env_data",
                          "Out of memory\n");
                r = 0;
            }


        }
        else
            r = 0;
    }

    return r;
}


static void
_write_quicklook(const void *buf, size_t n)
{
    memcpy(ql_nd->data, buf, n);
    flush_record(ql_nd, ql_fp);
}


/*
 * sens_write_env_data - write samples to current data file.
 * @t: timestamp for data record.
 * @x: piston position in centimeters.
 * @s: pointer to structure containing the latest samples.
 * @ql_flag: if non-zero, write data record to quick-look file.
 *
 * Write the timestamp @t, piston position @x and the environmental data
 * samples to the current data file.
 */
void
sens_write_env_data(long t, float x, struct sample_t *s, int ql_flag)
{
    struct file_table   *ftp = &f_table[ENV_FILE];

    if(!ftp->fp)
    {
        log_error("write_env_data", "Data file not open!\n");
        return;
    }


    write_env_variable(ftp->nd, "time", &t, 4);
    write_env_variable(ftp->nd, "mode", &s->mode, 4);
    write_env_variable(ftp->nd, "pressure", &s->P[s->which_pr], 4);
    write_env_variable(ftp->nd, "pravg", &s->Pavg[s->which_pr], 4);
    write_env_variable(ftp->nd, "piston", &x, 4);
    if(s->sensors & SENS_THERM)
        write_env_variable(ftp->nd, "therm", &s->therm, 4);


    if(s->ctds > 0)
    {
        /* sanity check */
        if(s->ctds > 2)
            s->ctds = 2;

        write_env_variable(ftp->nd, "temp", &s->T[0], s->ctds*4);
        write_env_variable(ftp->nd, "sal", &s->S[0], s->ctds*4);
    }


    /*
    ** Write the new record to the file.
    */
    if(!flush_record(ftp->nd, ftp->fp))
    {
        log_error("write_env_data", "Error writing data record (t = %ld)\n",
                  t);
        if(!flush_record(ftp->nd, ftp->fp))
            log_error("write_env_data", "Second error writing data record (t = %ld)\n",
                      t);
    }


    if(ql_flag)
    {
        if(_open_quicklook())
        {
            _write_quicklook(ftp->nd->data, ftp->nd->size);
            _close_quicklook();
        }
        else
            log_error("write_env_data",
                      "Cannot open quick-look file\n");
    }

}


/**
 * sens_write_gps_fix - write a record to a GPS file
 * @gdp: pointer to GPS data
 *
 */
void
sens_write_gps_fix(GPSdata *gdp)
{
    float       x;
    long        lx;
    struct file_table   *ftp = &f_table[GPS_FILE];

    if(!ftp->fp)
    {
        log_error("write_gps_fix", "Data file not open!\n");
        return;
    }


    lx = RtcToCtm();
    write_gps_variable(ftp->nd, "time", (void*)&lx, 4);

    x = gdp->lat.deg;
    x += (((float)gdp->lat.min + ((float)gdp->lat.frac*1e-4))*0.0166667);
    if(gdp->lat.dir == 'S')
        x *= -1.0;
    write_gps_variable(ftp->nd, "lat", (void*)&x, 4);

    x = gdp->lon.deg;
    x += (((float)gdp->lon.min + ((float)gdp->lon.frac*1e-4))*0.0166667);
    if(gdp->lon.dir == 'W')
        x *= -1.0;
    write_gps_variable(ftp->nd, "lon", (void*)&x, 4);

    lx = gdp->satellites;
    write_gps_variable(ftp->nd, "nsats", (void*)&lx, 4);
    if(!flush_record(ftp->nd, ftp->fp))
        log_error("store-gps-fix", "Error writing GPS data record\n");
}

int
sens_wait_for_gps(GPSdata *gdp, long timeout, int setclock, int n_fixes)
{
    long        t0 = RtcToCtm();
    int         count;

    log_event("Waiting for GPS satellites\n");

    count = 0;

    do
    {
        gps_read_data(gdp);
        if(gdp->status != 0)
            count++;
        PET_WATCHDOG();
    } while((RtcToCtm() - t0) < timeout && count < n_fixes);

    gps_read_data(gdp);

    log_event("%d GPS satellites visible\n", gdp->satellites);
    if(gdp->satellites > 0 && setclock)
        gps_set_clock();

    return gdp->status;
}
