/*
** arch-tag: 48c52e51-f9cc-4b93-a852-381d8011c513
** Time-stamp: <2005-10-10 14:05:46 mike>
*/
#ifndef _FILEQ_H_
#define _FILEQ_H_

void fq_init(void);
int fq_add(const char *file);
int fq_add_next(const char *file);
int fq_send(int (*fsend)(const char*), int keep);
int fq_len(void);
int fq_add_uncompressed(const char *file);


#endif /* _FILEQ_H_ */
