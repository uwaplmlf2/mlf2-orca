/*
** arch-tag: module for netcdf environmental data files.
**
** The variables in the env files can vary from mission to mission
** so they are handled by a separate module.  See ncbase.c in libmlf2
** for details.
**
*/
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <setjmp.h>
#include <assert.h>
#include <time.h>
#ifdef __GNUC__
#include <unistd.h>
#endif
#include "netcdf.h"

#include "ncattr.h"

/*
** Dimension descriptions for each file type.
*/

#define ENV_TIME_INDEX	1

/* Environmental data */
static struct dim env_dims[] = {
{ _CTD,		2L },
{ _TIME,	0L },
};

static int32 env_dim_index_scalar[] = { ENV_TIME_INDEX };
static int32 env_dim_index_ctd[] = { ENV_TIME_INDEX, 0 };


static struct var env_vars[] = {
{ _TIME,	1, env_dim_index_scalar,	1, &attr_table[TIME_UNITS],
    NC_INT,	0,	0,	0,	0 },
{ "mode",	1, env_dim_index_scalar,	0, NULL,
    NC_INT,	0,	0,	0,	0 },
{ _PRESSURE,	1, env_dim_index_scalar,	1, &attr_table[PRESSURE_UNITS],
    NC_FLOAT,	0,	0,	0,	0 },
{ "pravg",	1, env_dim_index_scalar,	1, &attr_table[PRESSURE_UNITS],
    NC_FLOAT,	0,	0,	0,	0 },
{ _PISTON,	1, env_dim_index_scalar,	1, &attr_table[PISTON_UNITS],
    NC_FLOAT,	0,	0,	0,	0},
{ _TEMP,	2, env_dim_index_ctd,		1, &attr_table[TEMP_UNITS],
    NC_FLOAT,	0,	0,	0,	0 },
{ _SAL,		2, env_dim_index_ctd,		1, &attr_table[SAL_UNITS],
    NC_FLOAT,	0,	0,	0,	0 },
  };


/**
 * write_env_variable - write an environmental-data variable.
 * @nd: pointer to netCDF record descriptor
 * @name: name of the variable
 * @data: pointer to data value
 * @size: size (in bytes) of the data value
 *
 * Write the value of an environmental-data variable to the current
 * environmental-data record.  Returns 1 if successful, otherwise 0.
 */
int
write_env_variable(netCDFdesc *nd, const char *name, void *data, int size)
{
    assert(nd && nd->size > 0 && nd->vars > 0);
    return add_to_record(data, size, nd->data, nd->vars, 
			 sizeof(env_vars)/sizeof(struct var), name);
}


/**
 * write_env_header - write the netCDF header for an environmental data file.
 * @ofp: pointer to the netCDF output file.
 * @desc: descriptive string for the file.
 * @nr_ctd: number of CTDs (1 or 2)
 *
 * Writes the netCDF header for an environmental data file to the output
 * file pointed to by @ofp.  The string @desc will be stored in the
 * 'source' attribute in the header.  Returns a pointer to a netCDF record 
 * descriptor if successful, otherwise NULL.  The pointer is allocated from 
 * the heap and the caller is responsible for freeing the pointer.
 */
netCDFdesc*
write_env_header(FILE *ofp, const char *desc, int nr_ctd)
{
    netCDFdesc	*nd;
    long	nbytes, total_size;
  
    env_dims[0].length = nr_ctd;
    
    nbytes = write_nc_header(ofp, env_dims, 
			     sizeof(env_dims)/sizeof(struct dim),
			     env_vars, sizeof(env_vars)/sizeof(struct var),
			     desc);
    ALLOC_STRUCTURE(env);
    
    return nd;
}

