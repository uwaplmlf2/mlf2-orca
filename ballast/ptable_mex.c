/**@
** Parameter table management functions - implementation for Matlab simulator
**
**
** The table associates a name (character string) with a "parameter handle" --
** a type code and memory location.  Its purpose is to allow runtime
** modification of various control variables by messages sent during the
** mission.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "ptable_mex.h"

static HashTable *ptable = NULL;



/**
 * Initialize the MLF2 parameter table.
 * The table associates a name (character string) with a "parameter handle" --
 * a type code and memory location.  Its purpose is to allow runtime
 * modification of various control variables by messages sent during the
 * mission without having to resort to a large number of global variables.
 *
 * Note that the parameter names are treated as case-insensitive.
 *
 * @return 1 if successful or 0 if an error occurs, 2 if the table already exists.
 */

int
init_param_table(void)
{
    if(ptable != NULL)
	return 2;

    if((ptable = ht_create(NR_CELLS, HT_NOCASE)) == NULL)
	return 0;

    return 1;
}


/**
 * Add a new parameter to the table.  Only the address of the parameter
 * is stored, the value is not copied therefore the parameter should
 * be static, global, or stored on the heap (malloc'ed).  The parameter
 * type code must be one of the following constants.
 *
 *  - PTYPE_SHORT - addr points to a short integer.
 *  - PTYPE_LONG - addr points to a long integer.
 *  - PTYPE_DOUBLE - addr points to a double precision float.
 *  - PTYPE_STRING - addr is a char string (upper 8 bits are string length)
 *
 * The type codes may be ORed with PTYPE_READ_ONLY which will disallow
 * modification of the parameter.
 *
 * @param  name  key to associate with the value.
 * @param  type  parameter type code.
 * @param  addr  pointer to parameter value.
 *
 */
void
add_param(const char *name, int type, void *addr)
{
    struct param	p;

    if(ptable == NULL)
	init_param_table();

    /*
    ** All new parameters are marked as "dirty" so they will be printed
    ** on the first call to dump_params().
    */
    p.type = type | PTYPE_DIRTY;
    p.loc = addr;

    /*
    ** insert_elem will copy the contents of 'p' to malloc'ed memory.
    */
    if(ptable)
	ht_insert_elem(ptable, name, &p, (unsigned short)sizeof(struct param));
}

/**
 * Parses the string to obtain the new value for the named parameter.
 *
 * @param  name  parameter name.
 * @param  str  ascii string representation of the new value
 * @return 1 if successful, otherwise 0.
 */
int
set_param_str(const char *name, const char *str)
{
    struct param	*p;
    unsigned		type, n;

    if(!ptable || !ht_find_elem(ptable, name, (void**)&p, 0))
	return 0;

    type = p->type & ~PTYPE_SIZEMASK;

    switch(type & ~PTYPE_DIRTY)
    {
	case PTYPE_DOUBLE:
	    *((double*)p->loc) = atof(str);
	    log_event("Set %s to %f\n", name, *((double*)p->loc));
	    break;
	case PTYPE_LONG:
	    *((long*)p->loc) = strtol(str, NULL, 0);
	    log_event("Set %s to %ld\n", name, *((long*)p->loc));
	    break;
	case PTYPE_SHORT:
	    *((short*)p->loc) = (short)strtol(str, NULL, 0);
	    log_event("Set %s to %d\n", name, *((short*)p->loc));
	    break;
	case PTYPE_STRING:
	    n = PTYPE_GETSIZE(p->type);
	    strncpy((char*)p->loc, str, n-1);
	    ((char*)p->loc)[n-1] = '\0';
	    log_event("Set %s to '%s'\n", name, (char*)p->loc);
	    break;
	default:
	    return 0;
    }

    p->type |= PTYPE_DIRTY;

    return 1;
}

/**
 * Sets the named parameter to value.
 *
 * @param  name  parameter name.
 * @param  value   new parameter value.
 * @return 1 if successful, otherwise 0.
 */
int
set_param_int(const char *name, long value)
{
    struct param	*p;
    unsigned		type;

    if(!ptable || !ht_find_elem(ptable, name, (void**)&p, 0))
	return 0;

    type = p->type & ~PTYPE_SIZEMASK;

    switch(type & ~PTYPE_DIRTY)
    {
	case PTYPE_DOUBLE:
	    *((double*)p->loc) = (double)value;
	    log_event("Set %s to %f\n", name, *((double*)p->loc));
	    break;
	case PTYPE_LONG:
	    *((long*)p->loc) = value;
	    log_event("Set %s to %ld\n", name, *((long*)p->loc));
	    break;
	case PTYPE_SHORT:
	    *((short*)p->loc) = (short)value;
	    log_event("Set %s to %d\n", name, *((short*)p->loc));
	    break;
	default:
	    return 0;
    }

    p->type |= PTYPE_DIRTY;

    return 1;
}

/**
 * Sets the named parameter to value.
 *
 * @param  name  parameter name.
 * @param  value   new parameter value.
 * @return 1 if successful, otherwise 0.
 */
int
set_param_double(const char *name, double value)
{
    struct param	*p;
    unsigned		type;

    if(!ptable || !ht_find_elem(ptable, name, (void**)&p, 0))
	return 0;

    type = p->type & ~PTYPE_SIZEMASK;

    switch(type & ~PTYPE_DIRTY)
    {
	case PTYPE_DOUBLE:
	    *((double*)p->loc) = value;
	    log_event("Set %s to %f\n", name, *((double*)p->loc));
	    break;
	case PTYPE_LONG:
	    *((long*)p->loc) = (long)value;
	    log_event("Set %s to %ld\n", name, *((long*)p->loc));
	    break;
	case PTYPE_SHORT:
	    *((short*)p->loc) = (short)value;
	    log_event("Set %s to %d\n", name, *((short*)p->loc));
	    break;
	default:
	    return 0;
    }

    p->type |= PTYPE_DIRTY;

    return 1;
}

/**
 * Looks up the information (type and address) for the named parameter.
 *
 * @param  name  parameter name
 * @param  param  pointer to returned parameter information.
 * @return 1 if sucessful, otherwise 0.
 */
int
get_param(const char *name, struct param *param)
{
    struct param	*p;

    if(ptable && ht_find_elem(ptable, name, (void**)&p, 0))
    {
	param->type = p->type;
	param->loc = p->loc;
	return 1;
    }

    return 0;
}

/**
 * Return a parameter value as an integer.
 *
 * @param  name  parameter name
 * @return current value.
 */
long
get_param_as_int(const char *name)
{
    struct param	*p;
    long		r = 0L;
    unsigned		type;

    if(ptable && ht_find_elem(ptable, name, (void**)&p, 0))
    {
	type = p->type & ~PTYPE_SIZEMASK;
	switch(type & PTYPE_TYPEMASK)
	{
	    case PTYPE_DOUBLE:
		r = (long)(*((double*)p->loc));
		break;
	    case PTYPE_LONG:
		r = *((long*)p->loc);
		break;
	    case PTYPE_SHORT:
		r = (long)(*((short*)p->loc));
		break;
	}
    }

    return r;
}


/**
 * Return a parameter value as a double.
 *
 * @param  name  parameter name
 * @return current value.
 */
double
get_param_as_double(const char *name)
{
    struct param	*p;
    double		r = 0.;
    unsigned		type;

    if(ptable && ht_find_elem(ptable, name, (void**)&p, 0))
    {
	type = p->type & ~PTYPE_SIZEMASK;
	switch(type & PTYPE_TYPEMASK)
	{
	    case PTYPE_DOUBLE:
		r = *((double*)p->loc);
		break;
	    case PTYPE_LONG:
		r = (double)(*((long*)p->loc));
		break;
	    case PTYPE_SHORT:
		r = (double)(*((short*)p->loc));
		break;
	}
    }

    return r;
}

static void
print_param(struct elem *e, void *calldata)
{
    FILE		*fp = (FILE*)calldata;
    struct param	*p = (struct param*)e->data;
    unsigned		type;

    /* Only print parameters which have changed */
    if(!(p->type & PTYPE_DIRTY))
	return;

    p->type &= ~PTYPE_DIRTY;

    type = p->type & ~PTYPE_SIZEMASK;

    fprintf(fp, "<param name='%s' ", e->name);
    switch(type & PTYPE_TYPEMASK)
    {
	case PTYPE_DOUBLE:
	    fprintf(fp, "type='double'>%.6g</param>\n", *((double*)p->loc));
	    break;
	case PTYPE_LONG:
	    fprintf(fp, "type='long'>%ld</param>\n", *((long*)p->loc));
	    break;
	case PTYPE_SHORT:
	    fprintf(fp, "type='short'>%hd</param>\n", *((short*)p->loc));
	    break;
	case PTYPE_STRING:
	    fprintf(fp, "type='string'>%s</param>\n", (char*)p->loc);
	    break;
    }
}

/**
 * Dump all updated parameters to a file in XML format.
 *
 * @param  fp  pointer to output FILE
 */
void
dump_params(FILE *fp)
{
    fprintf(fp, "<ptable time=0>\n");
    ht_foreach_elem(ptable, print_param, (void*)fp);
    fputs("</ptable>\n", fp);
}



/**
 * Read parameters from a file.
 * Read the parameters from file and set the corresponding
 * values in the parameter table.
 *
 * @param  file  filename.
 * @return 1 if successful, 0 on error
 * (Originally found in xml_util.c)
 */
int params_read(const char *file)
{
	FILE *f;
	char buf[1024];
	char * pch, *pname, *ptype, *pval, *pp;
		// delimeters for xml reading
	#define DELIM (" <>='")

    log_event("Updating parameters from %s\n", file);

	f = fopen(file,"r");
	if (f==NULL)
	{
		return 0;
	}

	do
	{
		if (  fgets(buf, 1024,f) !=NULL)
		{
			pch=strtok (buf,DELIM);
			if (strcmp(pch,"param")) continue;
			pch=strtok (NULL,DELIM);
			if (strcmp(pch,"name")) continue;
			pname = strtok (NULL, DELIM);
			pch=strtok (NULL,DELIM);
			if (strcmp(pch,"type")) continue;
			ptype = strtok(NULL, DELIM);

			pp = NULL;
			// loop until the end
			do {
			pval = pp; // store previous token
			pp = strtok(NULL, DELIM);
			}	while (strcmp(pp,"/param"));


//			printf ("%s(%s)=%s\n",pname,ptype,pval);
			if(set_param_str(pname, pval) == 0)
				log_event("ERROR: parameter '%s' is not in ptable\n", pname);

		}
	} while(!feof(f));
	fclose(f);
	return 1;
}




/*
** This is the replacement of hash table object functionality with a linked list.
** Search performance is lower, but the order of the entries is preserved.
** All the function and variable names are the same, so (in principle) this should be a drop-in replacement.
**
*/


#define MIN(a,b)	((a) < (b) ? (a) : (b))

static int
my_strcasecmp(const char *s1, const char *s2)
{
    while(*s1 != '\0' && tolower(*s1) == tolower(*s2))
    {
	s1++;
	s2++;
    }

    return tolower(*(unsigned char *) s1) - tolower(*(unsigned char *) s2);
}

/*
 * Allocate memory for a new table cell.  The cell is allocated as a
 * single block with one call to malloc.
 */
static struct elem*
alloc_node(size_t namelen, short datalen)
{
    register struct elem	*e;

    /*
    ** Round the length of the name up to the next longword boundary.
    */
    namelen = (namelen + 3) & ~3;
    if((e = (struct elem*)malloc(sizeof(struct elem) + namelen + datalen)))
	e->data = (char*)e + sizeof(struct elem) + namelen;
    return e;
}

static struct elem*
lookup(HashTable *htp, const char *name)
{
    register struct elem	*e;

    for(e = htp->first;e != NULL;e = e->next)
	if(htp->cmp(name, e->name) == 0)
	    break;
    return e;
}

/**
 * Create a new HashTable.
 * Create a HashTable data structure to associate strings (names) with
 * arbitrary blocks of data.  The return value is a pointer to a new HashTable
 * or NULL if memory allocation fails.  @type must be set to one of the
 * following constants:
 * - HT_STANDARD (key lookups are case sensitive)
 * - HT_NOCASE (key lookups are case insensitive)
 *
 * @param  size  number of table entries - NOT USED, ONLY FOR COMPATIBILITY!
 * @param  type  determines how key lookups are handled.
 * @return a new HashTable instance.
 */
HashTable*
ht_create(unsigned size, int type)
{
    register HashTable	*htp;
    ulong		nbytes;

    nbytes = sizeof(HashTable) ;

    if((htp = malloc(nbytes)) != NULL)
    {
	if(type == HT_NOCASE)
	{
	    htp->cmp = my_strcasecmp;
	}
	else
	{
	    htp->cmp = strcmp;
	}
    htp->first = NULL;
	htp->last = NULL;
    }

    return htp;
}

/**
 * Destroy a HashTable instance.
 * Free all memory associated with a HashTable.
 *
 * @param  htp  pointer to HashTable.
 */
void
ht_destroy(HashTable *htp)
{
    register struct elem	*p, *q;

	if(htp->first)
	{
	    /*
	    ** Free the chain rooted at this index
	    */
	    p = htp->first;
	    while(p)
	    {
		q = p->next;
		free(p);
		p = q;
	    }
	}

    free(htp);
}


/**
 * Insert a new element in the table.
 * This function associates a name with an arbitrary block of data pointed
 * to by what.  If len is greater than zero, the data is copied into the
 * hash table.
 *
 * @param  htp  pointer to HashTable
 * @param  name  name of the element.
 * @param  what  opaque data handle to associate with @name.
 * @param  len  size (in bytes) of the data.
 * @return 1 if successful, 0 if memory allocation fails.
 */

int
ht_insert_elem(HashTable *htp, const char *name, const void *what,
	     unsigned short len)
{
    struct elem		*e;

    e = lookup(htp, name);
    /*
     * If the entry exists but is not large enough to hold the
     * new value, we must remove it.
     */
    if(e != NULL && e->len < len)
    {
	 ht_remove_elem(htp, name);
	 e = NULL;
    }

    if(e == NULL)
    {
	if((e = alloc_node(strlen(name), len)) == NULL)
	    return 0;
	strcpy(e->name, name);
	e->next = NULL;
	if (htp->first==NULL)
	{
		htp->first = e;
	}
	else
	{
		htp->last->next = e;
	}
	htp->last = e;

	}

    if(len > 0)
	memcpy(e->data, what, (size_t)len);
    else
	e->data = (void*)what;

    e->len = len;
    return 1;
}


/**
 * Remove a named element from the table.
 * Removes the element and frees all associated memory.
 *
 * @param  htp  pointer to HashTable.
 * @param  name  element name.
 */

void
ht_remove_elem(HashTable *htp, const char *name)
{
    register struct elem	*p, *q;

    q = 0;
    for(p = htp->first;p != NULL;q = p,p = p->next)
	if(htp->cmp(name, p->name) == 0)
	{
	    if(q == 0)
		{
			 htp->first = p->next;
		}
	    else
		{
			q->next = p->next;
		}
		if (htp->last==p)
		{
			htp->last = q;
		}
	     free(p);
	    break;
	}
}


/**
 * Determine whether a named element is in the table.
 *
 * @param  htp  pointer to HashTable.
 * @param  name  name of the element.
 * @return true if the element exists otherwise false.
 */
int
ht_elem_exists(HashTable *htp, const char *name)
{
    return (lookup(htp, name) != NULL);
}
/**
 * Retrieve an element from the table.
 * Find the element associated with name and return the data.  If the
 * data had actually been stored in the table (i.e. not just a pointer)
 * and lenp is non-NULL, the data will be copied into whatp.  Returns
 * 1 if the element was found or 0 if not found.

 * @param  htp  pointer to HashTable.
 * @param  name  name of the element.
 * @param  whatp  returned data handle (if non-NULL)
 * @param  lenp  will contain the data size on return (if non-NULL).
 * @return true if the element exists otherwise false.
 */
int
ht_find_elem(HashTable *htp, const char *name, void **whatp,
	   unsigned short *lenp)
{
    struct elem		*e;

    if((e = lookup(htp, name)) != NULL)
    {
	if(whatp)
	{
	    if(e->len > 0 && lenp)
		memcpy(*whatp, e->data, (size_t)MIN(e->len, *lenp));
	    else
		*whatp = e->data;
	}

	if(lenp)
	    *lenp = e->len;

	return 1;
    }

    return 0;
}


/**
 * Call a supplied function for each entry in the table.
 * Iterate over all elements of the table, calling the supplied function
 * and passing a pointer to the element as an argument.
 *
 * @param  htp  pointer to HashTable.
 * @param  func  function to apply to each entry.
 * @param  calldata  arbitrary data passed to func as second argument.
 */
void
ht_foreach_elem(HashTable *htp, ht_callback func, void *calldata)
{
    register struct elem	*e;

	for(e = htp->first;e != NULL;e = e->next)
	{
		(*func)(e, calldata);
    }
}


/*
 * Convert ptable to a Matlab structure. Return a pointer
 * For simplicity, make it a single (not hierarchical) structure
 * Ptable names need to be changed to allow them to serve as Matlab structure fields: Down.Pmax->DownPmax, etc.
 *
 *
 **/
mxArray* ptable2matlab(void)
{

 mxArray *out, *tmp;
 struct elem	*e;
 int n=0;
 int sz = sizeof(double);
 double* pdata;
 struct param	*p;
 mwSize dims[] = {0,2};

 // count parameters
  for(e = ptable->first;e != NULL;e = e->next)
	{
      dims[0]++;
	 }


 out = mxCreateCellArray(2,dims);

 // populate...
 n = 0;
	for(e = ptable->first;e != NULL;e = e->next)
	{
	 mxSetCell(out, n, mxCreateString(e->name));

     p = (struct param*)e->data;

	 tmp = mxCreateDoubleMatrix(1,1,mxREAL);
	 pdata = (double*)mxGetPr(tmp);
	 *pdata = mxGetNaN();
	switch(p->type & PTYPE_TYPEMASK)
	{
	    case PTYPE_DOUBLE:
		*pdata = *((double*)p->loc);
		break;
	    case PTYPE_LONG:
		*pdata = (double)(*((long*)p->loc));
		break;
	    case PTYPE_SHORT:
		*pdata = (double)(*((short*)p->loc));
		break;
	}

     mxSetCell(out, n+dims[0], tmp);
	 n++;
    }
	return out;
}