/*  arch-tag: c727d50b-4ddb-4274-afe9-ed1343f16e12
 Time-stamp: <2007-01-22 20:44:59 mike>

 (day_time, mode, *P, *T, *S,*B)
 Provides plausibly realistic values of Pressure, temperature, Salinity and Altitude
 given day_time and mode.
 For benchtop testing, this routine should be called before
 the ballasting routine is called (ballast.c) and the values
 passed to the ballasting routine. It should also be called without
 calls to ballast.c during MODE_COMM in order to bring the simulated
 float to the surface.
 The resulting P,T,S,Alt values should be similar to those obtained on a real
 mission and the bocha should move in a realistic way.
 11/7/99 First version to Mike Kenney
 3/26/00  Version 2 to Kenney
 6/03/01  Updated for T'cline and ML missions
 6/04/02 Update for Hurricane '02
 11/12/03 Param change for ADVfloat
 5/04/06  AESOP changes -
 11/17/06 - EQ07 changes - name is now labsimtest.  New calling parameters
 diag should be 0 in real mission, 1 in simulated mission
 If 1, program checks if float is behaving right - imperfectly of course
 3/11/07 - AESOP version - use with standard AESOP mission
 6/25/08 - HURR08
 */
#include <math.h>
#include <stdio.h>
#include "ballast.h"    /*  This contains function & structure definitions */
#ifdef SIMULATION
#define log_event	printf
/* __________  next section only for Matlab use __________*/
#include "mex.h"
/* Input Arguments */
#define	DAY_IN		prhs[0]
#define M_IN		prhs[1]
#define T_IN		prhs[2]
#define S_IN		prhs[3]
#define P_IN		prhs[4]
#define B_IN		prhs[5]
#define DIAG_IN       prhs[6]
/* Output Arguments */
#define	P_OUT	plhs[0]
#define	T_OUT	plhs[1]
#define	S_OUT	plhs[2]
#else
#include "log.h"
#endif /* SIMULATION */
/*__________________________________________________________*/
/* Float simulation routine starts here   */
/* This subroutine simulations the stratification and motion of MLF2 so
 that the software can be tested on the bench.
 It is driven here by a Matlab simulation program, but should be able
 to be used in the float directly
 */
/* Start set_ballast function  */

/* define constants for error checking */
void labsimtest(
				double	day_time,  /* units of days */
				int		mode,
				double  Tin,
				double  Sin,
				double  Pin,
				double  Bin,       /* UNITS OF m^3 = cm*20.1e-6 */
				int		diag,
				double	*Pressure,
				double	*Temperature,
				double	*Salinity
				)
{
    static double last_P=0;   /* remembers pressure */
    static double last_day=0;
    static double last_mode= -999;  /* remembers last mode */
    static double mode_start=0;  /* start time of this mode */
    static double SettleP0;  /* saves settle goal */
    static double Zsettle=-50.;  /* fluctuation in settle mode */
    static double WiggleZ=10.;  /* Tcline wiggle amplitude */
    static double WiggleT=20000.; /*  Period / sec */
    static double Tsettle=1000.;
    static double Wud=0.1;  /* up/down speed */
    static double Pml=30.;  /* mixed layer depth  */
    static double Wml=0.01;  /* mixed layer velocity */
    static double Daycalm=1.5;    /* Wind calms after this */
    static double Pcomm=5.;  /* pressure in Comm mode (end) */
    static double Wcalm=0.01; /* calm wind ML velocity */
    static double UD= -1.;  /* up/down parameter in ML */
    static double Depth=3000.;	/* Water depth */
    static double ml=1;  /* 1 for ML mission, 0 for T'cline mission */
    static double mlswitch=-1; /* subduct/obduct period <0 to void*/
    static double Tml=26.65;       /* 24 for EQUATORIAL PACIFIC  */
    static double Sml=39.5;     /* 34.5 FOR EQUATORIAL PACIFIC */
    static double Sz= -0.06;	/* S gradient */
    static double Tz= -0.06;  /* T gradient */
    static double Pgoal=80.;   /* 30 Tcline depth target */
    static double Tseek=3e3;  /* Tcline seek time*/
    static double Bgoal=150.e-6;  /* target for bocha */
    static double Brate= -1024./0.5/400.;  /* rate to change Sml to get B to Bgoal */
    static int icomm=0;   /* comm up=0; comm surf=1; */
    static int icheck=0;   /* count check messages */
    static int icheckp=0;  /* check changes */
    static double last_B=0; /* save bocha position */
    static int Errp=0;    /* save previous error */
    static double Bstart=0; /* Bocha at mode start */
	
    static double dBmin=1.e-6;  /* minimum change in Ball before warning */
    static double Bmin=5.5e-6;    /* below this is considered homed */
    static double Bmax=600.e-6; /*above this is full out */

    double x,xx, mode_time;
    int Err;
 	
    if (mode!=last_mode){
		mode_start=day_time;
		Bstart=Bin;
    }
    if (mode!=MODE_COMM)icomm=0;
	
    mode_time=day_time-mode_start;  /* time in this mode */
    if (mode==MODE_PROFILE_DOWN){
		*Pressure=last_P+ Wud*(day_time-last_day)*86400.;
    }
    else if ( mode==MODE_PROFILE_UP){
		*Pressure=last_P - Wud*(day_time-last_day)*86400.;
		if (*Pressure<0) {*Pressure=0.;}
    }
    else if (mode==MODE_SETTLE){
		if (mode_time==0){
			SettleP0=last_P;  /* pressure at start of settle */
		}
		x=mode_time*86400/Tsettle;
		if (ml==1)
			*Pressure=SettleP0 + Zsettle*x*exp( -x);
		else {
			*Pressure=SettleP0*exp(-x) + Pgoal*(1-exp(-x));
		}
		*Pressure = *Pressure +
        WiggleZ*6.28/WiggleT*(day_time-last_day)*86400.
        *sin(6.28*mode_time/WiggleT*86400.);
    }
    else if (is_drift(mode)){
		/* subduct and obduct */
		if( mlswitch>0.){
			if( fmod(day_time,mlswitch)<0.5){ml=1;}
			else ml=0.;
		}
		if (day_time>Daycalm)Wml=Wcalm; /* calm winds */
		if(ml==1){
			/* MIXED LAYER */
			*Pressure=last_P +UD*Wml*(day_time-last_day)*86400.;
			if (*Pressure <= 0. ){ UD= 1.;}
			if (*Pressure > Pml) { UD= -1.;}
			*Pressure=last_P +UD*Wml*(day_time-last_day)*86400.;
		}
		else{
			/* T'cline  */
			*Pressure= last_P - (last_P-Pgoal)/Tseek*(day_time-last_day)*86400.;
		}
		if (*Pressure > 1.1*Pml){  /* IW below mixed layer */
			*Pressure = *Pressure +
			WiggleZ*6.28/WiggleT*(day_time-last_day)*86400.
			*sin(6.28*mode_time/WiggleT*86400.);
		}
		
		/* Adjust Sml to seek Bocha to Bgoal */
		Sml=Sml+(Bgoal-Bin)*Brate*(day_time-last_day)*86400.;
		if (Sml<0.001){
			Sml=0.001;  /* Don't let S be zero or negative */
		}
    }
    else if (mode==MODE_COMM){
        if(last_P>0. && icomm==0) {
            *Pressure=last_P - Wud*(day_time-last_day)*86400.;
        }
        else if (last_P<0.) {
            *Pressure=0.;
            icomm=1;
        }
        else {
            icomm=1;
            *Pressure= last_P - (last_P-Pcomm)/Tseek*(day_time-last_day)*86400.;
        }
    }
    else if (mode==MODE_SLEEP){
        *Pressure= last_P;      // Sleep - float doesn't move
    }
    else {   /* Error or Comms  - Surface */
        *Pressure=last_P - Wud*(day_time-last_day)*86400.;
        if (*Pressure <0) *Pressure=0;
    }

    if(*Pressure<0) *Pressure=0; /*final idiot HACK to make pressure positive - might screw up other things*/

    /* GET CTD DATA AT THIS PRESSURE  */
    if (*Pressure <=Pml){
        *Salinity=Sml;
        *Temperature=Tml;
    }
    else {
        *Salinity=Sml+ Sz*( *Pressure - Pml);
        *Temperature=Tml + Tz * (*Pressure-Pml);
    }

    /* Now do checks   */
    /* if (diag!=0){  disable all the checks - they are not working right and get in the way */
    if (0){
        if (mode==MODE_PROFILE_DOWN && day_time>mode_start){
            if(last_B-Bin < dBmin  && Bin >dBmin){
                ++icheck;
                Err=1;   /* Down bocha not moving in */
                x=1.e6*(last_B-Bin);
                xx=1.e6*Bin;
            }
            else icheck=0;
        }
        else if (mode==MODE_PROFILE_UP && day_time>mode_start){
            if(Bin-last_B < dBmin && Bin <Bmax){
                ++icheck;
                Err=2;  /* Up bocha not moving out */
                x=1e6*(Bin-last_B);
                xx=1.e6*Bin;
            }
            else icheck=0;
        }
		else if (mode==MODE_SETTLE && day_time>mode_start){
            if(fabs(Bin-last_B) < dBmin  && Bin <Bmax ){
                ++icheck;
                Err=3;    /* Settle bocha not moving */
                x=1e6*(Bin-last_B);
                xx=1.e6*Bin;
            }
            else icheck=0;
        }
        else if ( (mode==MODE_DRIFT_ISO  ||
                   mode==MODE_DRIFT_SEEK ||
                   mode==MODE_DRIFT_ML         ) &&
                 day_time-mode_start >0.1){
            if(Bin <20.e-6 || Bin >300.e-6){   /* Special for EQ07 labsimtest mission */
                ++icheck;
                Err=4;         /* Bocha out of bounds for drift */
                x=Bin*1e6;
                xx=1.e6*Bin;
            }
            else icheck=0;
        }
        else icheck=0;

        if (icheck==1)log_event("WARNING: mode %d  Error %d  %5.1f  %5.1f [first occurrance]\n",mode,Err,x,xx);
        else if (icheck==2)
            log_event("WARNING: mode %d  Error %d  %5.1f  %5.1f [second occurance, stop listing]\n",mode,Err,x,xx);
        else if (icheck==0 && icheckp>0)
            log_event("WARNING END: No more errors of type %d [total of %d found]\n",Errp,icheckp);
        icheckp=icheck;
        Errp=Err;
    }
    last_P= *Pressure;
    last_day=day_time;
    last_mode=mode;
    last_B=Bin;

}
#ifdef SIMULATION
/* ___________________________cut here_________________________________*/
/*  MATLAB INTERFACE ROUTINE - IGNORE FOR FLOAT INSTALLATION */
void mexFunction(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
				 )
{
    double	*day,*P,*T,*S,*mode_in,*diag_in,*Tin0,*Sin0,*Pin0,*Bin0;
    unsigned int	m,n;
    /* Check for proper number of arguments */
    if (nrhs != 7) {
		mexErrMsgTxt("labsimtest requires 7 input arguments.");
    } else if (nlhs !=3) {
		mexErrMsgTxt("labsimtest requires 3 output arguments.");
    }
	/* Create a matrix for the return argument */
    P_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);
    T_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);
    S_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);
    /* Assign pointers to the various parameters */
    P = mxGetPr(P_OUT);
    S = mxGetPr(S_OUT);
    T = mxGetPr(T_OUT);
    day = mxGetPr(DAY_IN);
    mode_in = mxGetPr(M_IN);
    Tin0=mxGetPr(T_IN);
    Sin0=mxGetPr(S_IN);
    Pin0=mxGetPr(P_IN);
    Bin0=mxGetPr(B_IN);
    diag_in=mxGetPr(DIAG_IN);
	
    /* Do the actual computations in a subroutine */
    labsimtest(*day,(int)*mode_in,*Tin0,*Sin0,*Pin0,*Bin0,*diag_in,P,T,S);
    return;
}
#endif /* SIMULATION */



