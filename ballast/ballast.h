/*
 ** arch-tag: ballast header file
 ** Time-stamp: <2007-01-17 20:47:24 mike>
 **
 ** Parameters for MLF2 ballasting routine

 4/26/00 D'Asaro mods on Kenney version
 6/25/00
 Rewrite & simplify after first missions
 7/4/00 - close to final for July00
 mission
 8/16/00 - start mods for Fall00 ML mission
 6/5/02 - Hurricane
 3/1/02 - MIXED
 7/11/02 -CBLAST03
 2/2/04 - ADV float mods
 1/30/05 - Equatorial mods
 4/28/06 - AESOP mods and cleanup
 9/21/06 - DOGEE Gasfloat mods
 11/16/06 - Equatorial Nightbug mods and update
 11/14/07 -NAB changes in Targetting parameters
 11/10/10 - Eliminate eos structure, Add Error structure
 9/10/11 - Add StageSim structure
 5/21/12  - "Global timer" functionality added (AS)
 5/21/12  - Second UP mode added (AS)
 1/31/14 - New sensor macros and formatting cleanup
 2/12/14 - Multi-day timers, fix profile saving overrun
 */
#undef SIMULATION

#ifndef _BALLAST_H_     /* check that only one definition is included */
#define _BALLAST_H_

#ifndef SIMULATION
#include "config.h"
#endif

enum {MODE_PROFILE_DOWN=0,
      MODE_SETTLE,
      MODE_PROFILE_UP,
      MODE_DRIFT_ISO,
      MODE_DRIFT_SEEK,
      MODE_DRIFT_ML,
      MODE_PROFILE_SURF,
      NR_REAL_MODES,
      MODE_GPS,
      MODE_COMM,
      MODE_ERROR,
      MODE_DONE,
      MODE_START,
      MODE_SLEEP};

#define is_profile(m)   ((m) == MODE_PROFILE_DOWN ||\
                         (m) == MODE_PROFILE_UP ||\
                         (m) == MODE_PROFILE_SURF)
#define is_drift(m)     ((m) == MODE_DRIFT_ISO || (m) == MODE_DRIFT_ML || (m) == MODE_DRIFT_SEEK )

/*
 ** Global variables for mode control
 */
extern unsigned long Sensors[];
extern short Si[];

#define ENABLE_SENSORS(m, slist) \
    do { if((m) >= 0 && (m) < NR_REAL_MODES)        \
        Sensors[(m)] |= (slist);        \
    } while(0)

#define DISABLE_SENSORS(m, slist) \
    do { if((m) >= 0 && (m) < NR_REAL_MODES)    \
        Sensors[(m)] &= ~(slist);       \
    } while(0)

/*
 ** Sensor types
 */
#if (HAVE_CTD & 1)
#  define SENS_CTD1     0x001
#else
#  define SENS_CTD1     0x0
#endif
#if (HAVE_CTD & 2)
#  define SENS_CTD2     0x002
#else
#  define SENS_CTD2     0x0
#endif
#ifdef HAVE_ADCP
#  define SENS_ADCP     0x004
#else
#  define SENS_ADCP     0x0
#endif
#ifdef HAVE_BB2F
#  define SENS_BB2F 0x008
#else
#  define SENS_BB2F 0x0
#endif
#ifdef HAVE_ALTIMETER
# define  SENS_ALT      0x010
#else
# define  SENS_ALT      0x0
#endif
#ifdef HAVE_PAR
#  define SENS_IPAR     0x020
#else
#  define SENS_IPAR     0x0
#endif
#ifdef HAVE_NOISE
#  define SENS_NOISE    0x040
#else
#  define SENS_NOISE    0x0
#endif
#ifdef HAVE_GTD
#  define SENS_GTD      0x100
#else
#  define SENS_GTD      0x0
#endif
#if defined(HAVE_FLUOROMETER) || defined(HAVE_ECO)
#  define SENS_FLR      0x200
#else
#  define SENS_FLR      0x0
#endif
#ifdef HAVE_I490
#  define SENS_I490     0x400
#else
#  define SENS_I490     0x0
#endif
#ifdef HAVE_CTDO
#  define SENS_CTDO     0x800
#  define SENS_O2       0x200000L
#else
#  define SENS_CTDO     0x0
#  define SENS_O2       0x0
#endif
#ifdef HAVE_THERM
#  define SENS_THERM    0x2000
#else
#  define SENS_THERM    0x0
#endif

/* Bio-Heavy sensors (managed separately) */
#ifdef HAVE_BIOSENSORS
#define HAVE_OPTODE
#define HAVE_FLNTU
#endif

/* Optode */
#ifdef HAVE_OPTODE
#define SENS_OPTODE     0x08000L
#else
#define SENS_OPTODE     0x0
#endif

/* FLNTU */
#ifdef HAVE_FLNTU
#define SENS_FLNTU      0x04000L
#else
#define SENS_FLNTU      0x0
#endif

/* Acoustic Noise Recorder */
#ifdef HAVE_ANR
#define SENS_ANR        0x100000L
#else
#define SENS_ANR        0x0
#endif

#ifdef HAVE_SSAL
#define SENS_SSAL_CAL   0x400000L
#else
#define SENS_SSAL_CAL   0x0
#endif

#ifdef HAVE_ARG
#define SENS_ARG        0x800000L
#else
#define SENS_ARG        0x0
#endif

#ifdef HAVE_QCP
#define SENS_QCP        0x1000000L
#else
#define SENS_QCP        0x0
#endif

#ifdef HAVE_MCP
#define SENS_MCP        0x2000000L
#else
#define SENS_MCP        0x0
#endif

#ifdef HAVE_ECOPAR
#define SENS_ECOPAR     0x4000000L
#else
#define SENS_ECOPAR     0x0
#endif

#ifdef HAVE_GTD2
#define SENS_GTD2       0x8000000L
#else
#define SENS_GTD2       0x0
#endif

#ifdef HAVE_SUNA
#define SENS_SUNA       0x10000000L
#else
#define SENS_SUNA       0x0
#endif

#define SENS_PAR    (SENS_IPAR | SENS_I490 | SENS_QCP | SENS_MCP |\
                         SENS_ECOPAR)
#define SENS_CTD        (SENS_CTD1 | SENS_CTD2)


/*
 ** Allowed sensors in each mode.
 */
#define PROF_SENSORS    (SENS_CTD | SENS_CTDO | SENS_ADCP | SENS_THERM |\
                         SENS_PAR | SENS_FLR | SENS_GTD | SENS_GTD2 |\
                         SENS_ANR | SENS_O2 | SENS_ARG | SENS_SUNA |\
                         SENS_OPTODE | SENS_FLNTU | SENS_BB2F)
#define SETTLE_SENSORS  (PROF_SENSORS)
#define DRIFT_SENSORS   (PROF_SENSORS)


/* function prototypes */

int next_mode(int mode, double day_time);
int sampling(int nmode, double day_time, double day_sec, double mode_time);
int setup(void); // Set up parameters

double sw_dens(double S, double T, double P);
double sw_pden(double S, double T, double P, double PR);
double sw_ptmp(double S,double T,double P,double PR);
double sw_temp(double S,double T,double P,double PR);
double  sw_adtg(double S,double T, double P);
double sw_dens0(double S, double T);
double filtmean(double *X, int N);
double opt_med5(double *p);
void mlf2_ballast(double day_time, double Pressure, double Temperature,
                  double Salinity,double Temperature_2, double Salinity_2,
                  double ballast, int mode,
                  int drogue_in, double daysec, int command,
                  double *B, int *mode_out,int *drogue_out, double *telem_out);
void safe_ballast(double day_time, double Pressure,double ballast, int mode,
                  double *B,int *mode_out,int *drogue_out);
void labsim(double day_time, int mode, double *Pressure,
            double *Temperature, double *Salinity);
void labsimtest(double day_time, int mode,
                double T, double S, double P,
                double B,int diag,
                double *Pressure, double *Temperature, double *Salinity);

#ifndef SIMULATION
#include <time.h>

short daytime(time_t sampleTime,
          float lon,
          float lat,
          long amoffset,
          long pmoffset,
          long darkSamplePeriod);
#endif
void * ssal_start_profile(long timeout);
int ssal_stop_profile(void *obj);

/* Mission Parameters
Structures - organized by Mode  */


/* DOWN LEG OF PROFILE  */
struct down {
    double Pmax; /* Max depth of down-leg */
    double timeout;   /* Max time of down-leg  */
    double Sigmax;     /* Maximum potential density - ends mode */
    double Bmin;       /* m^3  Bocha setting  (was B0)*/
    double Speed;      /* target speed */
    double Brate;      /* Bocha speed */
    double Pspeed;   /* don't control speed shallower than this */
    short drogue;      /* drogue position: 0 closed, 1 open */
};


/* UP LEG OF PROFILE*/
struct up {
    double Bmax;    /* max bocha setting (was Ball)*/
    double Speed;   /* target speed */
    double Brate;   /* bocha rate to use for speed control */
    double Pend;    /* Minimum Pressure - end of up-leg */
    double timeout; /* Max duration of up-leg   */
    short  Speedbrake; /* if 1, use drogue as speedbrake until float rises */
    double PHyst;     /* Hysterisis (m) allowed in speed control */
    short  drogue;    /* 0 or 1 - drogue open or closed during up */
    double surfacetime;  /* time/sec past reacing up.Pend before end */
};

/* UpSurf NEAR-SURFACE LEG OF PROFILE*/
struct upsurf {
    double Bmax;    /* max bocha setting (was Ball)*/
    double Speed;   /* target speed */
    double Brate;   /* bocha rate to use for speed control */
    double Pend;    /* Minimum Pressure - end of up-leg */
    double timeout; /* Max duration of up-leg   */
    short  Speedbrake; /* if 1, use drogue as speedbrake until float rises */
    double PHyst;     /* Hysterisis (m) allowed in speed control */
    short  drogue;    /* 0 or 1 - drogue open or closed during up */
    double surfacetime;  /* time/sec past reacing up.Pend before end */
    double overlaptime;  /* time/sec during which SSAL and upper CTD are run together (via SENS_SSAL_CAL) */
};

/* SETTLE MODE */
struct settle {
    double secOday;   /* end Settle when time passes this clock time [0 86400] */
    double timeout;         /* Max time of settle-leg/sec -  Skip settle if negative*/
    double seek_time;     /* time for active seeking, if seek_time<1, then a fraction of timeout  */
    double decay_time;    /* decay seeking over this time scale after seek_time, if decay_time<1, then a fraction of seek_time  */
    short  nav;         /* number of points to average to get settled volume */
    double drogue_time; /* time after start of settle to keep drogue open */
    double beta; /* pseudo compressive gain*/
    double tau;             /* seek gain */
    double weight_error;    /* Accept seek values if Dsig*V0 is less than this */
    double Vol_error;       /* Accept volume if stdev of V0 is less than this ( if <0 no volume set) */
    double Ptarget;      /* Target Pressure - used only with Nfake */
    double Nfake;           /* Fake stratification relative to Ptarget  No effect if 0 */
    short  nskip;           /*  (if 0, skip settle mode always)  */
    short SetTarget; /* How to set target
    1- current PotDensity,2-Ballast.target,3-Drift.target, else constant*/
    double Target;          /* Settle Target isopycnal */
    double B0;              /* bocha */
    double Bmin; /* Minimum Bocha */
    double Bmax; /* Maximum Bocha */
};


/* DRIFT MODE  */
/*NB: some of the parameters (Air, Compress, Thermal_exp, Tref) belong in Ballast - need to move them there eventually! - AS */
struct drift {
    short int SetTarget; /*  How to set Target:
    1- current value, 2-Ballast.target, 3-Settle.target, else constant*/
    short int VoffZero;  /* 1 to set Voff=0 at drift start, else keep value */
    short int median;  /* 1 use 5 point median filter, 0 don't */
    short int timetype; /* How to end Drift mode
1: Use time since end of last drift mode or mission start
2: At the given timeOday (defaults to noon = 0.5)
    Also use timeout */
    double time_end_sec;   /*time/seconds to end Drift mode */
    double timeout_sec;    /* Additional timeout to end mode / seconds*/
    double Tref;      /* reference temperature */
    double Voff;       /* Ballast adjustment during Drift  */
    double Voffmin;  /*prevent negative runaway on bottom */
    double Moff;      /* Offset of Mass from Mass */
    double Air;      /* grams of air buoyancy at surface -always used*/
    double Compress;  /* float compressibility db^-1*/
    double Thermal_exp;     /* float thermal expansion coeff */
    double Target;         /* target isopycnal */
    double iso_time;        /*seek time toward surface  sec  */
    double seek_Pmin;      /* depth range for drift seek mode*/
    double seek_Pmax;      /* continued */
    double iso_Gamma;/*Pseudo-compressibility  m^3/unit*/
    double time2;  /* second timeout parameter */
    double closed_time;   /* drogue opens after this time   sec  */
};

/*  BALLAST  VARIABLES
 */

struct ballast{
    short  SetTarget;  /* how to set Ballast.Target
    1,2,3-no changes,    4- end of good Settle
    5 - end of Drift, 6-end of Down, 7-end of Up,
    8- Value at P=Ballast.Pgoal
    9- Value from GetMLB (not implemented yet) */
    short  Vset;    /*set V0: 0 - fixed; 1 - from good settles,
    2 - from Settle and Drift end, 3 from drift end only */
    short MLsigmafilt;   /* 1 to use LowPass filtered Sigma in ML, else Ballast.rho0 */
    double MLthreshold;   /* Switch to ML ballasting if P<Pdev*     */
    double MLmin;            /*        or if P<     */
    double MLtop;          /* or between top and bottom */
    double MLbottom;
    double SEEKthreshold;   /* Allow isoSEEK if P>Pdev*  */
    double Offset;      /* adds offset to bocha in drift mode */
    double T0;      /* most recent temperature at ballasted point */
    double S0;      /* most recent Salinity  */
    double P0;      /* most recent Pressure*/
    double rho0;    /* most recent water (=float) density */
    double B0;      /* most recent bocha setting at ballasted point */
    double V0;      /* Estimated float volume at zero bocha, at surface, ref Temp, no air */
    double TH0;     /* most recent potential temperature */
    double Vdev;  /* most recentstdev of float volume estimates */
    double Pgoal;  /* if STP==2, set STP at this depth */
    double Target; /* target isopycnal e.g. 1022 */
};

/*  CTD  VARIABLES */
#define BOTTOMCTD   0
#define TOPCTD      1
#define MEANCTD     2
#define MAXCTD      3
struct ctd{
    short which;  /* How to get one CTD value from two CTD's see
            options are set above */
    short  BadMax;    /*How many bad CTDs before error */
    double Ptopmin;      /* TopCTD not good above this pressure */
    double Poffset;      /* Offset for Pressure to be zero when top of float is at surface */
    double Separation;  /* Distance between two CTDs*/
    double TopSoffset;    /* Correction to top CTD salinity */
    double TopToffset;   /*Correction to top CTD temperature*/
    double BottomSoffset;   /* Correction to Bottom CTD salinity */
    double BottomToffset;   /*Correction to Bottom CTD temperature*/
};




/*  MIXED LAYER BASE STRUCTURE  */

#define Nsave 1000  /* array size for raw profile data */
#define Ngrid 100  /* for gridded data */
struct mlb{
    short  go;  /* 1 to compute MLB target, otherwise don't */
    short record;   /* 1 to record data in arrays; else don't */
    short point;       /* points at next open element in raw arrays, 0-> empty */
    short Nmin;    /* minimum number of data to do computation */
    double dP;     /* grid spacing m */
    double dSig;  /* bin size for Sigma search */
    double Sigoff; /* Offset of goal from ML density */
    double Psave[Nsave]; /* array of raw pressure */
    double Sigsave[Nsave]; /* array of raw potential density */
    double Pgrid[Ngrid]; /* array of gridded pressure */
    double Siggrid[Ngrid]; /* array of gridded potential density */
};
/* function declared after definition */
double getmlb(struct mlb *);  /* pass pointer to structure auto */
double z2sigma(struct mlb *,double Z);  /* pass pointer to structure auto */

/* BUTTERWORTH FILTER STRUCTURE */
struct butter {
   double Tfilt;  /* filter time - coeff should be consistent */
   double A2; double A3; /* Y coeff A1=1 */
   double B1;  double B2; double B3;/* X coeff */
   double Xp;    /* remember previous values */
   double Xpp;
   double Yp;
   double Ypp;
   };
/* function declaration */
double Bfilt(struct butter * , double , double, short , struct butter );
double ButterLowCoeff(double, struct butter *);
double ButterHiCoeff(double, struct butter  *);

/*  DEPTH ERROR PARAMETERS */
struct error{
    short Modes;      /* 1: drift only   2: settle only 3: drift and settle   Else: None */
    double Pmin;      /* Minimum Pressure  allowed */
    double Pmax;     /* Maximum Pressure  allowed */
    double timeout;    /* time outside of this range before error is declared*/
};

/* BUG MITIGATION PARAMETERS */
struct bugs{
    double start;  /* sunset  / seconds of GMT day 0-86400 */
    double stop;  /* sunrise */
    double start_weight;  /* sunset weight / kg  */
    double stop_weight;  /* sunrise weight /kg - linear interpolation between */
    double flap_interval;  /* time between flaps / seconds */
    double flap_duration;  /* time between close and open */
    double weight;
};


/* SAFE MODE PARAMETERS -- why here ? Move to safeballast.c*/
struct safe{
    double Ptarget;   /* target depth /m */
    double Pband;     /* deadband width +- about this /m */
    double ballast0;  /* first guess of ballast position at Ptarget */
    double dbdp;   /* PART 1 -  stabilizing slope - m^3 / db */
    double Alpha;   /* PART 2- Seek Gain   m^3/sec/dbar */
    double timeout_sec;  /*cycle time - surface and COMM this often */
    double downtime;     /* initial time in down mode / sec */
};

//
///* Stage simulation */
//struct stagesim{
//  short jump;   /* change stage after this many COMMS */
//  short stage1; /* change stage to this first  */
//  short stage2;  /* second */
//  short count;  /* counts COMMS  */
//};


/* GLOBAL TIMERS */
struct timer{
    short enable;   // ==1 if all timers are enabled (individual timers can be controlled by setting the time ore stage to -1
    double time1;   // time (in seconds since midnight) for the first timer event. May want to change to multy-day timers eventually
    double time2;   // ... for the second timer event
    double time3;   // ... for the third timer event
    double time4;   // ... for the fourth timer event

    short stage1;   // stage to switch to when the first timer is triggered (set to -1 to disable)
    short stage2;   // ... second timer
    short stage3;   // ... third timer
    short stage4;   // ... fourth timer

    short nskip1;  // skip this many timer triggers - allows multiday timers
    short nskip2;  // ... second timer
    short nskip3;  // ... third timer
    short nskip4;  // ... fourth timer

    short countdown1; // counts down skips
    short countdown2;
    short countdown3;
    short countdown4;

};

/*
 END STRUCTURES */



#endif /* _BALLAST_H_ */
