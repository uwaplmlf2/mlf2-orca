/* This file is included in ballast.c */
/* It sets initial values of all control variables, so that the main code
	does not need to be modified when values change */
/* Do not change this variable */
static const char *__id__ = "$Id$";

/* Oct 2014 - ORCA - Massive simplification of this code since mission is so simple
                     No Level II parameters */
// 25 July 2012 - separate parameter declaration (global) and initialization(in setup() function), in anticipation of the two-tier parameter model



# define Nav  100         /*Size of averaging array in Settle, Settle.nav must be smaller than this*/
# define RHOMIN 900  /* minimum allowed density */
# define NLOG     100   /* output data every NLOG calls  */
# define NCOMMAND 3 /* Number of real commands  1 2 3 ... NCOMMAND */
# define LOGDAYSSIM 10  /* Stop ballastlog after this many days in simulation */

/* Misc. global scalar parameters - all satellite settable */
static double Mass0 = 50.00;  /* initial mass */
static double Creep=0.00e-6;  /* kg/day */
static double deep_control = 25.e-6;
static double bottom_P=450.;  /* 180 Depth to push out piston */
static double error_P=480.; /* 220 Depth to declare emergency */
static double top_P=-100;   /* Minimum allowed depth */
static double shallow_control=3e-6;  /*  m^2/dbar */
static short int stage=0;   /* Master mode control variable */
static short int newstage=0;  /* set this by satellite to next desired stage, for orderly change at end of present stage */
static double telem_Step=0.1;   /* Commanded change in target */
static double Home_days=1;    /* Home about this often */
static short Commskipmax=3; /* maximum number of comms that can be skipped by timers - a safety feature */
static double MaxQuietDays=5; /*maximum number of days without a comm mode - ERROR - another safety feature */
static double SSAL_off_depth = 1; // disable SSAL and re-enable regular CTD at this depth (unless in UP2 mode)
                            // This turns off SSAL on a down after a COM
                            // Note, that SSAL will likely be already off on a timeout, so this is mostly about re-enabling top CTD.
static double sleepduration=8000;// Total sleep time sec
static double  PgtdCont=3;   /* GTD to continuous pumping shallower than this */

/**************************************************************************************************
	Declare global mission-specific parameters - Tier II
   Mission is so simple that these are pretty simple also
****************************************************************************************************/

/* Initialize parameter values and link them with the Ptable */

int initialize_mission_parameters(void){
	int i;
	 /* Sampling intervals
     ** Note that for the profiles, the "sampling interval" is now
     ** used to specify the maximum time allowed for ballast adjustment
     ** during each sample.
     */
    Si[MODE_PROFILE_UP] = 10;
    Si[MODE_PROFILE_DOWN] = 10;
    Si[MODE_PROFILE_SURF] = 10;


    Si[MODE_SETTLE] = 30;
    Si[MODE_DRIFT_ISO] = 30;
    Si[MODE_DRIFT_ML] = 30;
    Si[MODE_DRIFT_SEEK] = 30;

	/************************************************************************************************
	 Initialization of Tier 1 parameters.
	*************************************************************************************************/

	Down.Pmax = 98; /* Max depth of down-leg */
    Down.timeout = 4000;   /* 4000 Max time of down-leg  */
    Down.Sigmax = 5000;     /* Maximum potential density - ends mode */
    Down.Bmin = 0;              /* m^3  Bocha setting  */
    Down.Speed = 0.11;      /* 0.11 target speed */
    Down.Brate = 0.5e-6;      /* 5e-8 Bocha speed */
    Down.Pspeed = 2.;   /* don't control speed shallower than this */
    Down.drogue = 0;      /* drogue position: 0 closed, 1 open */

	Up.Bmax = 650.e-6;    /* max bocha setting*/
    Up.Speed = 0.07;   /* 0.07 target speed */
    Up.Brate = 0.05e-6;   /* bocha rate to use for speed control */
    Up.Pend = 3;    /* Minimum Pressure - end of up-leg */
    Up.timeout = 6000; /* Max duration of up-leg   */
    Up.Speedbrake = 1; /* if 1, use drogue as speedbrake until float rises */
    Up.PHyst = 3.;     /* Hysterisis (m) allowed in speed control */
    Up.drogue = 0;    /* 0 or 1 - drogue open or closed during up */
    Up.surfacetime= 500.;  /* time/sec past reaching up.Pend before end */

	Settle.secOday = -1;   /* end Settle when time passes this clock time [0 86400] (obsolete: replaced by timers) */
    Settle.timeout = 7200;         /* Max time of settle-leg/sec -  Skip settle if negative*/
    Settle.seek_time = 0.8;     /* time for active seeking, if seek_time<1, then a fraction of timeout  */
    Settle.decay_time = 0.4;      /* decay seeking over this time scale after seek_time, if decay_time<1, then a fraction of seek_time  */
    Settle.nav = 20;		    /* number of points to average to get settled volume */
    Settle.drogue_time = 300; /* time after start of settle to keep drogue open */
    Settle.beta = 4; /* pseudo compressive gain (big for stable)*/
    Settle.tau = 400;             /* seek gain (big for stable)*/
    Settle.weight_error = 1;    /* Accept seek values if Dsig*V0 is	less than this (in kg)*/
    Settle.Vol_error = 0.5e-6;       /* 0.5e-6 Accept volume if stdev of V0 is less	than this ( if <0 no volume set) */
    Settle.Ptarget = 15;      /* Target Pressure - used only with Nfake */
	Settle.Nfake = 0;		    /* Fake stratification relative to Ptarget  No effect if 0 */
    Settle.nskip = 1;           /*	(if 0, skip settle mode always)  */
    Settle.SetTarget = 0; /* How to set target: 	1- current PotDensity,2-Ballast.target,3-Drift.target, else constant*/
    Settle.Target = 1022.4;          /* Settle Target isopycnal */
    Settle.B0 = 50e-6;              /* bocha,  don't need to set */
    Settle.Bmin = 0.; /* Minimum Bocha */
    Settle.Bmax = 650.e-6; /* Maximum Bocha */

	Drift.SetTarget = 3; /*  How to set Target: 	1- current value, 2-Ballast.target, 3-Settle.target, else constant*/
    Drift.VoffZero = 1;  /* 1 to set Voff=0 at drift start, else keep value */
    Drift.median = 1;  /* 1 use 5 point median filter, 0 don't */
    Drift.timetype = 2; /* How to end Drift mode 1: Use time since end of last drift mode or mission start 2: At the given timeOday (defaults to noon = 0.5) 	Also use timeout */
    Drift.time_end_sec = -1;   /*time/seconds to end Drift mode */
    Drift.timeout_sec = 500.;	   /* Additional timeout to end mode / seconds*/
    Drift.Tref = 8;      /* reference temperature */
    Drift.Voff = 0;       /* Ballast adjustment during Drift  */
    Drift.Voffmin = -200e-6;  /*prevent negative runaway on bottom */
    Drift.Moff = 0;      /* Offset of Mass from Mass */
    Drift.Air = 15.5e-6;      /* grams of air buoyancy at surface -always used*/
    Drift.Compress = 3.1e-6;  /* float compressibility db^-1*/
    Drift.Thermal_exp = 0.722e-4;     /* float thermal expansion coeff */
    Drift.Target = 1024.0;         /* target isopycnal */
    Drift.iso_time = 7200;        /*seek time toward surface  sec  */
    Drift.seek_Pmin = -100;      /* depth range for drift seek mode*/
    Drift.seek_Pmax = -50;      /* continued */
    Drift.iso_Gamma = 1;/*Pseudo-compressibility  m^3/unit, 1 = isopycnal*/
    Drift.time2 = 600;  /* second timeout parameter -- compatibility only, DO NOT USE  */
    Drift.closed_time = 200;   /* drogue opens after this time   sec  */

	Ballast.SetTarget = 8;  /* how to set Ballast.Target	1,2,3-no changes,    4- end of good Settle 	5 - end of Drift, 6-end of Down, 7-end of Up, 	8- Value at P=Ballast.Pgoal	9- Value from GetMLB (not implemented yet) */
    Ballast.Vset = 0;    /*set V0: 0 - fixed/manual; 1 - from good settles, 	2 - from Settle and Drift end, 3 from drift end only */
	Ballast.MLsigmafilt = 0;   /* 1 to use LowPass filtered Sigma in ML, else Ballast.rho0 */
    Ballast.MLthreshold = -100;   /* Switch to ML ballasting if P<Pdev*[]     */
    Ballast.MLmin = 2;            /*        or if P<     */
	Ballast.MLtop = 2;          /* or between top and bottom */
	Ballast.MLbottom =10;
    Ballast.SEEKthreshold = -100;   /* Allow isoSEEK if P>Pdev*[]  & Pressure between Drift.seek_* limits   */
    Ballast.Offset = 0;		/* adds offset to bocha in drift mode */
    Ballast.T0 = 26.8;      /* most recent temperature at ballasted point */
    Ballast.S0 = 37.45;      /* most recent Salinity  */
    Ballast.P0 = 0;      /* most recent Pressure*/
    Ballast.rho0 = 1024.628;    /* most recent water (=float) density (used to be important)*/
    Ballast.B0 = 160.e-6;      /* most recent bocha setting at ballasted point */
    Ballast.V0 = 48700.00e-6;      /* Estimated float volume at zero bocha, at surface, ref Temp, no air -- IMPORTANT - First guess for Settle*/
    Ballast.TH0 = 26.8;     /* most recent potential temperature */
    Ballast.Vdev = 0;  /* most recent stdev of float volume estimates */
    Ballast.Pgoal = 30;  /* if STP==2, set STP at this depth */
    Ballast.Target = 1022.4; /* target isopycnal e.g. 1022 */

	CTD.which = BOTTOMCTD;  /* How to get one CTD value from two CTD's - BOTTOMCTD ,TOPCTD, MEANCTD or MAXCTD */
    CTD.BadMax = 25;    /*How many bad CTDs before error   Long integer - Can be up to 2.1e9 */
    CTD.Ptopmin = 2.1;      /* TopCTD not good above this pressure */
    CTD.Poffset = 0.106;      /* Offset for Pressure to be zero when top of float is at surface, adjusts pressure to be that at middle of float hull*/
    CTD.Separation = 1.42;  /* Distance between two CTDs*/
    CTD.TopSoffset = 0;    /* Correction to top CTD salinity */
    CTD.TopToffset = 0;   /*Correction to top CTD temperature*/
    CTD.BottomSoffset = 0;   /* Correction to Bottom CTD salinity */
    CTD.BottomToffset = 0;   /*Correction to Bottom CTD temperature*/

	Mlb.go = 1;  /* 1 to compute MLB target, otherwise don't */
    Mlb.record = 0;   /* 1 to record data in arrays; else don't */
    Mlb.point = 0;       /* points at next open element in raw arrays, 0-> empty */
    Mlb.Nmin = 45;    /* minimum number of data to do computation */
    Mlb.dP = 2;     /* grid spacing m */
    Mlb.dSig = 0.02;  /* bin size for Sigma search */
    Mlb.Sigoff = 0.2; /* Offset of goal from ML density, final target = MLsigma + Sigoff */
    //Mlb.Psave = 0; /* array of raw pressure */
    //Mlb.Sigsave =0 ; /* array of raw potential density */
    //Mlb.Pgrid = 0; /* array of gridded pressure */
    //Mlb.Siggrid = 0; /* array of gridded potential density */


	Error.Modes = 3;      /* 1: drift only   2: settle only 3: drift and settle   Else: None */
    Error.Pmin = 2;      /* Minimum Pressure  allowed */
    Error.Pmax = 460;     /* Maximum Pressure  allowed */
    Error.timeout = 3600;    /* time outside of this range before error is declared*/

	Bugs.start = -1;  /* sunset  / seconds of GMT day 0-86400 */
	Bugs.stop = -1;  /* sunrise */
	Bugs.start_weight = 0;  /* sunset weight / kg  */
	Bugs.stop_weight = 0;  /* sunrise weight /kg - linear interpolation between */
	Bugs.flap_interval = 10000;  /* time between flaps / seconds */
	Bugs.flap_duration = 130;  /* time between close and open */
	Bugs.weight = 0; /* weight - don't set  */

	Timer.enable = 0;	// ==1 if all timers are enabled (individual timers can be controlled by setting the time ore stage to -1
	Timer.time1 = -1;	// time (in seconds since midnight GMT) for the first timer event. May want to change to multy-day timers eventually
	Timer.time2 = -1;	// ... for the second timer event
	Timer.time3 = -1;	// ... for the third timer event
	Timer.time4 = -1;	// ... for the fourth timer event
	Timer.stage1 = 1;	// stage to switch to when the first timer is triggered (set to -1 to disable)
	Timer.stage2 = 1;   // ... second timer
	Timer.stage3 = 1;   // ... third timer
	Timer.stage4 = 1;   // ... fourth timer
    Timer.nskip1 = 0;   // How many timer triggers to skip?
    Timer.nskip2 = 0;   // timer2
    Timer.nskip3 = 0;   // timer3
    Timer.nskip4 = 0;   // timer4

    Timer.countdown1=Timer.nskip1; // DON'T SET - internal countdown variable
    Timer.countdown2=Timer.nskip2; // DON'T SET - internal countdown variable
    Timer.countdown3=Timer.nskip3; // DON'T SET - internal countdown variable
    Timer.countdown4=Timer.nskip4; // DON'T SET - internal countdown variable

	};
