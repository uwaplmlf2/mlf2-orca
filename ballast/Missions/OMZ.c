/* arch-tag: 0e0de6c5-8b15-4b25-8b35-21d785e9076f */
/* OMZ from Bengal from SPURS from LAKE2 from LATMIX  */
/* NEED TO SET PARAMETERS SET IN InitializeOMZ */
/* Feb, 2014  */

/* Mission consists of
 * stage==0:  Puget Sound ballast, followed by EOS
 * else :          Lagrangian in ML immediately, COMM repeat
 *
 * Add StageSim structure to simulate satellite stage changes
 *
 * Includes BART board commands to end mission. Ignore change isopycnal command
 */

#define max(a, b)	((a) > (b) ? (a) : (b))

int next_mode(int nmode, double day_time)
{
    static int icall=0;

    static int oldstage=0;
    static int savemode; /* saved value of mode - except error */
    static int saveVset= -999; /* save value of Ballast.Vset */
    static int commskipcount=0;  /* Counts skipped comm modes */
    static int goodcomm=1;  /* flag to set if this program set COMM mode */
    static double hometime=0; /* time of last home */

	// backup parameter structures to keep them safe during PS ballast (instead of "save1", "save2", etc.)
	static struct down Down0;
	static struct drift Drift0;
	static struct settle Settle0;

    int oldmode,i, istep;
    double x;
    oldmode=nmode;


            if (stage!=0 && stage!=1 && stage!=2){
        nmode=MODE_ERROR;
        log_event("next_mode: stage is not a valid value %d\n",stage);
            }

    /* command handling - do_command, command_end is a global variable */
    if (do_command>0){
        if (do_command==1){  /* SURFACE NOW */
            log_event("Execute Command 1 - surface in emergency\n");
            nmode=MODE_ERROR;
        }
        /*  Comment out ballast change command
         * else if (do_command==2){   /* lighter - no mode change
         * Ballast.Target=Ballast.Target+telem_Step;
         * log_event("Execute Command 2 - DOWN.  New target %8.4f\n",Ballast.Target);
         * }
         * else if (do_command==3){   /* heavier - no mode change
         * Ballast.Target=Ballast.Target-telem_Step;
         * log_event("Execute Command 3 - UP.  New target %8.4f\n",Ballast.Target);
         * }
         */
        else {
            nmode=MODE_ERROR;
        }
        do_command=0;
        return(nmode);  /* No mode change yet- act through command_end */
    }

    /* Check for return from MODE_ERROR in COMM mode */
    if (nmode==MODE_COMM && goodcomm!=1 ){
        /* COMM at wrong place - must be an error */
        /* Return to mode before ERROR was called, i.e. last call to this subroutine */
        /* do not increment icall; do not advance to next mode */
        nmode=savemode;
        log_event("next_mode: ERROR return. Mode stays at %d\n",nmode);
        return(nmode);
    }
    goodcomm=0; /* reset COMM flag */

    if (stage==0 ){  /* normal PSBallast Followed by EOS */

        if (nmode==MODE_START || stage!=oldstage){
            log_event("STAGE 0: PS BALLAST\n");
			/* Set  HOME at end of DOWN  */
			set_param_int("down_home",1);
			// Save parameters that may be overridden during PS ballast
            Down0 = Down;
			Drift0 = Drift;
			Settle0 = Settle;
			// Disable timers for PS ballast:
			Timer.enable = 0;

            oldstage=stage;
            icall=0;
        }
        ++icall;
        switch(icall){
            /* --------------------- INITIAL DIVE ---------------   */
            case 1: nmode=MODE_PROFILE_DOWN;  /* Initial captive dive  - short */
                Down.Pmax = PSBallast.depth1;
                break;
            case 2: nmode=MODE_SETTLE;
				/* set settle target at end of dive -- this is where it settles */
				//Ballast.Target = PotDensity;   // Perhaps we can set Settle.target directly? (make sure Settle.SetTarget==0) Can actually do it automatically by setting Settle.SetTarget==1!
				Settle.SetTarget = 1;
                Settle.timeout = PSBallast.settle1_time;
                Settle.Ptarget = PSBallast.depth1; // This is NOT what controlls the Settle depth (Ballast.target is)! It is here just in case Nfake is used
				Ballast.Vset=0;  /* do not set ballast.v0 based on this settle */
                break;
            case 3: nmode=MODE_DRIFT_SEEK;
                Drift.timeout_sec = PSBallast.drift1_time;   /* very short test drift mode */
                Drift.closed_time = Drift.timeout_sec+100.; /* don't open drogue in captive dive */
                break;
            case 4: nmode=MODE_PROFILE_UP;
                break;
            case 5: nmode=MODE_PROFILE_SURF;
                break;
            case 6: nmode=MODE_COMM;
                goodcomm=1;
                log_event("next_mode:COMM after test dive\n");
                break;

            /**** Actual ballast - longer settle */
            case 7: nmode=MODE_PROFILE_DOWN;
            Down.Pmax = PSBallast.depth2;
            hometime=day_time; // Record this home
            break;
            case 8:
                nmode=MODE_SETTLE;
                //Ballast.Target = PotDensity;   // Perhaps we can set Settle.target directly? (make sure Settle.SetTarget==0) Can actually do it automatically by setting Settle.SetTarget==1!
                Settle.SetTarget = 1;
                Settle.timeout = PSBallast.settle2_time;
                Settle.Ptarget = PSBallast.depth2;  // This is NOT what controlls the Settle depth (Ballast.target is)! It is here just in case Nfake is used
                Settle.seek_time=PSBallast.settle2_seektime;

                Ballast.Vset = 1;  /* DO set ballast.v0 this time! */
                break;
            case 9: nmode=MODE_DRIFT_SEEK;
            Drift.timeout_sec = PSBallast.drift2_time;  /* longer test drift mode */
            Drift.closed_time = Drift.timeout_sec/5.; //open part way through
            break;
            case 10: nmode=MODE_PROFILE_UP;
            // Restore saved parameters:
            Down = Down0;
            Drift = Drift0;
            Settle = Settle0;
            // enable timers:
            Timer.enable = 1;
            stage=1;  /* continue as EOS */
            log_event("End of PS ballast.\n");
            break;
            default: nmode=MODE_ERROR;
            log_event("ERROR in next_mode(), impossible combination: stage %d, icall %d\n",stage, icall);
            break;  /* can't get here */
        }  /* End Stage=0 loop */
    }

    /* ------------------ EOS -- Stage=1   'Steps' parameter set --------------  */
    else if (stage==1 ){
        if (nmode==MODE_START || stage!=oldstage){
            log_event("STAGE 1: EOS\n");
            oldstage=stage;
            icall=0;
        }
        ++icall;
        log_event("next_mode: icall=%d\n",icall);
        switch(icall){
            case 1: nmode=MODE_PROFILE_UP;
            Up = Steps.Up;
            break;
            case 2: nmode=MODE_COMM;
            goodcomm=1;
            commskipcount=0; /* reset check */
            break;
            case 3: nmode=MODE_PROFILE_DOWN;  /* really short down to get away from surface */
               // Set up the Down modes first
               // verify that the down is deep enough
            for (i=0;i<=3;i++)	{
                if (Steps.Down.Pmax <= Steps.Settle[i].Ptarget) {
                    log_event("WARNING: down.Pmax (%6.3f) <= Steps.Settle[%d].Ptarget (%6.3f), ", Steps.Down.Pmax,i+1,Steps.Settle[i].Ptarget);
                    Steps.Down.Pmax = Steps.Settle[i].Ptarget+1;
                    log_event("extending to %6.3f\n", Steps.Down.Pmax);
                }
            }
            Down = Steps.SlowDown;
            Down.Pmax=3; /* really short down */
            Mlb.point=0;  /* initialize recorder */
            Mlb.record=1;  /* Start recording */
            if (hometime==0 || day_time-hometime > Home_days){   // Home on this down
                set_param_int("down_home",  1);
                hometime=day_time; // remember the time
            }
            else {
                set_param_int("down_home", -1); // do not home
            }
            log_event("Start Recording downcast \n");
            break;
            case 4: nmode=MODE_PROFILE_DOWN; // SlowDown
                Down=Steps.SlowDown;
            break;
            case 5: nmode=MODE_PROFILE_DOWN;  // Regular down
                Down=Steps.Down;
            break;

            case 6: nmode=MODE_SETTLE;   /* Settle #1 */
            Mlb.record=0; // Stop recording
                log_event("Finish Recording downcast\n");
                pMlb=&Mlb;
				// continue, do not break yet!
			case 8:  /* Settle #2 */
			case 10:/* Settle #3 */
            case 12:/* Settle #4  */
            case 14:/* Settle #5  */
            case 16:/* Settle #6 */
            case 18:/* Settle #7 */
			case 20:/* Settle #8 */
                nmode=MODE_SETTLE;
				istep = (icall/2)-3;  /* 6->0   8->1 20->7 */
				Settle = Steps.Settle[istep];
				/*
				NB: there's a similar mechanism in ballast.c to set Ballast.Target from density at a given depth (activated with Ballast.SetTarget==8,
				   but it only works for a single depth. This is a more advanced algorithm that remembers the profile (but it needs the profile in the first place!)
				   Eventually, the two should probably be merged. -AS
				*/
                if (Settle.Ptarget>0 ){
                    x=z2sigma(pMlb,Settle.Ptarget);  /* Get new target from saved profile */
                    if (saveVset!= -999){  /* Restore value of Ballast.Vset if last settle was at surface */
                        Ballast.Vset=saveVset;
                        saveVset=-999;
                    }
                }
                else {  /* if target is 0 or less, this is a surface settle */
                    x=Ballast.Target-10;  /* Push float to surface */
                    log_event("Target is at surface- Do not set V0 \n");
                    saveVset=Ballast.Vset;  /* don't set Ballast.V0 */
                    Ballast.Vset=0;
                }

                if (x>0){
                    Settle.Target = x;
                    log_event("Settle Target #%d: %6.3f db %6.3f sigma\n",istep+1,Settle.Ptarget,x-1000);
                }
                else {    /* if z2sigma() fails use Ballast.Target from initial ballasting */
                    log_event("ERROR: next_mode, no new target #%d, use %6.3g\n",istep+1,Ballast.Target-1000.);
                }
                break;

            case 7: /* Short Drifts at end of settle - for oxygen measurement */
            case 9:
            case 11:
            case 13:
            case 15:
            case 17:
            case 19:
            case 21:
                nmode=MODE_DRIFT_ISO;
                log_event("DRIFT MODE FOR OXYGEN\n");
                break;

            case 22: nmode=MODE_PROFILE_UP;
                Up=Steps.Up;
                Up.Pend=Down.Pmax; /* deepest possible profile point */
                for (i=0;i<8;i++){
                    x=Steps.Settle[i].Ptarget; /* A step target */
                    if (Up.Pend>x && x>0 && Steps.Settle[i].timeout>0 ){
                        Up.Pend=x;
                    }
                }
                log_event("Choose %4.1f as profile top\n",Up.Pend);

                if ( commskipcount<Commskipmax ){
                    icall = 2; // go to first down mode
                    commskipcount=commskipcount+1; /* skipped home counter */
                }
                else {
                    icall=0;  /* do a comm - too many skips */
                    log_event("Too many (%d) comm modes skipped\n",commskipcount);
                    commskipcount=0;
                }

                break;

            default: nmode=MODE_ERROR;
            log_event("ERROR in next_mode(), impossible combination: stage %d, icall %d\n",stage, icall);
            break;  /* can't get here */
        }  /* End Stage=1 loop */
    }

    else {  /* BAD STAGE */
        nmode=MODE_ERROR;
        log_event("ERROR in next_mode(): invalid stage %d\n",stage);
    }
    log_event("next_mode: %d->%d\n",oldmode,nmode);

//#ifdef SIMULATION
///* Simulate satellite change of stage */
///* switch between new and old every jump COMM modes */
//if (nmode==MODE_COMM){
//    log_event("Stagesim COMM: count %d  jump %d stage %d\n",Stagesim.count, Stagesim.jump,stage);
//    ++Stagesim.count;
//    if (Stagesim.count>=Stagesim.jump){
//        log_event("CHANGE STAGE: %d ->",stage);
//        if (stage!=Stagesim.stage1 ){
//            stage=Stagesim.stage1;
//            Stagesim.count=0;
//        }
//        else{
//            stage=Stagesim.stage2;
//            Stagesim.count=0;
//        }
//        log_event("%d\n",stage);
//    }
//}
//#endif
savemode=nmode;
return(nmode);
}
#include "SampleOMZ.c"
