/* This file is included in ballast.c */
/* It sets initial values of all control variables, so that the main code
	does not need to be modified when values change */
/* Do not change this variable */
static const char *__id__ = "$Id$";

/* May 20, 2009 - HURR09 updated for PUGETSOUNDBALLASTEOS*/

static struct up Up = {
    650.e-6,    /*250e-6 * Ball */
    1.,        /* Gas 0.02   target Speed db/s */
    0.,        /* Gas 5.e-8    Brate m^3/sec 150cc/1hr */
    2.,        /* Pend (must be bigger than CTD.separation/2) */
    3600.,   /*(3600) timeout */
    1,		/* Speedbrake 1= ON */
    10.,        /* Hysteresis in speed control / m*/
    0.,		/* Drogue 1=open, 0 closed */ 
    900.		/* surfacetime / sec  <----------- NONSTANDARD (0.)*/
};

static struct down Down = {
     60.,       /*Pmax  (60) */
    2200.,      /* timeout */
    5025.5,     /* Sigmax  (downleg end) */
    0.,	        /* 50e-6 B0 */
    1.,	/* Gas 0.02  target speed m/s */
    0.,	/* Gas 20e-8 Bocha speed */
    0		/* Drogue  0 = closed */    
};

static struct drift Drift = {
    1,   	/* SetTarget  set value of Drift.Target at start of Drift
		         1- = current PotDensity 2-  =Ballast.Target, 3- =Settle.Target
			else don't change */
    1,	         /* VoffZero  1- set Voff=0 at drift start, else keep value */
    1,		/* median: 1 use 5 pt median filter, 0 don't */
    0,		/* timetype: 1 since last, 2 secondsOday, else only timeout  */
    120000.,/* time_end_sec    time in seconds   or  secondsOday (0-86400) */
    1800.,	/* timeout_sec / seconds */
    8.00,       /* 8      Tref  */
    0.,         /* Voff  */
    -200e-6,    /* Voffmin - minimum value of Voff (bottom interaction) */
    0.,         /* Moff */
    10.e-6,      /* 10.e-6 Air */
    3.64e-6,    /* 3.64e-6 Compress */
    0.724e-4,    /* 0.741e-4 Thermal_exp */
    1024.0,    /* Target isopycnal */
    2.e4,        /*2e4 iso_time seek time */
    0.,		 /* 30 seek_Pmin*/
    450.,       /* seek_Pmax */
    1.,        /* 1  Gamma pseudo_Compressibility 1=isopycnal*/
    600.,    /* time2 - used in mission programs - esp PUGETSOUNDBALLAST*/
    200.        /* 0  closed_time sec  drogue opens after this*/
  };

static struct settle Settle = {
    10000.,     /* timeout / sec */
     9000.,	   /*seek_time /sec */
    200.,       /*      decay time for seeking after end of seek_time */
    20,           /* nav - (20) number of points to average to get volume */
    200.,        /* 100 drogue_time /sec - drogue open */
    6., 	/* 3 beta -  PseudoComp (big for stable) */
    200.,       /* 300  tau - seek time (big for stable)*/
    1.00,        /* 0.001  weight_error   kg - really distance from target in kg*/
    0.5e-6, /* 0.5e-6 Vol_error */
    0.0,	/* 0.01  dead zone width, kg/m^3 */
    1,            /* nskip  0 for no settle*/
    1, 		/* SetTarget - set Settle.Target at start of Settle
			1- =current Potdensity, 2- =Ballast.Target, 3- =Drift.Target,
			 else don't change*/
    1023.5,   /* Target isopycnal */
    50e-6,  /* B0 - don't need to set */
    0.,		/* Bmin */
    650.e-6   /* Bmax */
};

static struct ballast Ballast ={
    8,             /*SetBallast - set Ballast.Target to current PotDensity
			4- at good Settle end, 5- at Drift end 
			6- at Down end, 7- at Up end 
			8- whenever P passes Ballast.Pgoal
			9- from GetMLB (not implemented yet)
			else don't change it */
    1,            /* Vset  Set Vol0 at: 1 settle, 0 fixed, 2 Settle&Drift, 3 Drift */
    0,		/* MLsigmafilt  1 to LP filter, else Ballast.rho0 */
    0.,		/* 4 MLthreshold  -ML mode when less than THIS*Pdev  */
    -10.,	/* 2m MLminimum depth */
    250.,	/* MLtop */
    500.,	/* MLbottom */
    0.,		/* SEEKthreshold */
    0.,         /* Offset  m^3 of bocha */
    8.0,            /* T0    First guess */
    33.3,         /* S0  */
    0.,              /* P0   */
    1027.53,        /* rho0  current density (used to be important)*/
    150.e-6,             /*B0 guess of Ballast - not crucial */
    0.048124,       /*  V0  - Important - First guess for Settle  0.0478308 */
    8.0,		/* TH0  */
    0.,			/* Vdev - don't set */
    60.,		/* Pgoal  */
    1027.53	/* Global Target  isopycnal  */
};

static struct ctd CTD ={
MEANCTD,  /* which CTDs to use (BOTTOM TOP MEAN MAX) */
25,		/* BadMax - max # of bad before error */
2.1,		/* 1.7 Ptopmin - top CTD bad above this */
0.106,      /* Poffset - Pressure offset
		0.106m =(33.5"/2 -12.5)*0.025  (5/21/06)
		adjusts pressure to be that at middle of float hull */
1.42,		/* Poffset -CTD separation/m  */
0.,		/* Top Sal offset */
0.,		/* Top T offset */
0.,		/* Bott Sal offset */
0.		/* Bott T offset */
};

static struct mlb Mlb={
	1, /* go  1=compute mlb density */
	0, /*record 1=yes to record data right now, 0 no */
	0, /* point  - 0 means empty */
	45, /*Nmin */
	2.,   /*2 dP  gridding Pressure interval*/
	0.02, /*0.02  dSig pot density search grid*/
	0.2,     /* Sigoff   final target = MLsigma + Sigoff*/
	0.,0.,0.,0.      /* Psave, Sigsave, Pgrid, Siggrid arrays */
};
struct mlb *pMlb; /* pointer at Mlb */

static struct eos EOS={
80.,     /* first, deepest */
50.,
25.,
15.,  /* last, shallowest */
6000.,  /* Settle duration (not used in HOODEOS)*/
1          /* comm, 0-> no comm mode on each cycle */
};

static struct bugs Bugs={
	  3600.,  /* start      sunset  / seconds of GMT day 0-86400 */
	40000.,  /*stop      sunrise */
	0.0,		/* start_weight  sunset  / kg     Zero weights to turn off this code  */
	0.0,		/*stop_weight;  /* sunrise weight /kg - linear interpolation between */
	10000,	/* flap_interval:  /* time between flaps / seconds */
	130.,		/* flap_duration: time between close and open / seconds  */
	0.		/* weight - don't set  */
};

static struct steps Steps={ 
1024,   /* Sig0 - surface density   NONE OF THESE USED in PUGETSOUNDBALLASTEOS*/
0.2,	/* dSig1 */
0.28,	/* dSig2 */
0.32,	/* dSig3 */
0.32,	/* dSig4 */
0.32,	/* dSig5 */
0.,	/* z0    Not used in PUGETSOUNDBALLASTEOS*/
15.,  /*  z1    4 depths for EOS */
30.,  /*  z2 */
60.,  /*  z3 */
120.,  /*  z4 */
150.,  /*  z5 Not used in PUGETSOUNDBALLASTEOS*/
300.,  /* time0   IN PUGETSOUND BALLAST - duration of short settle on first dive */
4000.,   /*time1 */
4000.,   /*time2 */
4000.,   /* time3 */
10000.,   /* time4 */
4000.,   /* time5   Not used in PUGETSOUNDBALLASTEOS*/
0.	     /* cycle - counts where we are in steps */
};

# define Nav  100         /*Size of averaging array in Settle, Settle.nav must be smaller than this*/
# define RHOMIN 900  /* minimum allowed density */
# define NLOG     100   /* output data every NLOG calls  */
# define NCOMMAND 3 /* Number of real commands  1 2 3 ... NCOMMAND */
# define LOGDAYSSIM 0.06  /* Stop ballastlog after this many days in simulation */


/* MISC variables - also satellite settable */
static double Mass0 = 49.3769;  /* initial mass */
static double Creep=0.00;  /* kg/day */
static double deep_control = 25.e-6;
static double bottom_P=150.;  /* Depth to push out piston */
static double error_P=190.; /* Depth to declare emergency */
static double top_P=-100;   /* Minimum allowed depth */
static double shallow_control=3e-6;  /*  m^2/dbar */
static short int stage=1;   /* Master mode control variable (2 adds EOS) */
static short int newstage=0;  /* set this by satellite to next desired stage,
for orderly change at end of present stage */
static double telem_Step=0.1;   /* Commanded change in target */

/* Butterworth filter structures */
/* Each holds both filter coeff and previous values
	so a separate structure is needed for each filter*/

/* Prototypes for holding coefficients of each type*/
static struct butter ButterLow= {  /* prototype LP filter */
   50000.,   /*  Tfilt / sec  - only specify this here */
   0.,0.,0.,0.,0.,0.,0.,0.,0.   /* program fills in these */
};

/* all of these are specified from above */
static struct butter ButterHi; /* prototype HP filter */
static struct butter FiltPlow;  /* filters for each variable */
static struct butter FiltPhi;
static struct butter FiltPdev;
static struct butter FiltSiglow;
