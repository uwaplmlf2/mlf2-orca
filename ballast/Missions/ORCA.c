/* arch-tag: 0e0de6c5-8b15-4b25-8b35-21d785e9076f */
/* ORCA from OMZ from Bengal from SPURS from LAKE2 from LATMIX  */
/*    But really not much like these - much simpler
/* NEED TO SET PARAMETERS SET IN InitializeORCA */
/* Oct, 2014  */

/* Mission consists of
 * stage==0:  DOWN
 * else :   UP1/UP2/COMM/DOWN1/DOWN2/SLEEP repeat
 *
 * Simplify since this will eventually be a different product
*/

#define max(a, b)	((a) > (b) ? (a) : (b))

int next_mode(int nmode, double day_time)
{
    static int icall=0;

    static int oldstage=0;
    static int savemode; /* saved value of mode - except error */
    static int saveVset= -999; /* save value of Ballast.Vset */
    static int commskipcount=0;  /* Counts skipped comm modes */
    static int goodcomm=1;  /* flag to set if this program set COMM mode */
    static double hometime=0; /* time of last home */
    static double sleeptime=0;/* time of last sleep start */

	// backup parameter structures to keep them safe during PS ballast (instead of "save1", "save2", etc.)
	static struct down Down0;
	static struct drift Drift0;
	static struct settle Settle0;

    int oldmode,i, istep;
    double x;
    oldmode=nmode;


    if (stage!=0 && stage!=1 ){
        nmode=MODE_ERROR;
        log_event("next_mode: stage is not a valid value %d\n",stage);
    }

    /* Check for return from MODE_ERROR in COMM mode */
    if (nmode==MODE_COMM && goodcomm!=1 ){
        /* COMM at wrong place - must be an error */
        /* Return to mode before ERROR was called, i.e. last call to this subroutine */
        /* do not increment icall; do not advance to next mode */
        nmode=savemode;
        log_event("next_mode: ERROR return. Mode stays at %d\n",nmode);
        return(nmode);
    }
    goodcomm=0; /* reset COMM flag */


    if (nmode==MODE_START ){
        log_event("Initial down\n");
        /* Set  HOME at end of DOWN  */
        set_param_int("down_home",1);

        nmode=MODE_PROFILE_DOWN;  /* Initial captive dive  - short */
        icall=0;
        // enable timers:
        Timer.enable = 1;
    }
    else{
        ++icall;
        log_event("next_mode: icall=%d\n",icall);
        switch(icall){
            case 1: nmode=MODE_PROFILE_UP;
                 break;
            case 2: nmode=MODE_COMM;
                goodcomm=1;
                break;
            case 3: nmode=MODE_PROFILE_DOWN;
                break;
            case 4: nmode=MODE_SLEEP;
                if (sleeptime==0){
                    sleeptime=day_time;
                    log_event("SLEEPING for %4.0fs\n",sleepduration);
                    icall=icall-1;  // stay in sleep
                }
                else {
                    if (day_time-sleeptime > sleepduration/86400.){
                        icall=0;  /* exit sleep */
                        sleeptime=0;
                        log_event("Exit Sleep\n");
                    }
                    else{
                        icall=icall-1;  /* stay sleeping */
                        log_event("Continue sleeping %5.0fs\n",(day_time-sleeptime)*86400);
                    }
                }
                break;
            default: nmode=MODE_ERROR;
                log_event("ERROR in next_mode(), impossible combination: stage %d, icall %d\n",stage, icall);
                break;  /* can't get here */
        }
    }

    log_event("next_mode: %d->%d\n",oldmode,nmode);

    savemode=nmode;
    return(nmode);
}
#include "SampleORCA.c"
