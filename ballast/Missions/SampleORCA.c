/* Sampling subroutine: for OMZ
Set float sampling scheme for this mission
Call on every data loop
May 29, 2012 - surface salinity profiler support
Jan 22, 2013 - Now pass mode_time to sampling(), so that we can run SSAL calibration for a given period
Feb 2014 -  modify for OMZ
*/

#ifdef SIMULATION
	/* Surface Salinity (Seabird STS) control dummy plugs */
	void* ssal_start_profile(long timeout){
		return (void*)1;
	}

	int ssal_stop_profile(void *obj){
		return 1;
	}

    /* CTD without pumping for oxygen zero operation */
	void* enable_pump_cycle(void){
		return (void*)1;
	}

	int disable_pump_cycle(void){
		return 1;
	}

#endif

int sampling(int nmode, double day_time, double day_sec, double mode_time)
{
	static void* ssal = NULL; // surface salinity control object
	int result;
	long ssal_timeout = 0;
    static short pumpcycle=0;

    /* TURN ON SBE43 Pump cycling in drift modes */
    if is_drift(nmode){
        if (mode_time==0){
            enable_pump_cycle();
            pumpcycle=1;
            log_event("Enable pump cycle\n");
        }
    }
    else{
        if (pumpcycle==1){
            disable_pump_cycle();
            pumpcycle=0;
            log_event("Disable pump cycle\n");
        }
    }

	if (nmode==MODE_PROFILE_SURF){ // UpSurf profile
		if (mode_time==0) { //first entrance - enable calibration
			 ENABLE_SENSORS(MODE_PROFILE_SURF, SENS_SSAL_CAL);
			 log_event("SSAL cal\n");
		}
		if (ssal==NULL && mode_time>UpSurf.overlaptime/86400.) {		// UpSurf mode was active for more than UpSurf.overlaptime, but the profiler has not been started - start it
			ssal = ssal_start_profile(ssal_timeout);
			if (ssal!=NULL){
				log_event("Start SSAL profile\n");
			}
			else {
				log_event("ERROR starting SSAL\n");
			}
		}
	}
	if ((nmode!=MODE_PROFILE_SURF && nmode!=MODE_COMM) && ssal!=NULL && PressureG>SSAL_off_depth) {
		// NOT UpSurf mode, but profiler is still active - stop it
		// Keep sampling in COMM mode, too (?!)
		result = ssal_stop_profile(ssal);
		ssal = NULL;
		log_event("Stop SSAL profile (%d)\n",result);
	}


return(0);
}
